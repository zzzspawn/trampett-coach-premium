package com.norwegianonapps.trampettcoachpremium;

import java.util.ArrayList;
import java.util.List;

public class TrampettTilleggsVerdier {

    private String enkelPikert;
    private String dobbelPikert;
    private String trippelPikert;
    private String enkelStrak;
    private String dobbelStrak;
    private String trippelStrak;
    private String enkelHalvHelSkru;
    private String dobbelHalvHelSkru;
    private String trippelHalvHelSkru;
    private String enkelHalvannenSkru;
    private String dobbelHalvannenSkru;
    private String trippelHalvannenSkru;
    private String enkelDobbelSkru;
    private String dobbelDobbelSkru;
    private String trippelDobbelSkru;
    private String enkelTrippelSkru;
    private String dobbelTrippelSkru;
    private String trippelTrippelSkru;

    public String getEnkelPikert() {return enkelPikert;}
    public String getDobbelPikert() {return dobbelPikert;}
    public String getTrippelPikert() {return trippelPikert;}
    public String getEnkelStrak() {return enkelStrak;}
    public String getDobbelStrak(){return dobbelStrak;}
    public String getTrippelStrak(){return trippelStrak;}
    public String getEnkelHalvHelSkru(){return enkelHalvHelSkru;}
    public String getDobbelHalvHelSkru(){return dobbelHalvHelSkru;}
    public String getTrippelHalvHelSkru(){return trippelHalvHelSkru;}
    public String getEnkelHalvannenSkru(){return enkelHalvannenSkru;}
    public String getDobbelHalvannenSkru(){return dobbelHalvannenSkru;}
    public String getTrippelHalvannenSkru(){return trippelHalvannenSkru;}
    public String getEnkelDobbelSkru(){return enkelDobbelSkru;}
    public String getDobbelDobbelSkru(){return dobbelDobbelSkru;}
    public String getTrippelDobbelSkru(){return trippelDobbelSkru;}
    public String getEnkelTrippelSkru(){return enkelTrippelSkru;}
    public String getDobbelTrippelSkru(){return dobbelTrippelSkru;}
    public String getTrippelTrippelSkru(){return trippelTrippelSkru;}

    public List<String> getAllValues() {
        List<String> list = new ArrayList<String>();
        list.add(enkelPikert);
        list.add(dobbelPikert);
        list.add(trippelPikert);
        list.add(enkelStrak);
        list.add(dobbelStrak);
        list.add(trippelStrak);
        list.add(enkelHalvHelSkru);
        list.add(dobbelHalvHelSkru);
        list.add(trippelHalvHelSkru);
        list.add(enkelHalvannenSkru);
        list.add(dobbelHalvannenSkru);
        list.add(trippelHalvannenSkru);
        list.add(enkelDobbelSkru);
        list.add(dobbelDobbelSkru);
        list.add(trippelDobbelSkru);
        list.add(enkelTrippelSkru);
        list.add(dobbelTrippelSkru);
        list.add(trippelTrippelSkru);

        return list;
    }

    public void addEnkelPikert(String verdi) {
        enkelPikert = verdi;
    }
    public void addDobbelPikert(String verdi) {
        dobbelPikert = verdi;
    }
    public void addTrippelPikert(String verdi) {
        trippelPikert = verdi;
    }
    public void addEnkelStrak(String verdi) {
        enkelStrak = verdi;
    }
    public void addDobbelStrak(String verdi) {
        dobbelStrak = verdi;
    }
    public void addTrippelStrak(String verdi) {
        trippelStrak = verdi;
    }
    public void addEnkelHalvHelSkru(String verdi) {
        enkelHalvHelSkru = verdi;
    }
    public void addDobbelHalvHelSkru(String verdi) {
        dobbelHalvHelSkru = verdi;
    }
    public void addTrippelHalvHelSkru(String verdi) {
        trippelHalvHelSkru = verdi;
    }
    public void addEnkelHalvannenSkru(String verdi) {
        enkelHalvannenSkru = verdi;
    }
    public void addDobbelHalvannenSkru(String verdi) {
        dobbelHalvannenSkru = verdi;
    }
    public void addTrippelHalvannenSkru(String verdi) {
        trippelHalvannenSkru = verdi;
    }
    public void addEnkelDobbelSkru(String verdi) {
        enkelDobbelSkru = verdi;
    }
    public void addDobbelDobbelSkru(String verdi) {
        dobbelDobbelSkru = verdi;
    }
    public void addTrippelDobbelSkru(String verdi) {
        trippelDobbelSkru = verdi;
    }
    public String addEnkelTrippelSkru(String verdi){return enkelTrippelSkru = verdi;}
    public String addDobbelTrippelSkru(String verdi){return dobbelTrippelSkru = verdi;}
    public String addTrippelTrippelSkru(String verdi){return trippelTrippelSkru = verdi;}



}
