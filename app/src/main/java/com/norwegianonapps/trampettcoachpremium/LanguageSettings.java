package com.norwegianonapps.trampettcoachpremium;

import android.content.Context;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.TextView;
import android.widget.Toast;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.Reader;

public class LanguageSettings extends AppCompatActivity {
    FileHandlerClass fHC;
    Toast toast;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_language_settings);
        fHC = new FileHandlerClass(this);
        setLanguages();
        setUpCheckboxes();
    }
    public void buttonBackPressed(View view){
        onBackPressed();
    }

    public void onCheckButtonPressed(View view){

        CheckBox checkBox = findViewById(view.getId());

        CheckBox english = findViewById(R.id.English);
        CheckBox norwegian = findViewById(R.id.Norwegian);
        CheckBox german = findViewById(R.id.German);

        if(english.getId() == checkBox.getId()){
            english.setChecked(true);
            norwegian.setChecked(false);
            german.setChecked(false);
            fHC.savePressedLanguage("English");
            setLanguages();
        }else if(norwegian.getId() == checkBox.getId()){
            english.setChecked(false);
            norwegian.setChecked(true);
            german.setChecked(false);
            fHC.savePressedLanguage("Norwegian");
            setLanguages();
        }else if(german.getId() == checkBox.getId()){
            english.setChecked(false);
            norwegian.setChecked(false);
            german.setChecked(true);
            fHC.savePressedLanguage("German");
            setLanguages();
        }


    }


    public void setLanguages(){
        TextView languageTitle = findViewById(R.id.languageTitle);
        CheckBox english = findViewById(R.id.English);
        CheckBox norwegian = findViewById(R.id.Norwegian);
        CheckBox german = findViewById(R.id.German);
        Button backButtonLanguage = findViewById(R.id.backButtonLanguage);
        //CheckBox customButton = findViewById(R.id.customTegnButton);

        TextClassGetter tCG = new TextClassGetter(this);
        String[] array = tCG.getText("LanguageSettings");
        languageTitle.setText(array[0]);
        english.setText(array[1]);
        norwegian.setText(array[2]);
        german.setText(array[3]);
        backButtonLanguage.setText(array[4]);
        //customButton.setText(array[4]);
    }

    public void setUpCheckboxes(){
        CheckBox english = findViewById(R.id.English);
        CheckBox norwegian = findViewById(R.id.Norwegian);
        CheckBox german = findViewById(R.id.German);

        String language = fHC.getLanguage();

        if(language.equals("English")){
            english.setChecked(true);
            norwegian.setChecked(false);
            german.setChecked(false);
        }else if (language.equals("Norwegian")){
            english.setChecked(false);
            norwegian.setChecked(true);
            german.setChecked(false);
        }else if (language.equals("German")){
            english.setChecked(false);
            norwegian.setChecked(false);
            german.setChecked(true);
        }
    }



}
