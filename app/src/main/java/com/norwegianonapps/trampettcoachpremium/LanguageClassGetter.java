package com.norwegianonapps.trampettcoachpremium;

import android.content.Context;

/**
 * Created by zzzsp on 06-Nov-17.
 */

public class LanguageClassGetter {
    FileHandlerClass fileHandlerClass;
    Context context;
    public LanguageClassGetter(Context context){
        this.context = context;
        fileHandlerClass = new FileHandlerClass(context);
    }

    public String getLanguage(){

        return fileHandlerClass.getLanguage();
        //return "norwegian";
    }


}
