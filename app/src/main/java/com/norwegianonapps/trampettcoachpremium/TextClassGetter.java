package com.norwegianonapps.trampettcoachpremium;

import android.content.Context;

/**
 * Created by zzzsp on 06-Nov-17.
 */

public class TextClassGetter {
    Context context;
    public TextClassGetter(Context context){
        this.context = context;
    }

    //følg leseretningen
    public String[] getText(String skjerm){
        LanguageClassGetter lCG = new LanguageClassGetter(context);
        if(skjerm.equals("MainActivity")){

            if(lCG.getLanguage().equals("English")){
                String[] array = new String[] {"Settings","Trampet","Vaulting table"};
                return array;
            }else if (lCG.getLanguage().equals("Norwegian")){
                String[] array = new String[] {"Instillinger","Trampett","Pegasus"};
                return array;
            }else if (lCG.getLanguage().equals("German")){
                String[] array = new String[] {"Einstellungen","Trampolin","Sprungtisch"};
                return array;
            }

        }else if(skjerm.equals("ValgSkjerm")){
            if(lCG.getLanguage().equals("English")){
                String[] array = new String[] {"Clear Last","Clear All","Done","Please choose a series","Only possible to choose ", " series."};
                return array;
            }else if (lCG.getLanguage().equals("Norwegian")){
                String[] array = new String[] {"Fjern Siste","Fjern Alle","Ferdig","Vennligst velg en serie","Kun mulig å velge ", " serier."};
                return array;
            }else if (lCG.getLanguage().equals("German")){
                String[] array = new String[] {"Klares Letztes","Alles löschen","Fertig","Bitte wählen Sie eine Serie","Nur noch ", " Serie wie möglich zu wählen."};
                return array;
            }
        }else if(skjerm.equals("FinalScore")){
            FileHandlerClass fileHandlerClass = new FileHandlerClass(context);
            String rulesetFile = fileHandlerClass.getRuleset();
            if(lCG.getLanguage().equals("English")){
                String ruleset = "";
                if(rulesetFile.equalsIgnoreCase("NGTF")){
                    ruleset = "Norwegian Code of Points";
                }if(rulesetFile.equalsIgnoreCase("UEG")){
                    ruleset = "UEG Code of Points";
                }
                String[] array = new String[] {"Value: ","Value: ","Value: ","Value: ","Value: ","Value: ", "Total Value: ",ruleset, "Back",
                        "Removed Series Illegal in this ruleset","Removed Series Illegal in this ruleset","Added Series Illegal in previous ruleset"};
                return array;
            }else if (lCG.getLanguage().equals("Norwegian")){
                String ruleset = "";
                if(rulesetFile.equalsIgnoreCase("NGTF")){
                    ruleset = "NGTF Reglement";
                }if(rulesetFile.equalsIgnoreCase("UEG")){
                    ruleset = "UEG Code of Points";
                }
                String[] array = new String[] {"Verdi: ","Verdi: ","Verdi: ","Verdi: ","Verdi: ","Verdi: ", "Total Verdi: ", ruleset, "Tilbake",
                        "Fjernet serier ulovlige i dette regelsettet","Fjernet serier ulovlige i dette regelsettet","La til serier ulovlige i forrige regelsettet"};
                return array;
            }else if (lCG.getLanguage().equals("German")){
                String ruleset = "";
                if(rulesetFile.equalsIgnoreCase("NGTF")){
                    ruleset = "NGTF Wertungsvorschriften";
                }if(rulesetFile.equalsIgnoreCase("UEG")){
                    ruleset = "UEG Wertungsvorschriften";
                }
                String[] array = new String[] {"Wert: ","Wert: ","Wert: ","Wert: ","Wert: ","Wert: ", "Gesamtwert: ", ruleset, "Zurück",
                        "In diesem Regelsatz wurde die Serie \"Illegal\" entfernt.","In diesem Regelsatz wurde die Serie \"Illegal\" entfernt.","Serien im vorherigen Regelsatz hinzugefügt"};
                return array;
            }
        }else if(skjerm.equals("Settings")){
            if(lCG.getLanguage().equals("English")){
                String[] array = new String[] {"Settings","Choose Ruleset","Choose Language","Sign Explanations", "Create Custom Series", "Back"};
                return array;
            }else if (lCG.getLanguage().equals("Norwegian")){
                String[] array = new String[] {"Instillinger","Velg Reglement","Velg Språk","Tegnforklaringer", "Lag egendefinerte serier", "Tilbake"};
                return array;
            }else if (lCG.getLanguage().equals("German")){
                String[] array = new String[] {"Einstellungen","Regelsatz auswählen","Sprache wählen","Symbolerklärungen", "Benutzerdefinierte Serie erstellen", "Zurück"};
                return array;
            }
        }else if(skjerm.equals("RulesetSettings")){
            if(lCG.getLanguage().equals("English")){
                String[] array = new String[] {"Choose Ruleset","UEG Code of Points","Norwegian Code of Points","Hordaland Kretsklasse Rules", "Back"};
                return array;
            }else if (lCG.getLanguage().equals("Norwegian")){
                String[] array = new String[] {"Velg Reglement","UEG Code of Points","NGTF Reglement","Hordaland Kretsklasse Reglement", "Tilbake"};
                return array;
            }else if (lCG.getLanguage().equals("German")){
                String[] array = new String[] {"Regelsatz auswählen","UEG WERTUNGSVORSCHRIFTEN","NGTF WERTUNGSVORSCHRIFTEN","Hordaland Kretsklasse Wettbewerbsregeln", "Zurück"};
                return array;
            }
        }else if(skjerm.equals("LanguageSettings")){
            if(lCG.getLanguage().equals("English")){
                String[] array = new String[] {"Choose Language","English","Norwegian","German", "Back"};
                return array;
            }else if (lCG.getLanguage().equals("Norwegian")){
                String[] array = new String[] {"Velg Språk","Engelsk","Norsk","Tysk", "Tilbake"};
                return array;
            }else if (lCG.getLanguage().equals("German")){
                String[] array = new String[] {"Wähle eine Sprache","Englisch","Norwegisch","Deutsch", "Zurück"};
                return array;
            }
        }else if(skjerm.equals("SignExplanations")){
            if(lCG.getLanguage().equals("English")){
                String[] array = new String[] {"Symbols and their meaning","Symbols","Elements","Stretched Jump","X-Jump","Straddle Pike Jump","Tucked Salto",
                        "Piked Salto","Straight Salto","Tsukahara","Handspring","90\u00B0 on 90\u00B0 off", "Twisting in degrees", "Back"};
                return array;
            }else if (lCG.getLanguage().equals("Norwegian")){
                String[] array = new String[] {"Symboler og deres betydning","Symboler","Elementer","Strekkhopp","X-Hopp","Splitthopp","Kroppert Salto",
                        "Pikert Salto","Strak Salto","Tsukahara","Overslag","90\u00B0 på 90\u00B0 av", "Skru i grader", "Tilbake"};
                return array;
            }else if (lCG.getLanguage().equals("German")){
                String[] array = new String[] {"Symbole und ihre Bedeutung","Symbole","Elemente","Gestreckter Sprung","X-Sprung","Spreizen mit Hecht","Gehockt Salto",
                        "gebückter Salto","Gerader Salto","Tsukahara","Überschlag","90\u00B0 auf 90\u00B0 aus", "Verdrehen in Grad", "Zurück"};
                return array;
            }
        }

        return null;

    }






}
