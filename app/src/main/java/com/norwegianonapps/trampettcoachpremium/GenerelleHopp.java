package com.norwegianonapps.trampettcoachpremium;

import android.content.Context;

import java.math.BigDecimal;

/**
 * Created by zzzsp on 16-Oct-17.
 */

public class GenerelleHopp {


    private TilleggsVerdier tV;
    private BaseVerdier bv;
    private HestTilleggsVerdier hTV;
    private String name;
    private BigDecimal verdi;
    Context context;
    int antallRotasjoner;
    boolean spesialHopp;
    String hoppredskap;
    Enum<Retning> retning;
    private BigDecimal skruEn;
    private BigDecimal skruTo;
    private BigDecimal skruTre;

    public Enum<Retning> getRetning(){
        return null;
    }

    public BigDecimal getSkruEn(){
        return null;
    }
    public BigDecimal getSkruT0(){
        return null;
    }
    public BigDecimal getSkruTre(){
        return null;
    }


    public BigDecimal getVerdi() {
        return null;
    }

    public String getName() {
        return name;
    }

    public int getAntallRotasjoner() {
        return antallRotasjoner;
    }

    public boolean isSpesialHopp(){
        return spesialHopp;
    }

    public String getHoppredskap() {
        return hoppredskap;
    }

    public Enum<Posisjon> getPosisjonEn(){
        return null;
    }
    public Enum<Posisjon> getPosisjonTo(){
        return null;
    }
    public Enum<Posisjon> getPosisjonTre(){
        return null;
    }
}
