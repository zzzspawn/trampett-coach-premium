package com.norwegianonapps.trampettcoachpremium;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;

public class CreateCustomSeries extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create_custom_series);
    }

    public void onAddSeriesPressed(View view){

        Intent i=new Intent(this,AddSeries.class);
        startActivity(i);

    }

    public void onSeeAllCustomSeriesPressed(View view){
        Intent i=new Intent(this,AllCustom.class);
        startActivity(i);
    }


}
