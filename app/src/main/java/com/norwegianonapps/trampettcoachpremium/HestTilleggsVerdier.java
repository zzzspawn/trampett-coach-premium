package com.norwegianonapps.trampettcoachpremium;

/**
 * Created by zzzsp on 11-Nov-17.
 */

import java.util.ArrayList;
import java.util.List;

public class HestTilleggsVerdier {

    private String foroverSaltoPikert;
    private String foroverDobbelSaltoPikert;
    private String bakoverHalvInnSaltoPikert;
    private String bakoverTSUPikert;
    private String bakoverTSUDobbelPikert;
    private String foroverOverslagStrak;
    private String foroverSaltoStrak;
    private String foroverDobbelSaltoStrak;
    private String bakoverHalvInnSaltoStrak;
    private String bakoverTSUStrak;
    private String bakoverTSUDobbelStrak;
    private String foroverOverslagHalvHelSkru;
    private String foroverSaltoHalvHelSkru;
    private String foroverDobbelSaltoHalvHelSkru;
    private String bakoverHalvInnSaltoHalvHelSkru;
    private String bakoverTSUHalvHelSkru;
    private String bakoverTSUDobbelHalvHelSkru;
    private String foroverOverslagHalvannenSkru;
    private String foroverSaltoHalvannenSkru;
    private String foroverDobbelSaltoHalvannenSkru;
    private String bakoverHalvInnSaltoHalvannenSkru;
    private String bakoverTSUHalvannenSkru;
    private String bakoverTSUDobbelHalvannenSkru;
    private String foroverOverslagDobbelSkru;
    private String foroverSaltoDobbelSkru;
    private String foroverDobbelSaltoDobbelSkru;
    private String bakoverHalvInnSaltoDobbelSkru;
    private String bakoverTSUDobbelSkru;
    private String bakoverTSUDobbelDobbelSkru;
    private String foroverOverslagPikert;


    private String foroverOverslagTrippelSkru;
    private String foroverSaltoTrippelSkru;
    private String foroverDobbelSaltoTrippelSkru;
    private String bakoverHalvInnTrippelSkru;
    private String bakoverTSUTrippelSkru;
    private String bakoverTSUDobbelTrippelSkru;


    public String getForoverOverslagTrippelSkru() {return foroverOverslagTrippelSkru;}
    public String getForoverSaltoTrippelSkru() {return foroverSaltoTrippelSkru;}
    public String getForoverDobbelSaltoTrippelSkru() {return foroverDobbelSaltoTrippelSkru;}
    public String getBakoverHalvInnSaltoTrippelSkru() {return bakoverHalvInnTrippelSkru;}
    public String getBakoverTSUTrippelSkru() {return bakoverTSUTrippelSkru;}
    public String getBakoverTSUDobbelTrippelSkru() {return bakoverTSUDobbelTrippelSkru;}

    public String getForoverSaltoPikert() {return foroverSaltoPikert;}
    public String getForoverDobbelSaltoPikert() {return foroverDobbelSaltoPikert;}
    public String getBakoverHalvInnSaltoPikert() {return bakoverHalvInnSaltoPikert;}
    public String getBakoverTSUPikert() {return bakoverTSUPikert;}
    public String getBakoverTSUDobbelPikert() {return bakoverTSUDobbelPikert;}
    public String getForoverOverslagStrak() {return foroverOverslagStrak;}
    public String getForoverSaltoStrak() {return foroverSaltoStrak;}
    public String getForoverDobbelSaltoStrak() {return foroverDobbelSaltoStrak;}
    public String getBakoverHalvInnSaltoStrak() {return bakoverHalvInnSaltoStrak;}
    public String getBakoverTSUStrak() {return bakoverTSUStrak;}
    public String getBakoverTSUDobbelStrak() {return bakoverTSUDobbelStrak;}
    public String getForoverOverslagHalvHelSkru() {return foroverOverslagHalvHelSkru;}
    public String getForoverSaltoHalvHelSkru() {return foroverSaltoHalvHelSkru;}
    public String getForoverDobbelSaltoHalvHelSkru() {return foroverDobbelSaltoHalvHelSkru;}
    public String getBakoverHalvInnSaltoHalvHelSkru() {return bakoverHalvInnSaltoHalvHelSkru;}
    public String getBakoverTSUHalvHelSkru() {return bakoverTSUHalvHelSkru;}
    public String getBakoverTSUDobbelHalvHelSkru() {return bakoverTSUDobbelHalvHelSkru;}
    public String getForoverOverslagPikert() {return foroverOverslagPikert;}
    public String getForoverOverslagHalvannenSkru() {return foroverOverslagHalvannenSkru;}
    public String getForoverSaltoHalvannenSkru() {return foroverSaltoHalvannenSkru;}
    public String getForoverDobbelSaltoHalvannenSkru() {return foroverDobbelSaltoHalvannenSkru;}
    public String getBakoverHalvInnSaltoHalvannenSkru() {return bakoverHalvInnSaltoHalvannenSkru;}
    public String getBakoverTSUHalvannenSkru() {return bakoverTSUHalvannenSkru;}
    public String getBakoverTSUDobbelHalvannenSkru() {return bakoverTSUDobbelHalvannenSkru;}
    public String getForoverOverslagDobbelSkru() {return foroverOverslagDobbelSkru;}
    public String getForoverSaltoDobbelSkru() {return foroverSaltoDobbelSkru;}
    public String getForoverDobbelSaltoDobbelSkru() {return foroverDobbelSaltoDobbelSkru;}
    public String getBakoverHalvInnSaltoDobbelSkru() {return bakoverHalvInnSaltoDobbelSkru;}
    public String getBakoverTSUDobbelSkru() {return bakoverTSUDobbelSkru;}
    public String getBakoverTSUDobbelDobbelSkru() {return bakoverTSUDobbelDobbelSkru;}

    public List<String> getAllValues() {
        List<String> list = new ArrayList<String>();
        list.add(foroverOverslagPikert);
        list.add(foroverSaltoPikert);
        list.add(foroverDobbelSaltoPikert);
        list.add(bakoverHalvInnSaltoPikert);
        list.add(bakoverTSUPikert);
        list.add(bakoverTSUDobbelPikert);
        list.add(foroverOverslagStrak);
        list.add(foroverSaltoStrak);
        list.add(foroverDobbelSaltoStrak);
        list.add(bakoverHalvInnSaltoStrak);
        list.add(bakoverTSUStrak);
        list.add(bakoverTSUDobbelStrak);
        list.add(foroverOverslagHalvHelSkru);
        list.add(foroverSaltoHalvHelSkru);
        list.add(foroverDobbelSaltoHalvHelSkru);
        list.add(bakoverHalvInnSaltoHalvHelSkru);
        list.add(bakoverTSUHalvHelSkru);
        list.add(bakoverTSUDobbelHalvHelSkru);
        list.add(foroverOverslagHalvannenSkru);
        list.add(foroverSaltoHalvannenSkru);
        list.add(foroverDobbelSaltoHalvannenSkru);
        list.add(bakoverHalvInnSaltoHalvannenSkru);
        list.add(bakoverTSUHalvannenSkru);
        list.add(bakoverTSUDobbelHalvannenSkru);
        list.add(foroverOverslagDobbelSkru);
        list.add(foroverSaltoDobbelSkru);
        list.add(foroverDobbelSaltoDobbelSkru);
        list.add(bakoverHalvInnSaltoDobbelSkru);
        list.add(bakoverTSUDobbelSkru);
        list.add(bakoverTSUDobbelDobbelSkru);
        list.add(foroverOverslagTrippelSkru);
        list.add(foroverSaltoTrippelSkru);
        list.add(foroverDobbelSaltoTrippelSkru);
        list.add(bakoverHalvInnTrippelSkru);
        list.add(bakoverTSUTrippelSkru);
        list.add(bakoverTSUDobbelTrippelSkru);
        return list;
    }

    public void addForoverOverslagPikert(String verdi) {
        foroverOverslagPikert = verdi;
    }
    public void addForoverSaltoPikert(String verdi) {
        foroverSaltoPikert = verdi;
    }
    public void addForoverDobbelSaltoPikert(String verdi) {
        foroverDobbelSaltoPikert = verdi;
    }
    public void addBakoverHalvInnSaltoPikert(String verdi) {
        bakoverHalvInnSaltoPikert = verdi;
    }
    public void addBakoverTSUPikert(String verdi) {
        bakoverTSUPikert = verdi;
    }
    public void addBakoverTSUDobbelPikert(String verdi) {
        bakoverTSUDobbelPikert = verdi;
    }
    public void addForoverOverslagStrak(String verdi) {
        foroverOverslagStrak = verdi;
    }
    public void addForoverSaltoStrak(String verdi) {
        foroverSaltoStrak = verdi;
    }
    public void addForoverDobbelSaltoStrak(String verdi) {
        foroverDobbelSaltoStrak = verdi;
    }
    public void addBakoverHalvInnSaltoStrak(String verdi) {
        bakoverHalvInnSaltoStrak = verdi;
    }
    public void addBakoverTSUStrak(String verdi) {
        bakoverTSUStrak = verdi;
    }
    public void addBakoverTSUDobbelStrak(String verdi) {
        bakoverTSUDobbelStrak = verdi;
    }
    public void addForoverOverslagHalvHelSkru(String verdi) {
        foroverOverslagHalvHelSkru = verdi;
    }
    public void addForoverSaltoHalvHelSkru(String verdi) {
        foroverSaltoHalvHelSkru = verdi;
    }
    public void addForoverDobbelSaltoHalvHelSkru(String verdi) {foroverDobbelSaltoHalvHelSkru = verdi;}
    public void addBakoverHalvInnSaltoHalvHelSkru(String verdi) {bakoverHalvInnSaltoHalvHelSkru = verdi;}
    public void addBakoverTSUHalvHelSkru(String verdi) {
        bakoverTSUHalvHelSkru = verdi;
    }
    public void addBakoverTSUDobbelHalvHelSkru(String verdi) {bakoverTSUDobbelHalvHelSkru = verdi;}
    public void addForoverOverslagHalvannenSkru(String verdi) {foroverOverslagHalvannenSkru = verdi;}
    public void addForoverSaltoHalvannenSkru(String verdi) {
        foroverSaltoHalvannenSkru = verdi;
    }
    public void addForoverDobbelSaltoHalvannenSkru(String verdi) {foroverDobbelSaltoHalvannenSkru = verdi;}
    public void addBakoverHalvInnSaltoHalvannenSkru(String verdi) {bakoverHalvInnSaltoHalvannenSkru = verdi;}
    public void addBakoverTSUHalvannenSkru(String verdi) {
        bakoverTSUHalvannenSkru = verdi;
    }
    public void addBakoverTSUDobbelHalvannenSkru(String verdi) {bakoverTSUDobbelHalvannenSkru = verdi;}
    public void addForoverOverslagDobbelSkru(String verdi) {
        foroverOverslagDobbelSkru = verdi;
    }
    public void addForoverSaltoDobbelSkru(String verdi) {
        foroverSaltoDobbelSkru = verdi;
    }
    public void addForoverDobbelSaltoDobbelSkru(String verdi) {foroverDobbelSaltoDobbelSkru = verdi;}
    public void addBakoverHalvInnSaltoDobbelSkru(String verdi) {bakoverHalvInnSaltoDobbelSkru = verdi;}
    public void addBakoverTSUDobbelSkru(String verdi) {
        bakoverTSUDobbelSkru = verdi;
    }
    public void addBakoverTSUDobbelDobbelSkru(String verdi) {
        bakoverTSUDobbelDobbelSkru = verdi;
    }

    public void addForoverOverslagTrippelSkru(String verdi) {
        foroverOverslagTrippelSkru = verdi;
    }
    public void addForoverSaltoTrippelSkru(String verdi) {
        foroverSaltoTrippelSkru = verdi;
    }
    public void addForoverDobbelSaltoTrippelSkru(String verdi) {foroverDobbelSaltoTrippelSkru = verdi;}
    public void addBakoverHalvInnSaltoTrippelSkru(String verdi) {bakoverHalvInnTrippelSkru = verdi;}
    public void addBakoverTSUTrippelSkru(String verdi) {
        bakoverTSUTrippelSkru = verdi;
    }
    public void addBakoverTSUDobbelTrippelSkru(String verdi) {bakoverTSUDobbelTrippelSkru = verdi;}

}
