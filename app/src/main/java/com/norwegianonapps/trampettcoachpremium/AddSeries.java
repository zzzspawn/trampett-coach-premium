package com.norwegianonapps.trampettcoachpremium;

import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import java.math.BigDecimal;


public class AddSeries extends AppCompatActivity {

    boolean pickingApparatus;
    boolean pickingDirection;
    boolean pickingNRofRotations;
    boolean pickingTwists;
    boolean specialHoppPicked;
    String apparatus;
    int nrOfRotations;
    String[] possibleSkruVerdier;
    View.OnClickListener listener;

    BigDecimal skruNull;
    BigDecimal skruEn;
    BigDecimal skruTo;
    BigDecimal skruTre;

    Posisjon posisjon0;
    Posisjon posisjon1;
    Posisjon posisjon2;
    Posisjon posisjon3;

    Retning retning;

    String name;
    String value;

    GenerelleHopp genereltHopp;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_series);
        apparatus = "";
        rotationTeller = 0;
        possibleSkruVerdier = new String[10];
        int i = 0;
        int skru = 0;
        while (i < possibleSkruVerdier.length){
            possibleSkruVerdier[i] = Integer.toString(skru);
            skru = skru+180;
            i++;
        }

        setUpPickApparatus();




        listener = new View.OnClickListener() {
            @Override
            public void onClick(View btn) {
                Button b = (Button) btn;
                String skru = b.getText().toString();
                if(rotationTeller == 0){
                    skruNull = new BigDecimal(getKorrektSkruVerdi(skru));
                }else if(rotationTeller == 1){
                    skruEn = new BigDecimal(getKorrektSkruVerdi(skru));
                }else if(rotationTeller == 2){
                    skruTo = new BigDecimal(getKorrektSkruVerdi(skru));
                }else if(rotationTeller == 3){
                    skruTre = new BigDecimal(getKorrektSkruVerdi(skru));
                }

                if(rotationTeller == nrOfRotations){
                    showSeries();
                }else {
                    rotationTeller++;
                    setUpSkruVerdier();
                }

            }
        };

    }

    private void setUpPickApparatus() {
        TextView textView = findViewById(R.id.newSeriesTV);
        textView.setText("Pick Apparatus");
        Button trampett = findViewById(R.id.newSeriesButtonOne);
        Button hest = findViewById(R.id.newSeriesButtonTwo);

        trampett.setText("Trampet");
        trampett.setVisibility(View.VISIBLE);
        hest.setText("Pegasus");
        hest.setVisibility(View.VISIBLE);
        pickingApparatus = true;
    }

    public void onButtonPressed(View view){

        Button b = (Button) view;
        if(pickingApparatus){
            Button trampett = findViewById(R.id.newSeriesButtonOne);
            Button hest = findViewById(R.id.newSeriesButtonTwo);
            trampett.setVisibility(View.GONE);
            hest.setVisibility(View.GONE);
            if(b.getText().equals("Trampet")){
                apparatus = "Trampet";
                setUpPickNumberOfRotations();
            }else if (b.getText().equals("Pegasus")){
                apparatus = "Pegasus";
                setUpForwardBackwards();
            }
        }else if (pickingDirection){
            Button trampett = findViewById(R.id.newSeriesButtonOne);
            Button hest = findViewById(R.id.newSeriesButtonTwo);
            trampett.setVisibility(View.GONE);
            hest.setVisibility(View.GONE);
            if(b.getText().equals("Forwards")){
                retning = Retning.FOROVER;
            }else if(b.getText().equals("Backwards")){
                retning = Retning.BAKOVER;
            }
            setUpPickNumberOfRotations();
        }


    }

    private void setUpForwardBackwards() {
        pickingApparatus = false;
        pickingDirection = true;
        TextView textView = findViewById(R.id.newSeriesTV);
        textView.setText("Pick Direction");
        Button forwards = findViewById(R.id.newSeriesButtonOne);
        Button backwards = findViewById(R.id.newSeriesButtonTwo);
        forwards.setText("Forwards");
        backwards.setText("Backwards");
        forwards.setVisibility(View.VISIBLE);
        backwards.setVisibility(View.VISIBLE);
    }



    private void setUpPickNumberOfRotations() {
        pickingApparatus = false;
        pickingDirection = false;
        pickingNRofRotations = true;
        TextView textView = findViewById(R.id.newSeriesTV);
        textView.setText("Pick Number of rotations");
        Button nill = findViewById(R.id.newSeriesButtonNrNull);
        Button one = findViewById(R.id.newSeriesButtonNrOne);
        Button two = findViewById(R.id.newSeriesButtonNrTwo);
        Button three = findViewById(R.id.newSeriesButtonNrThree);
        nill.setVisibility(View.VISIBLE);
        one.setVisibility(View.VISIBLE);
        two.setVisibility(View.VISIBLE);
        if(apparatus.equals("Trampet")) {
            three.setVisibility(View.VISIBLE);
        }


    }

    public void onNumberOfRotationsPressed(View view){
        Button b = (Button) view;

        if(b.getText().equals("0")){
            if(apparatus.equals("Trampet")){
                specialHoppPicked = true;
            }
            nrOfRotations = 0;
        }else if(b.getText().equals("1")){
            nrOfRotations = 1;
        }else if(b.getText().equals("2")){
            nrOfRotations = 2;
        }else if(b.getText().equals("3")){
            nrOfRotations = 3;
        }

        if(specialHoppPicked){
            setUpNaming();
        }else {
            setUpPosition();
        }

    }

    public void setUpNaming(){
        Button nill = findViewById(R.id.newSeriesButtonNrNull);
        Button one = findViewById(R.id.newSeriesButtonNrOne);
        Button two = findViewById(R.id.newSeriesButtonNrTwo);
        Button three = findViewById(R.id.newSeriesButtonNrThree);
        nill.setVisibility(View.GONE);
        one.setVisibility(View.GONE);
        two.setVisibility(View.GONE);
        three.setVisibility(View.GONE);

        EditText nameBox = findViewById(R.id.nameBox);
        EditText scoreBox = findViewById(R.id.scoreBox);
        Button doneButton = findViewById(R.id.doneFieldBTN);

        nameBox.setVisibility(View.VISIBLE);
        scoreBox.setVisibility(View.VISIBLE);
        doneButton.setVisibility(View.VISIBLE);

    }


    int rotationTeller;
    private void setUpSkruVerdier() {

        TextView textView = findViewById(R.id.newSeriesTV);
        //textView.setText("Pick number of twists for salto " + rotationTeller);
        LinearLayout linlay = findViewById(R.id.linlaySkruverdier);
        linlay.setVisibility(View.VISIBLE);
        if(rotationTeller == 0) {
            int i = 0;
            while (i < possibleSkruVerdier.length) {
                Button tempB = new Button(this);
                tempB.setText(possibleSkruVerdier[i]);
                tempB.setOnClickListener(listener);
                linlay.addView(tempB);
                i++;
            }
        }

        Button kroppert = findViewById(R.id.kroppertBTN);
        Button pikert = findViewById(R.id.pikertBTN);
        Button strak = findViewById(R.id.strakBTN);
        kroppert.setVisibility(View.GONE);
        pikert.setVisibility(View.GONE);
        strak.setVisibility(View.GONE);
        //legg inn i skruknapp

        if(rotationTeller == 0){
            rotationTeller++;
        }
        textView.setText("Pick number of twists for salto " + rotationTeller);
    }

    public void showSeries() {
        TextView textView = findViewById(R.id.newSeriesTV);
        textView.setText("Your new Series: ");
        LinearLayout linlay = findViewById(R.id.linlaySkruverdier);
        linlay.setVisibility(View.GONE);
        Button doneBTN = findViewById(R.id.newSeriesButtonDone);
        Button doneBTNSpecial = findViewById(R.id.doneFieldBTN);
        TextView serieTV = findViewById(R.id.NySerieTextView);
        TextView verdiTV = findViewById(R.id.NySerieVerdiTextView);
        EditText nameBox = findViewById(R.id.nameBox);
        EditText scoreBox = findViewById(R.id.scoreBox);
        nameBox.setVisibility(View.GONE);
        scoreBox.setVisibility(View.GONE);
        doneBTNSpecial.setVisibility(View.GONE);
        verdiTV.setVisibility(View.VISIBLE);
        doneBTN.setVisibility(View.VISIBLE);
        serieTV.setVisibility(View.VISIBLE);
        genereltHopp = null;
        if (specialHoppPicked) {
            SerieManager sM = new SerieManager(this);
            sM.customHopp(name, value, null, apparatus, null, null, null, null, nrOfRotations);
            genereltHopp = sM.getCustomHopp();
        } else {
            Salto salto0 = null;
            Salto salto1 = null;
            Salto salto2 = null;
            Salto salto3 = null;
            SerieManager sM = new SerieManager(this);
            if (nrOfRotations > 0 || nrOfRotations == 0) {
                salto0 = new Salto(posisjon0, skruNull);
            }
            if (nrOfRotations > 0) {
                salto1 = new Salto(posisjon1, skruEn);
            }
            if (nrOfRotations > 1) {
                salto2 = new Salto(posisjon2, skruTo);
            }
            if (nrOfRotations > 2) {
                salto3 = new Salto(posisjon3, skruTre);
            }
            if (apparatus.equals("Pegasus")) {
                sM.customHopp(name, value, retning, apparatus, salto0, salto1, salto2, null, nrOfRotations);
            } else if (apparatus.equals("Trampet")) {
                sM.customHopp(name, value, null, apparatus, salto0, salto1, salto2, salto3, nrOfRotations);
            }
            genereltHopp = sM.getCustomHopp();
        }


        serieTV.setText(genereltHopp.getName());
        verdiTV.setText(genereltHopp.getVerdi().toString());
        if (!genereltHopp.isSpesialHopp()) {
            Typeface typeface = null;
            if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.O) {
                typeface = getResources().getFont(R.font.turntegnregular);
            } else {
                typeface = Typeface.createFromAsset(getAssets(), "fonts/turntegnregular.ttf");
            }
            serieTV.setTypeface(typeface);
        }
    }

    public void setUpPosition(){
        Button nill = findViewById(R.id.newSeriesButtonNrNull);
        Button one = findViewById(R.id.newSeriesButtonNrOne);
        Button two = findViewById(R.id.newSeriesButtonNrTwo);
        Button three = findViewById(R.id.newSeriesButtonNrThree);
        nill.setVisibility(View.GONE);
        one.setVisibility(View.GONE);
        two.setVisibility(View.GONE);
        three.setVisibility(View.GONE);
        TextView textView = findViewById(R.id.newSeriesTV);
        //textView.setText("Pick position for salto " + rotationTeller);
        Button kroppert = findViewById(R.id.kroppertBTN);
        Button pikert = findViewById(R.id.pikertBTN);
        Button strak = findViewById(R.id.strakBTN);
        kroppert.setVisibility(View.VISIBLE);
        pikert.setVisibility(View.VISIBLE);
        strak.setVisibility(View.VISIBLE);

        if(rotationTeller == 0){
            rotationTeller++;
            if(apparatus.equalsIgnoreCase("pegasus")){
                posisjon0 = Posisjon.STRAK;
            }
        }
        textView.setText("Pick position for salto " + rotationTeller);
    }

    public void onPositionButtonPressed(View view){
        Button b = (Button) view;
        String posisjon = b.getText().toString();
        if(rotationTeller == 0 && apparatus.equalsIgnoreCase("trampet")){
            if(posisjon.equalsIgnoreCase("Kroppert")){
                posisjon0 = Posisjon.KROPPERT;
            }else if(posisjon.equalsIgnoreCase("Pikert")){
                posisjon0 = Posisjon.PIKERT;
            }else if(posisjon.equalsIgnoreCase("Strak")){
                posisjon0 = Posisjon.STRAK;
            }
        }else if(rotationTeller == 1){
            if(posisjon.equalsIgnoreCase("Kroppert")){
                posisjon1 = Posisjon.KROPPERT;
            }else if(posisjon.equalsIgnoreCase("Pikert")){
                posisjon1 = Posisjon.PIKERT;
            }else if(posisjon.equalsIgnoreCase("Strak")){
                posisjon1 = Posisjon.STRAK;
            }
        }else if(rotationTeller == 2){
            if(posisjon.equalsIgnoreCase("Kroppert")){
                posisjon2 = Posisjon.KROPPERT;
            }else if(posisjon.equalsIgnoreCase("Pikert")){
                posisjon2 = Posisjon.PIKERT;
            }else if(posisjon.equalsIgnoreCase("Strak")){
                posisjon2 = Posisjon.STRAK;
            }
        }else if(rotationTeller == 3){
            if(posisjon.equalsIgnoreCase("Kroppert")){
                posisjon3 = Posisjon.KROPPERT;
            }else if(posisjon.equalsIgnoreCase("Pikert")){
                posisjon3 = Posisjon.PIKERT;
            }else if(posisjon.equalsIgnoreCase("Strak")){
                posisjon3 = Posisjon.STRAK;
            }
        }

        if(rotationTeller == nrOfRotations){
            rotationTeller = 0;
            setUpSkruVerdier();
        }else {
            rotationTeller++;
            setUpPosition();
        }

    }

    public String getKorrektSkruVerdi(String skruInn){
        BigDecimal bD = new BigDecimal(skruInn);
        bD = bD.divide(new BigDecimal("360"));
        return bD.toString();
    }

    public void editTextNamePressed(View view){
        EditText name = (EditText) view;
        if(name.getText().toString().equalsIgnoreCase("Name")){
            name.setText("");
        }
    }
    public void editTextScorePressed(View view){
        EditText score = (EditText) view;
        if(score.getText().toString().equalsIgnoreCase("Value")){
            score.setText("");
        }
    }
    Toast toast;
    public void onFieldsDonePressed(View view){
        boolean nameSet = false;
        boolean scoreSet = false;
        EditText name = findViewById(R.id.nameBox);
        EditText score = findViewById(R.id.scoreBox);

        if(name.getText().toString().equals("") || name.getText().toString().equals(" ") || name.getText().toString().equalsIgnoreCase("Name")){
            if(toast == null){
                toast = new Toast(this);
                toast = Toast.makeText(this,"Please select a valid name",Toast.LENGTH_SHORT);
                toast.setDuration(Toast.LENGTH_SHORT);
                toast.show();
            }else {
                toast.cancel();
                toast = Toast.makeText(this,"Please select a valid name",Toast.LENGTH_SHORT);
                toast.show();
            }
        }else{
            nameSet = true;
        }

        if(!correctFormat(score.getText().toString())){
            if(toast == null){
                toast = new Toast(this);
                toast = Toast.makeText(this,"Please select a valid Value, make sure you use \".\" and not \",\"",Toast.LENGTH_SHORT);
                toast.show();
            }else {
                toast.cancel();
                toast = Toast.makeText(this,"Please select a valid Value, make sure you use \".\" and not \",\"",Toast.LENGTH_SHORT);
                toast.show();
            }
        }else{
            scoreSet = true;
        }

        if(nameSet && scoreSet){
            this.name = name.getText().toString();
            this.value = score.getText().toString();
            showSeries();
        }

    }

    private boolean correctFormat(String text) {
        boolean worked = true;
        try{
            Double.parseDouble(text);
        }catch (Exception e){
            worked =  false;
        }

        return worked;
    }
    public void onDoneNewSeriesPressedButton(View view){

        FileHandlerClass fileHandlerClass = new FileHandlerClass(this);


        HoppStringConverter hoppStringConverter = new HoppStringConverter(this);
        String hopp = hoppStringConverter.getStringFromGenereltHopp(genereltHopp);

        if (fileHandlerClass.doesSeriesExist(hopp)){
            if(toast == null){
                toast = new Toast(this);
                toast = Toast.makeText(this,"This series already exist in the custom series list",Toast.LENGTH_SHORT);
                toast.setDuration(Toast.LENGTH_SHORT);
                toast.show();
            }else {
                toast.cancel();
                toast = Toast.makeText(this,"This series already exist in the custom series list",Toast.LENGTH_SHORT);
                toast.show();
            }
        }else {
            fileHandlerClass.addCustomSeries(hopp);
            if(toast == null){
                toast = new Toast(this);
                toast = Toast.makeText(this,"Custom series Added",Toast.LENGTH_SHORT);
                toast.setDuration(Toast.LENGTH_SHORT);
                toast.show();
            }else {
                toast.cancel();
                toast = Toast.makeText(this,"Custom series Added",Toast.LENGTH_SHORT);
                toast.show();
            }

        }
        recreate();


    }


}
