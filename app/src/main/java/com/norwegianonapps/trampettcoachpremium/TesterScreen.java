package com.norwegianonapps.trampettcoachpremium;

import android.graphics.Color;
import android.graphics.Typeface;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class TesterScreen extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tester_screen);

        SerieManager sM = new SerieManager(this);
        sM.generateOverslag();
        sM.generateOverslagSaltoer();
        sM.generateOverslagDobbelSaltoer();
        sM.generateTSU();
        sM.generateDobbelTSU();
        List<MedHoppredskapSerie> medHoppredskapSerieList = sM.getMedHoppredskapList();

        SerieManager sM2 = new SerieManager(this);
        sM2.generateEnkleSaltoer();
        sM2.generateDobleSaltoer();
        sM2.generateTripleSaltoer();
        sM2.generateSpesialHopp();
        List<UtenHoppredskapSerie> utenHoppredskapSerieList = sM2.getUtenHoppredskapList();
        List<SpesialHopp> spesialHoppList = sM2.getSpesialHoppList();

        List<GenerelleHopp> generelleHopp = toGenerelleHopp(utenHoppredskapSerieList, spesialHoppList);
        Log.v("asdfizew","List size: " + generelleHopp.size());
        generelleHopp.addAll(toGenerelleHopp(medHoppredskapSerieList));
        Log.v("asdfizew","List size2: " + generelleHopp.size());

        LinearLayout sV = (LinearLayout) findViewById(R.id.linlayTest);
        List<String> namesholder = new ArrayList<>();
        Typeface typeface = null;
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.O) {
            typeface = getResources().getFont(R.font.turntegnregular);
        }else {
            typeface = Typeface.createFromAsset(getAssets(),  "fonts/turntegnregular.ttf");
        }
        //SpannableStringBuilder SS = new SpannableStringBuilder("আমারநல்வரவு");
        int i = 0;
        while(i < generelleHopp.size()){
            if(!checkNames(generelleHopp.get(i).getName(),namesholder)) {
                namesholder.add(generelleHopp.get(i).getName());
                //TextView tv = new TextView(this);
                // tv.setText(" xdc");
                TextView name = new TextView(this);

                TextView score = new TextView(this);
                score.setPadding(5,2,10,2);
                LinearLayout linearLayoutInside = new LinearLayout(this);
                linearLayoutInside.setOrientation(LinearLayout.HORIZONTAL);
                linearLayoutInside.setMinimumWidth(20);
                linearLayoutInside.setGravity(Gravity.RIGHT);
                //SS.setSpan(font, 0, medHoppredskapSerieList.get(i).getName().length(), Spannable.SPAN_INCLUSIVE_INCLUSIVE);
                name.setText(generelleHopp.get(i).getName());
                name.setTypeface(typeface);
                name.setAllCaps(false);
                name.setPadding(30,0,30,0);
                score.setText(generelleHopp.get(i).getVerdi().toString());
                linearLayoutInside.addView(name);
                linearLayoutInside.addView(score);
                sV.addView(linearLayoutInside);
                sV.setPadding(0,0,10,0);
            }
            i++;
        }

    }


    private List<GenerelleHopp> toGenerelleHopp(List<MedHoppredskapSerie> medHoppredskapSerieList) {
        List<GenerelleHopp> genList = new ArrayList<>();
        int i = 0;
        while(i < medHoppredskapSerieList.size()){
            genList.add(medHoppredskapSerieList.get(i));
            i++;
        }
        return genList;
    }
    private List<GenerelleHopp> toGenerelleHopp(List<UtenHoppredskapSerie> utenHoppredskapSerieList,List<SpesialHopp> spesialHoppList) {
        List<GenerelleHopp> genList = new ArrayList<>();
        int i = 0;
        while(i < spesialHoppList.size()){
            genList.add(spesialHoppList.get(i));
            i++;
        }
        i = 0;
        while(i < utenHoppredskapSerieList.size()){
            genList.add(utenHoppredskapSerieList.get(i));
            i++;
        }
        return genList;
    }

    public boolean checkNames(String name, List<String> nameList){
        int i = 0;
        while (i < nameList.size()){
            if(name.equals(nameList.get(i))){
                return true;
            }
            i++;
        }

        return false;
    }
}
