package com.norwegianonapps.trampettcoachpremium;

import android.content.Context;
import android.util.Log;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.List;


public class TilleggsVerdier {
    private TrampettTilleggsVerdier tTV;
    private HestTilleggsVerdier hTV;
    private String reglement;
    Context context;
    public TrampettTilleggsVerdier getTrampettTilleggsVerdier(){
        return tTV;
    }

    public HestTilleggsVerdier getHestTilleggsVerdier(){
        return hTV;
    }

    public TilleggsVerdier(Context context){
        this.context = context;
        FileHandlerClass fileHandlerClass = new FileHandlerClass(context);
        reglement = fileHandlerClass.getRuleset();
    }

    public void importValues() {
        String filnavn;
        if(reglement.equalsIgnoreCase("ngtf")){
            filnavn = "tilleggsverdier";
        }else if(reglement.equalsIgnoreCase("ueg")){
            filnavn = "tilleggsverdierueg";
        }else if(reglement.equalsIgnoreCase("island")){
            filnavn = "tilleggsverdierisland";
        }else {
            filnavn = "tilleggsverdier";
        }
        BufferedReader br;
        tTV = new TrampettTilleggsVerdier();
        hTV = new HestTilleggsVerdier();
        try {

            InputStream inputStream = context.getResources().openRawResource(
                    context.getResources().getIdentifier(filnavn,
                            "raw", context.getPackageName()));

            InputStreamReader read = new InputStreamReader(inputStream);
            br = new BufferedReader(read);

            //br = new BufferedReader(new FileReader("tilleggsVerdier.csv"));
            br.readLine();
            br.readLine();
            String line = br.readLine();
            tTV.addEnkelPikert(extractValue(line,1));
            tTV.addDobbelPikert(extractValue(line,2));
            tTV.addTrippelPikert(extractValue(line,3));
            hTV.addForoverOverslagPikert(extractValue(line,4));
            hTV.addForoverSaltoPikert(extractValue(line,5));
            hTV.addForoverDobbelSaltoPikert(extractValue(line,6));
            hTV.addBakoverHalvInnSaltoPikert(extractValue(line,7));
            hTV.addBakoverTSUPikert(extractValue(line,8));
            hTV.addBakoverTSUDobbelPikert(extractValue(line,9));

            line = br.readLine();
            tTV.addEnkelStrak(extractValue(line,1));
            tTV.addDobbelStrak(extractValue(line,2));
            tTV.addTrippelStrak(extractValue(line,3));

            hTV.addForoverOverslagStrak(extractValue(line,4));
            hTV.addForoverSaltoStrak(extractValue(line,5));
            hTV.addForoverDobbelSaltoStrak(extractValue(line,6));
            hTV.addBakoverHalvInnSaltoStrak(extractValue(line,7));
            hTV.addBakoverTSUStrak(extractValue(line,8));
            hTV.addBakoverTSUDobbelStrak(extractValue(line,9));

            line = br.readLine();
            tTV.addEnkelHalvHelSkru(extractValue(line,1));
            tTV.addDobbelHalvHelSkru(extractValue(line,2));
            tTV.addTrippelHalvHelSkru(extractValue(line,3));

            hTV.addForoverOverslagHalvHelSkru(extractValue(line,4));
            hTV.addForoverSaltoHalvHelSkru(extractValue(line,5));
            hTV.addForoverDobbelSaltoHalvHelSkru(extractValue(line,6));
            hTV.addBakoverHalvInnSaltoHalvHelSkru(extractValue(line,7));
            hTV.addBakoverTSUHalvHelSkru(extractValue(line,8));
            hTV.addBakoverTSUDobbelHalvHelSkru(extractValue(line,9));

            line = br.readLine();
            tTV.addEnkelHalvannenSkru(extractValue(line,1));
            tTV.addDobbelHalvannenSkru(extractValue(line,2));
            tTV.addTrippelHalvannenSkru(extractValue(line,3));

            hTV.addForoverOverslagHalvannenSkru(extractValue(line,4));
            hTV.addForoverSaltoHalvannenSkru(extractValue(line,5));
            hTV.addForoverDobbelSaltoHalvannenSkru(extractValue(line,6));
            hTV.addBakoverHalvInnSaltoHalvannenSkru(extractValue(line,7));
            hTV.addBakoverTSUHalvannenSkru(extractValue(line,8));
            hTV.addBakoverTSUDobbelHalvannenSkru(extractValue(line,9));

            line = br.readLine();
            tTV.addEnkelDobbelSkru(extractValue(line,1));
            tTV.addDobbelDobbelSkru(extractValue(line,2));
            tTV.addTrippelDobbelSkru(extractValue(line,3));

            hTV.addForoverOverslagDobbelSkru(extractValue(line,4));
            hTV.addForoverSaltoDobbelSkru(extractValue(line,5));
            hTV.addForoverDobbelSaltoDobbelSkru(extractValue(line,6));
            hTV.addBakoverHalvInnSaltoDobbelSkru(extractValue(line,7));
            hTV.addBakoverTSUDobbelSkru(extractValue(line,8));
            hTV.addBakoverTSUDobbelDobbelSkru(extractValue(line,9));

            line = br.readLine();
            tTV.addEnkelTrippelSkru(extractValue(line,1));
            tTV.addDobbelTrippelSkru(extractValue(line,2));
            tTV.addTrippelTrippelSkru(extractValue(line,3));

            hTV.addForoverOverslagTrippelSkru(extractValue(line,4));
            hTV.addForoverSaltoTrippelSkru(extractValue(line,5));
            hTV.addForoverDobbelSaltoTrippelSkru(extractValue(line,6));
            hTV.addBakoverHalvInnSaltoTrippelSkru(extractValue(line,7));
            hTV.addBakoverTSUTrippelSkru(extractValue(line,8));
            hTV.addBakoverTSUDobbelTrippelSkru(extractValue(line,9));

            br.close();

        }catch(Exception e) {
            Log.v("dfgfsdgha","crashed: ");
            e.printStackTrace();
        }
    }

    public String extractValue(String line, int placement) {
        int i = 0;
        int teller = 0;
        boolean first = false;
        String lineOut = "";
        while(i < line.length()) {
            if(line.charAt(i) == ',') {
                teller++;
            }
            if(teller == placement) {

                if(!first) {
                    i++;
                    first = true;
                }
                lineOut = lineOut + line.charAt(i);
            }
            i++;
        }
        return lineOut;
    }


    public void printValues() {
        List<String> trampettList = tTV.getAllValues();
        List<String> hestList = hTV.getAllValues();
        //	System.out.println("Trampettlist size: "+trampettList.size());
        System.out.println("Navn             ,        Trampett - Enkel         ,        Trampett - Dobbel        ,        Trampett - Trippel       ,   Pegasus - Forover - Overslag  ,    Pegasus - Forover - Salto    , Pegasus - Forover - Dobbel Salto,   Pegasus - Bakover - Halv inn  ,     Pegasus - Bakover - TSU     ,  Pegasus - Bakover - Dobbel TSU ");

        System.out.print("Pikert(Per Salto),               ");
        int i = 0;
        while(i < 3) {
            String temp = trampettList.get(i);
            if(temp.contains("-")) {
                temp =  "  " + temp;
                temp = temp + " ";
            }else if(temp.length() < 4) {
                temp = temp + "0";
            }
            System.out.print(temp);
            System.out.print("              ,               ");
            i++;
        }
        int ii = 0;
        while(ii < 6) {
            String temp = hestList.get(ii);
            if(temp.contains("-")) {
                temp =  "  " + temp;
                temp = temp + " ";
            }else if(temp.length() < 4) {
                temp = temp + "0";
            }
            System.out.print(temp);
            if(ii != 5) {
                System.out.print("              ,               ");
            }
            ii++;
        }
        System.out.println();
        System.out.print("Strak(Per Salto) ,               ");
        while(i < 6) {
            String temp = trampettList.get(i);
            if(temp.contains("-")) {
                temp =  "  " + temp;
                temp = temp + " ";
            }else if(temp.length() < 4) {
                temp = temp + "0";
            }
            System.out.print(temp);
            System.out.print("              ,               ");
            i++;
        }
        while(ii < 12) {
            String temp = hestList.get(ii);
            if(temp.contains("-")) {
                temp =  "  " + temp;
                temp = temp + " ";
            }else if(temp.length() < 4) {
                temp = temp + "0";
            }
            System.out.print(temp);
            if(ii != 11) {
                System.out.print("              ,               ");
            }
            ii++;
        }
        System.out.println();
        System.out.print("0.5-1.0 skru     ,               ");
        while(i < 9) {
            String temp = trampettList.get(i);
            if(temp.contains("-")) {
                temp =  "  " + temp;
                temp = temp + " ";
            }else if(temp.length() < 4) {
                temp = temp + "0";
            }
            System.out.print(temp);
            System.out.print("              ,               ");
            i++;
        }
        while(ii < 18) {
            String temp = hestList.get(ii);
            if(temp.contains("-")) {
                temp =  "  " + temp;
                temp = temp + " ";
            }else if(temp.length() < 4) {
                temp = temp + "0";
            }
            System.out.print(temp);
            if(ii != 17) {
                System.out.print("              ,               ");
            }
            ii++;
        }
        System.out.println();
        System.out.print("1.5 skru         ,               ");
        while(i < 12) {
            String temp = trampettList.get(i);
            if(temp.contains("-")) {
                temp =  "  " + temp;
                temp = temp + " ";
            }else if(temp.length() < 4) {
                temp = temp + "0";
            }
            System.out.print(temp);
            System.out.print("              ,               ");
            i++;
        }
        while(ii < 24) {
            String temp = hestList.get(ii);
            if(temp.contains("-")) {
                temp =  "  " + temp;
                temp = temp + " ";
            }else if(temp.length() < 4) {
                temp = temp + "0";
            }
            System.out.print(temp);
            if(ii != 23) {
                System.out.print("              ,               ");
            }
            ii++;
        }
        System.out.println();
        System.out.print("2.0+ skru        ,               ");
        while(i < 15) {
            String temp = trampettList.get(i);
            if(temp.contains("-")) {
                temp =  "  " + temp;
                temp = temp + " ";
            }else if(temp.length() < 4) {
                temp = temp + "0";
            }
            System.out.print(temp);
            System.out.print("              ,               ");
            i++;
        }
        while(ii < 30) {
            String temp = hestList.get(ii);
            if(temp.contains("-")) {
                temp =  "  " + temp;
                temp = temp + " ";
            }else if(temp.length() < 4) {
                temp = temp + "0";
            }
            System.out.print(temp);
            if(ii != 29) {
                System.out.print("              ,               ");
            }
            ii++;
        }
        System.out.println();


    }


}
