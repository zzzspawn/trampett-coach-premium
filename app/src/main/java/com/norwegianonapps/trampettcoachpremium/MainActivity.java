package com.norwegianonapps.trampettcoachpremium;

import android.content.Intent;
import android.os.CountDownTimer;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import java.util.List;

public class MainActivity extends AppCompatActivity {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        //setContentView(R.layout.activity_splash);

        findViewById(R.id.pegaKnapp).setEnabled(false);
        findViewById(R.id.settingsKnapp).setEnabled(false);
        findViewById(R.id.Logo).setVisibility(View.VISIBLE);
        findViewById(R.id.VersionText).setVisibility(View.GONE);
        findViewById(R.id.LogoBackground).setVisibility(View.VISIBLE);
        findViewById(R.id.trampettKnapp).setEnabled(false);

        new CountDownTimer(2000, 1000) { // 5000 = 5 sec

            public void onTick(long millisUntilFinished) {
            }

            public void onFinish() {
                findViewById(R.id.Logo).setVisibility(View.GONE);
                findViewById(R.id.LogoBackground).setVisibility(View.GONE);
                findViewById(R.id.VersionText).setVisibility(View.GONE);
                findViewById(R.id.pegaKnapp).setEnabled(true);
                findViewById(R.id.trampettKnapp).setEnabled(true);
                findViewById(R.id.settingsKnapp).setEnabled(true);
            }
        }.start();

        setLanguages();
    }
    @Override
    public void onResume()
    {  // After a pause OR at startup
        super.onResume();
        //Refresh your stuff here
        setLanguages();
    }


    public void trampValgtPressed(View view){
        Intent i=new Intent(this,ValgSkjerm.class);
        i.putExtra("valgt", "utenhoppredskap");
        startActivity(i);

    }

    public void pegaValgtPressed(View view){
        Intent i=new Intent(this,ValgSkjerm.class);
        i.putExtra("valgt", "medhoppredskap");
        startActivity(i);
    }

    public void onSettingsPressed(View view){
        Intent i=new Intent(this,Settings.class);
        startActivity(i);
    }


    public void setLanguages(){
        Button settingsButton = findViewById(R.id.settingsKnapp);
        Button trampKnapp = findViewById(R.id.trampettKnapp);
        Button pegaKnapp = findViewById(R.id.pegaKnapp);

        TextClassGetter tCG = new TextClassGetter(this);
        String[] array = tCG.getText("MainActivity");

        settingsButton.setText(array[0]);
        trampKnapp.setText(array[1]);
        pegaKnapp.setText(array[2]);
    }



}
