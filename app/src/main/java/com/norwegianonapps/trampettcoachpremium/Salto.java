package com.norwegianonapps.trampettcoachpremium;

import java.math.BigDecimal;

public class Salto  extends Hopp{
    private BigDecimal skru;
    private Enum<Posisjon> posisjon;

    public Salto(Enum<Posisjon> posisjon,BigDecimal skru){

        this.posisjon = posisjon;
//        if(posisjon == Posisjon.PIKERT){
//            if(skru.compareTo(new BigDecimal("0.5")) > 0){
//                this.skru = new BigDecimal("0.5");
//                System.out.println("kan ikke sette mer enn 180 grader skru på pikerte saltoer");
//            }else {
//                this.skru = skru;
//            }
//        } else {
        this.skru = skru;
//        }
    }
    public Salto(Enum<Posisjon> posisjon){
        this.skru = new BigDecimal("0");
        this.posisjon = posisjon;
    }
    public BigDecimal getSkru(){
        return skru;
    }
    public Enum<Posisjon> getPosisjon() {
        return posisjon;
    }

}

