package com.norwegianonapps.trampettcoachpremium;

import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import java.math.BigDecimal;
import java.util.List;

public class FinalScore extends AppCompatActivity {
    String valgtEn;
    String valgtTo;
    String valgtTre;
    String valgtFire;
    String valgtFem;
    String valgtSeks;
    String hoppredskap;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_final_score);
        Intent intent = getIntent();
        valgtEn = intent.getStringExtra("valgtEn");
        valgtTo = intent.getStringExtra("valgtTo");
        valgtTre = intent.getStringExtra("valgtTre");
        valgtFire = intent.getStringExtra("valgtFire");
        valgtFem = intent.getStringExtra("valgtFem");
        valgtSeks = intent.getStringExtra("valgtSeks");
        hoppredskap = intent.getStringExtra("hoppRedskap");
        Log.v("finalScore","hoppRedskap = " + hoppredskap);
        kjoer();
    }
    public void kjoer(){
        TextClassGetter tCG = new TextClassGetter(this);
        String[] array = tCG.getText("FinalScore");

        if(hoppredskap.equals("hest")){
            java.math.BigDecimal score1 = new java.math.BigDecimal("0.0");
            java.math.BigDecimal score2 = new java.math.BigDecimal("0.0");
            java.math.BigDecimal score3 = new java.math.BigDecimal("0.0");
            java.math.BigDecimal score4 = new java.math.BigDecimal("0.0");
            java.math.BigDecimal score5 = new java.math.BigDecimal("0.0");
            java.math.BigDecimal score6 = new java.math.BigDecimal("0.0");
            SerieManager sM = new SerieManager(this);
            sM.generateOverslag();
            sM.generateOverslagSaltoer();
            sM.generateOverslagDobbelSaltoer();
            sM.generateTSU();
            sM.generateDobbelTSU();
            List<MedHoppredskapSerie> medHoppredskapSerieList = sM.getMedHoppredskapList();
            MedHoppredskapSerie serie1 = null;
            MedHoppredskapSerie serie2 = null;
            MedHoppredskapSerie serie3 = null;
            MedHoppredskapSerie serie4 = null;
            MedHoppredskapSerie serie5 = null;
            MedHoppredskapSerie serie6 = null;

            int counter  = 0;
            int i = 0;
            while (i < medHoppredskapSerieList.size()){
                if(!(valgtEn.equals("NA"))) {
                    if (medHoppredskapSerieList.get(i).getName().equals(valgtEn)) {
                        score1 = medHoppredskapSerieList.get(i).getVerdi();
                        serie1 = medHoppredskapSerieList.get(i);
                        counter++;
                    }
                }
                if(!(valgtTo.equals("NA"))) {
                    if (medHoppredskapSerieList.get(i).getName().equals(valgtTo)) {
                        score2 = medHoppredskapSerieList.get(i).getVerdi();
                        serie2 = medHoppredskapSerieList.get(i);
                        counter++;
                    }
                }

                if(!(valgtTre.equals("NA"))) {
                    if (medHoppredskapSerieList.get(i).getName().equals(valgtTre)) {
                        score3 = medHoppredskapSerieList.get(i).getVerdi();
                        serie3 = medHoppredskapSerieList.get(i);
                        counter++;
                    }
                }

                if(!(valgtFire.equals("NA"))) {
                    if (medHoppredskapSerieList.get(i).getName().equals(valgtFire)) {
                        score4 = medHoppredskapSerieList.get(i).getVerdi();
                        serie4 = medHoppredskapSerieList.get(i);
                        counter++;
                    }
                }

                if(!(valgtFem.equals("NA"))) {
                    if (medHoppredskapSerieList.get(i).getName().equals(valgtFem)) {
                        score5 = medHoppredskapSerieList.get(i).getVerdi();
                        serie5 = medHoppredskapSerieList.get(i);
                        counter++;
                    }
                }

                if(!(valgtSeks.equals("NA"))) {
                    if (medHoppredskapSerieList.get(i).getName().equals(valgtSeks)) {
                        score6 = medHoppredskapSerieList.get(i).getVerdi();
                        serie6 = medHoppredskapSerieList.get(i);
                        counter++;
                    }
                }

                i++;
            }

            boolean done = false;
            while(!done){
                done = true;

                if(serie1 == null && serie2 != null){
                    done = false;
                    serie1 = serie2;
                    serie2 = null;
                }

                if(serie2 == null && serie3 != null){
                    done = false;
                    serie2 = serie3;
                    serie3 = null;
                }

                if(serie3 == null && serie4 != null){
                    done = false;
                    serie3 = serie4;
                    serie4 = null;
                }

                if(serie4 == null && serie5 != null){
                    done = false;
                    serie4 = serie5;
                    serie5 = null;
                }

                if(serie5 == null && serie6 != null){
                    done = false;
                    serie5 = serie6;
                    serie6 = null;
                }
            }


            FileHandlerClass fileHandlerClass = new FileHandlerClass(this);
            TextView changeTv = findViewById(R.id.change);

            int numberOfSeries = Integer.parseInt(fileHandlerClass.getNumberOfSeries());
            String changeOccurred = fileHandlerClass.getChangeOccurred();
            Log.v("sgfgvfnbb","counter, number of series: " + counter + " , " + fileHandlerClass.getNumberOfSeries());
            Log.v("sgfgvfnbb","changeOccurred: " + fileHandlerClass.getChangeOccurred());
            if(changeOccurred.equals("true")){
                if(counter < numberOfSeries){
                    Log.v("sgfgvfnbb","removed series");
                    TextClassGetter tCGg = new TextClassGetter(this);
                    String[] textarray = tCG.getText("FinalScore");
                    if(toast != null) {
                        toast.cancel();
                    }
                    toast = Toast.makeText(this,textarray[10],Toast.LENGTH_SHORT);
                    toast.show();


                }else if(counter > numberOfSeries){
                    Log.v("sgfgvfnbb","removed series");
                    TextClassGetter tCGg = new TextClassGetter(this);
                    String[] textarray = tCG.getText("FinalScore");
                    if(toast != null) {
                        toast.cancel();
                    }
                    toast = Toast.makeText(this,textarray[11],Toast.LENGTH_SHORT);
                    toast.show();
                }
            }else {
                fileHandlerClass.saveChangeOccurred("true");
            }
            fileHandlerClass.saveNumberOfSeries(Integer.toString(counter));

            GenerelleHopp s1 = serie1;
            GenerelleHopp s2 = serie2;
            GenerelleHopp s3 = serie3;
            GenerelleHopp s4 = serie4;
            GenerelleHopp s5 = serie5;
            GenerelleHopp s6 = serie6;
            GenerelleHopp[] tempArray = sorterSerier(s1,s2,s3,s4,s5,s6);


            if(tempArray == null){
                tempArray = new GenerelleHopp[1];
                tempArray[0] = null;

                score1 = new BigDecimal("0.0");
                score2 = new BigDecimal("0.0");
                score3 = new BigDecimal("0.0");
                score4 = new BigDecimal("0.0");
                score5 = new BigDecimal("0.0");
                score6 = new BigDecimal("0.0");

                s1 = null;
                s2 = null;
                s3 = null;
                s4 = null;
                s5 = null;
                s6 = null;
            }else {
                s1 = tempArray[0];
                s2 = tempArray[1];
                s3 = tempArray[2];
                s4 = tempArray[3];
                s5 = tempArray[4];
                s6 = tempArray[5];
            }



            java.math.BigDecimal finalScore = score1.add(score2);
            finalScore = finalScore.add(score3);
            finalScore = finalScore.add(score4);
            finalScore = finalScore.add(score5);
            finalScore = finalScore.add(score6);

            Log.v("finalScore","Score: " + finalScore.toString());

            //her sette du fonten jani :)
            //den øverste e den i res folderen, og den nederste e den i Assets folderen
            Typeface typeface = null;
            if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.O) {
                typeface = getResources().getFont(R.font.turntegnregular);
            }else {
                typeface = Typeface.createFromAsset(getAssets(),  "fonts/turntegnregular.ttf");
            }

            TextView serieTV1 = (TextView) findViewById(R.id.serieEn);
            TextView serieTV1Symbol = (TextView) findViewById(R.id.serieEnSymbol);
            serieTV1Symbol.setTypeface(typeface);
            TextView serieTV2 = (TextView) findViewById(R.id.serieTo);
            TextView serieTV2Symbol = (TextView) findViewById(R.id.serieToSymbol);
            serieTV2Symbol.setTypeface(typeface);
            TextView serieTV3 = (TextView) findViewById(R.id.serieTre);
            TextView serieTV3Symbol = (TextView) findViewById(R.id.serieTreSymbol);
            serieTV3Symbol.setTypeface(typeface);

            TextView serieTV4 = (TextView) findViewById(R.id.serieFire);
            TextView serieTV4Symbol = (TextView) findViewById(R.id.serieFireSymbol);
            serieTV4Symbol.setTypeface(typeface);
            TextView serieTV5 = (TextView) findViewById(R.id.serieFem);
            TextView serieTV5Symbol = (TextView) findViewById(R.id.serieFemSymbol);
            serieTV5Symbol.setTypeface(typeface);
            TextView serieTV6 = (TextView) findViewById(R.id.serieSeks);
            TextView serieTV6Symbol = (TextView) findViewById(R.id.serieSeksSymbol);
            serieTV6Symbol.setTypeface(typeface);
            TextView totalVerdiTV = (TextView) findViewById(R.id.totalVerdi);

            //skrive bare forklaring på første, men fungere likt i alle nerover, typ s1, s2, s3
            String textForTV = null;
            if(s1 != null) {
                textForTV = array[0] + s1.getVerdi().toString(); //her e ka som står i den der d står "Value, et eller aent.."
                serieTV1.setText(textForTV);
                serieTV1Symbol.setText(s1.getName()); //her sette du symbolene, med fonten, desom du vil legga t noge for testing, så kan du legga inn (s1.getName() + "skriv noge tekst her"), evt fær
                serieTV1.setVisibility(View.VISIBLE);
                serieTV1Symbol.setVisibility(View.VISIBLE);
            }else{
                serieTV1.setVisibility(View.GONE);
                serieTV1Symbol.setVisibility(View.GONE);
            }

            if(s2 != null) {
                textForTV = array[1] + s2.getVerdi().toString();
                serieTV2.setText(textForTV);
                serieTV2Symbol.setText(s2.getName());
                serieTV2.setVisibility(View.VISIBLE);
                serieTV2Symbol.setVisibility(View.VISIBLE);
            }else{
                serieTV2.setVisibility(View.GONE);
                serieTV2Symbol.setVisibility(View.GONE);
            }
            if(s3 != null) {
                textForTV = array[2] + s3.getVerdi().toString();
                serieTV3.setText(textForTV);
                serieTV3Symbol.setText(s3.getName());
                serieTV3.setVisibility(View.VISIBLE);
                serieTV3Symbol.setVisibility(View.VISIBLE);
            }else{
                serieTV3.setVisibility(View.GONE);
                serieTV3Symbol.setVisibility(View.GONE);
            }
            if(s4 != null) {
                textForTV = array[3] + s4.getVerdi().toString();
                serieTV4.setText(textForTV);
                serieTV4Symbol.setText(s4.getName());
                serieTV4.setVisibility(View.VISIBLE);
                serieTV4Symbol.setVisibility(View.VISIBLE);
            }else{
                serieTV4.setVisibility(View.GONE);
                serieTV4Symbol.setVisibility(View.GONE);
            }
            if(s5 != null) {
                textForTV = array[4] + s5.getVerdi().toString();
                serieTV5.setText(textForTV);
                serieTV5Symbol.setText(s5.getName());
                serieTV5.setVisibility(View.VISIBLE);
                serieTV5Symbol.setVisibility(View.VISIBLE);
            }else{
                serieTV5.setVisibility(View.GONE);
                serieTV5Symbol.setVisibility(View.GONE);
            }
            if(s6 != null) {
                textForTV = array[5] + s6.getVerdi().toString();
                serieTV6.setText(textForTV);
                serieTV6Symbol.setText(s6.getName());
                serieTV6.setVisibility(View.VISIBLE);
                serieTV6Symbol.setVisibility(View.VISIBLE);
            }else{
                serieTV6.setVisibility(View.GONE);
                serieTV6Symbol.setVisibility(View.GONE);
            }

            textForTV = array[6] + finalScore.toString();
            totalVerdiTV.setText(textForTV);

        }else if(hoppredskap.equals("tramp")){

            //her settes fonten for vanlig trampett, så på samme måde som på hest :)
            Typeface typeface = null;
            if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.O) {
                typeface = getResources().getFont(R.font.turntegnregular);
            }else {
                typeface = Typeface.createFromAsset(getAssets(),  "fonts/turntegnregular.ttf");
            }

            java.math.BigDecimal score1 = new java.math.BigDecimal("0.0");
            java.math.BigDecimal score2 = new java.math.BigDecimal("0.0");
            java.math.BigDecimal score3 = new java.math.BigDecimal("0.0");
            java.math.BigDecimal score4 = new java.math.BigDecimal("0.0");
            java.math.BigDecimal score5 = new java.math.BigDecimal("0.0");
            java.math.BigDecimal score6 = new java.math.BigDecimal("0.0");
            SerieManager sM = new SerieManager(this);
            sM.generateSpesialHopp();
            sM.generateEnkleSaltoer();
            sM.generateDobleSaltoer();
            sM.generateTripleSaltoer();


            List<UtenHoppredskapSerie> utenHoppredskapSerieList = sM.getUtenHoppredskapList();
            List<SpesialHopp> spesialHoppList = sM.getSpesialHoppList();

            UtenHoppredskapSerie serie1 = null;
            UtenHoppredskapSerie serie2 = null;
            UtenHoppredskapSerie serie3 = null;
            UtenHoppredskapSerie serie4 = null;
            UtenHoppredskapSerie serie5 = null;
            UtenHoppredskapSerie serie6 = null;

            SpesialHopp sSerie1 = null;
            SpesialHopp sSerie2 = null;
            SpesialHopp sSerie3 = null;
            SpesialHopp sSerie4 = null;
            SpesialHopp sSerie5 = null;
            SpesialHopp sSerie6 = null;

            int counter = 0;
            int i = 0;
            while (i < utenHoppredskapSerieList.size()){
                if(!(valgtEn.equals("NA"))) {
                    if (utenHoppredskapSerieList.get(i).getName().equals(valgtEn)) {
                        score1 = utenHoppredskapSerieList.get(i).getVerdi();
                        Log.v("dfgkjødafgnjkø ", "match 1.score: " + score1.toString());

                        serie1 = utenHoppredskapSerieList.get(i);
                        Log.v("dfgkjødafgnjkø ", "match 1: " + serie1.getName());
                        counter++;
                    }
                }else {
                    serie1 = null;
                }
                if(!(valgtTo.equals("NA"))) {
                    if (utenHoppredskapSerieList.get(i).getName().equals(valgtTo)) {
                        score2 = utenHoppredskapSerieList.get(i).getVerdi();
                        serie2 = utenHoppredskapSerieList.get(i);
                        counter++;
                    }
                }
                if(!(valgtTre.equals("NA"))) {
                    if (utenHoppredskapSerieList.get(i).getName().equals(valgtTre)) {
                        score3 = utenHoppredskapSerieList.get(i).getVerdi();
                        serie3 = utenHoppredskapSerieList.get(i);
                        counter++;
                    }
                }

                if(!(valgtFire.equals("NA"))) {
                    if (utenHoppredskapSerieList.get(i).getName().equals(valgtFire)) {
                        score4 = utenHoppredskapSerieList.get(i).getVerdi();
                        serie4 = utenHoppredskapSerieList.get(i);
                        counter++;
                    }
                }

                if(!(valgtFem.equals("NA"))) {
                    if (utenHoppredskapSerieList.get(i).getName().equals(valgtFem)) {
                        score5 = utenHoppredskapSerieList.get(i).getVerdi();
                        serie5 = utenHoppredskapSerieList.get(i);
                        counter++;
                    }
                }

                if(!(valgtSeks.equals("NA"))) {
                    if (utenHoppredskapSerieList.get(i).getName().equals(valgtSeks)) {
                        score6 = utenHoppredskapSerieList.get(i).getVerdi();
                        serie6 = utenHoppredskapSerieList.get(i);
                        counter++;
                    }
                }
                i++;
            }

            i = 0;
            while (i < spesialHoppList.size()){
                if(!(valgtEn.equals("NA"))) {
                    if (spesialHoppList.get(i).getName().equals(valgtEn)) {
                        score1 = spesialHoppList.get(i).getVerdi();
                        sSerie1 = spesialHoppList.get(i);
                        counter++;
                    }
                }
                if(!(valgtTo.equals("NA"))) {
                    if (spesialHoppList.get(i).getName().equals(valgtTo)) {
                        score2 = spesialHoppList.get(i).getVerdi();
                        sSerie2 = spesialHoppList.get(i);
                        counter++;
                    }
                }
                if(!(valgtTre.equals("NA"))) {
                    if (spesialHoppList.get(i).getName().equals(valgtTre)) {
                        score3 = spesialHoppList.get(i).getVerdi();
                        sSerie3 = spesialHoppList.get(i);
                        counter++;
                    }
                }

                if(!(valgtFire.equals("NA"))) {
                    if (spesialHoppList.get(i).getName().equals(valgtFire)) {
                        score4 = spesialHoppList.get(i).getVerdi();
                        sSerie4 = spesialHoppList.get(i);
                        counter++;
                    }
                }

                if(!(valgtFem.equals("NA"))) {
                    if (spesialHoppList.get(i).getName().equals(valgtFem)) {
                        score5 = spesialHoppList.get(i).getVerdi();
                        sSerie5 = spesialHoppList.get(i);
                        counter++;
                    }
                }

                if(!(valgtSeks.equals("NA"))) {
                    if (spesialHoppList.get(i).getName().equals(valgtSeks)) {
                        score6 = spesialHoppList.get(i).getVerdi();
                        sSerie6 = spesialHoppList.get(i);
                        counter++;
                    }
                }
                i++;
            }

            boolean done = false;
            while(!done){
                done = true;

                if(serie1 == null && sSerie1 == null && serie2 != null){
                    done = false;
                    serie1 = serie2;
                    serie2 = null;
                }else if (serie1 == null && sSerie1 == null && sSerie2 != null){
                    done = false;
                    sSerie1 = sSerie2;
                    sSerie2 = null;
                }

                if(serie2 == null && sSerie2 == null && serie3 != null){
                    done = false;
                    serie2 = serie3;
                    serie3 = null;
                }else if (serie2 == null && sSerie2 == null && sSerie3 != null){
                    done = false;
                    sSerie2 = sSerie3;
                    sSerie3 = null;
                }

                if(serie3 == null && sSerie3 == null && serie4 != null){
                    done = false;
                    serie3 = serie4;
                    serie4 = null;
                }else if (serie3 == null && sSerie3 == null && sSerie4 != null){
                    done = false;
                    sSerie3 = sSerie4;
                    sSerie4 = null;
                }

                if(serie4 == null && sSerie4 == null && serie5 != null){
                    done = false;
                    serie4 = serie5;
                    serie5 = null;
                }else if (serie4 == null && sSerie4 == null && sSerie5 != null){
                    done = false;
                    sSerie4 = sSerie5;
                    sSerie5 = null;
                }

                if(serie5 == null && sSerie5 == null && serie6 != null){
                    done = false;
                    serie5 = serie6;
                    serie6 = null;
                }else if (serie5 == null && sSerie5 == null && sSerie6 != null){
                    done = false;
                    sSerie5 = sSerie6;
                    sSerie6 = null;
                }
            }


            FileHandlerClass fileHandlerClass = new FileHandlerClass(this);
            TextView changeTv = findViewById(R.id.change);

            int numberOfSeries = Integer.parseInt(fileHandlerClass.getNumberOfSeries());
            String changeOccurred = fileHandlerClass.getChangeOccurred();
            Log.v("sgfgvfnbb","counter, number of series: " + counter + " , " + fileHandlerClass.getNumberOfSeries());
            Log.v("sgfgvfnbb","changeOccurred: " + fileHandlerClass.getChangeOccurred());
            if(changeOccurred.equals("true")){
                if(counter < numberOfSeries){
                    Log.v("sgfgvfnbb","removed series");
                    TextClassGetter tCGg = new TextClassGetter(this);
                    String[] textarray = tCG.getText("FinalScore");
                    if(toast != null) {
                        toast.cancel();
                    }
                    toast = Toast.makeText(this,textarray[10],Toast.LENGTH_SHORT);
                    toast.show();


                }else if(counter > numberOfSeries){
                    Log.v("sgfgvfnbb","removed series");
                    TextClassGetter tCGg = new TextClassGetter(this);
                    String[] textarray = tCG.getText("FinalScore");
                    if(toast != null) {
                        toast.cancel();
                    }
                    toast = Toast.makeText(this,textarray[11],Toast.LENGTH_SHORT);
                    toast.show();
                }
            }else {
                fileHandlerClass.saveChangeOccurred("true");
            }
            fileHandlerClass.saveNumberOfSeries(Integer.toString(counter));




            GenerelleHopp s1 = null;
            GenerelleHopp s2 = null;
            GenerelleHopp s3 = null;
            GenerelleHopp s4 = null;
            GenerelleHopp s5 = null;
            GenerelleHopp s6 = null;

            if(serie1 != null){
                Log.v("dfgkjødafgnjkø ", "1: " + serie1.getName());
                s1 = serie1;
            }else if(sSerie1 != null){
                Log.v("dfgkjødafgnjkø ", "1: " + sSerie1.getName());
                s1 = sSerie1;
            }
            if(serie2 != null){
                Log.v("dfgkjødafgnjkø ", "2: " + serie2.getName());
                s2 = serie2;
            }else if(sSerie2 != null){
                Log.v("dfgkjødafgnjkø ", "2: " + sSerie2.getName());
                s2 = sSerie2;
            }
            if(serie3 != null){
                s3 = serie3;
                Log.v("dfgkjødafgnjkø ", "3: " + serie3.getName());
            }else if(sSerie3 != null){
                Log.v("dfgkjødafgnjkø ", "3: " + sSerie3.getName());
                s3 = sSerie3;
            }
            if(serie4 != null){
                Log.v("dfgkjødafgnjkø ", "4: " + serie4.getName());
                s4 = serie4;
            }else if(sSerie4 != null){
                Log.v("dfgkjødafgnjkø ", "4: " + sSerie4.getName());
                s4 = sSerie4;
            }
            if(serie5 != null){
                Log.v("dfgkjødafgnjkø ", "5: " + serie5.getName());
                s5 = serie5;
            }else if(sSerie5 != null){
                Log.v("dfgkjødafgnjkø ", "6: " + sSerie5.getName());
                s5 = sSerie5;
            }
            if(serie6 != null){
                Log.v("dfgkjødafgnjkø ", "6: " + serie6.getName());
                s6 = serie6;
            }else if(sSerie6 != null){
                Log.v("dfgkjødafgnjkø ", "6: " + sSerie6.getName());
                s6 = sSerie6;
            }



            GenerelleHopp[] tempArray = sorterSerier(s1,s2,s3,s4,s5,s6);
            if(tempArray == null){
                tempArray = new GenerelleHopp[1];
                tempArray[0] = null;

                score1 = new BigDecimal("0.0");
                score2 = new BigDecimal("0.0");
                score3 = new BigDecimal("0.0");
                score4 = new BigDecimal("0.0");
                score5 = new BigDecimal("0.0");
                score6 = new BigDecimal("0.0");

                s1 = null;
                s2 = null;
                s3 = null;
                s4 = null;
                s5 = null;
                s6 = null;
            }else {
                s1 = tempArray[0];
                s2 = tempArray[1];
                s3 = tempArray[2];
                s4 = tempArray[3];
                s5 = tempArray[4];
                s6 = tempArray[5];
            }

            java.math.BigDecimal finalScore = score1.add(score2);
            finalScore = finalScore.add(score3);
            finalScore = finalScore.add(score4);
            finalScore = finalScore.add(score5);
            finalScore = finalScore.add(score6);

            Log.v("finalScore","Score: " + finalScore.toString());

            TextView serieTV1 = (TextView) findViewById(R.id.serieEn);
            //serieTV1.setTypeface(typeface);
            TextView serieTV1Symbol = (TextView) findViewById(R.id.serieEnSymbol);
            serieTV1Symbol.setTypeface(typeface);
            TextView serieTV2 = (TextView) findViewById(R.id.serieTo);
            // serieTV2.setTypeface(typeface);
            TextView serieTV2Symbol = (TextView) findViewById(R.id.serieToSymbol);
            serieTV2Symbol.setTypeface(typeface);
            TextView serieTV3 = (TextView) findViewById(R.id.serieTre);
            //serieTV3.setTypeface(typeface);
            TextView serieTV3Symbol = (TextView) findViewById(R.id.serieTreSymbol);
            serieTV3Symbol.setTypeface(typeface);

            TextView serieTV4 = (TextView) findViewById(R.id.serieFire);
            //serieTV4.setTypeface(typeface);
            TextView serieTV4Symbol = (TextView) findViewById(R.id.serieFireSymbol);
            serieTV4Symbol.setTypeface(typeface);

            TextView serieTV5 = (TextView) findViewById(R.id.serieFem);
            //serieTV5.setTypeface(typeface);
            TextView serieTV5Symbol = (TextView) findViewById(R.id.serieFemSymbol);
            serieTV5Symbol.setTypeface(typeface);

            TextView serieTV6 = (TextView) findViewById(R.id.serieSeks);
            //serieTV6.setTypeface(typeface);
            TextView serieTV6Symbol = (TextView) findViewById(R.id.serieSeksSymbol);
            serieTV6Symbol.setTypeface(typeface);

            TextView totalVerdiTV = (TextView) findViewById(R.id.totalVerdi);


            //her settes teksten for vanlig trampett :)
            if(s1 != null) {
                String textForTV =array[0] + s1.getVerdi().toString();
                serieTV1Symbol.setText(s1.getName());
                serieTV1.setText(textForTV);
                serieTV1.setVisibility(View.VISIBLE);
                serieTV1Symbol.setVisibility(View.VISIBLE);
            }else {
                serieTV1.setVisibility(View.GONE);
                serieTV1Symbol.setVisibility(View.GONE);
            }

            if(s2 != null) {
                String textForTV = array[1] + s2.getVerdi().toString();
                serieTV2.setText(textForTV);
                serieTV2Symbol.setText(s2.getName());
                serieTV2.setVisibility(View.VISIBLE);
                serieTV2Symbol.setVisibility(View.VISIBLE);
            }else {
                serieTV2.setVisibility(View.GONE);
                serieTV2Symbol.setVisibility(View.GONE);
            }

            if(s3 != null) {
                String textForTV = array[2] + s3.getVerdi().toString();
                serieTV3.setText(textForTV);
                serieTV3Symbol.setText(s3.getName());
                serieTV3.setVisibility(View.VISIBLE);
                serieTV3Symbol.setVisibility(View.VISIBLE);
            }else {
                serieTV3.setVisibility(View.GONE);
                serieTV3Symbol.setVisibility(View.GONE);
            }

            if(s4 != null) {
                String textForTV = array[3] + s4.getVerdi().toString();
                serieTV4.setText(textForTV);
                serieTV4Symbol.setText(s4.getName());
                serieTV4.setVisibility(View.VISIBLE);
                serieTV4Symbol.setVisibility(View.VISIBLE);
            }else {
                serieTV4.setVisibility(View.GONE);
                serieTV4Symbol.setVisibility(View.GONE);
            }

            if(s5 != null) {
                String textForTV = array[4] + s5.getVerdi().toString();
                serieTV5.setText(textForTV);
                serieTV5Symbol.setText(s5.getName());
                serieTV5.setVisibility(View.VISIBLE);
                serieTV5Symbol.setVisibility(View.VISIBLE);
            }else {
                serieTV5.setVisibility(View.GONE);
                serieTV5Symbol.setVisibility(View.GONE);
            }

            if(s6 != null) {
                String textForTV = array[5] + s6.getVerdi().toString();
                serieTV6.setText(textForTV);
                serieTV6Symbol.setText(s6.getName());
                serieTV6.setVisibility(View.VISIBLE);
                serieTV6Symbol.setVisibility(View.VISIBLE);
            }else {
                serieTV6.setVisibility(View.GONE);
                serieTV6Symbol.setVisibility(View.GONE);
            }

            String textForTV = array[6] + finalScore.toString();
            totalVerdiTV.setText(textForTV);

        }

        TextView textViewRuleset = findViewById(R.id.rulesetTVFinalScore);
        textViewRuleset.setText(array[7]);

        Button backButton = findViewById(R.id.backButtonFinalScore);
        backButton.setText(array[8]);



    }

    Toast toast;
    private GenerelleHopp[] sorterSerier(GenerelleHopp serie1, GenerelleHopp serie2, GenerelleHopp serie3, GenerelleHopp serie4, GenerelleHopp serie5, GenerelleHopp serie6) {
        //Log.v("asfdderfg","Serier: 1: " + serie1.getName() + " 2:" + serie2.getName() + " 3:" + serie3.getName());

        boolean removedSeries = false;

        if(serie1 != null && serie1.getVerdi() == null){
            Log.v("sgfgvfnbb","removed series1");
            serie1 = null;
            removedSeries = true;
        }
        if(serie2 != null && serie2.getVerdi() == null){
            Log.v("sgfgvfnbb","removed series2");
            serie2 = null;
            removedSeries = true;
        }
        if(serie3 != null && serie3.getVerdi() == null){
            Log.v("sgfgvfnbb","removed series3");
            serie3 = null;
            removedSeries = true;
        }
        if(serie4 != null && serie4.getVerdi() == null){
            Log.v("sgfgvfnbb","removed series4");
            serie4 = null;
            removedSeries = true;
        }
        if(serie5 != null && serie5.getVerdi() == null){
            Log.v("sgfgvfnbb","removed series5");
            serie5 = null;
            removedSeries = true;
        }
        if(serie6 != null && serie6.getVerdi() == null){
            Log.v("sgfgvfnbb","removed series6");
            serie6 = null;
            removedSeries = true;
        }

        if(removedSeries){
            Log.v("sgfgvfnbb","removed series");
            TextClassGetter tCG = new TextClassGetter(getApplicationContext());
            String[] array = tCG.getText("FinalScore");
            if(toast != null){
                toast.cancel();
                toast = Toast.makeText(this,array[9],Toast.LENGTH_SHORT);
                toast.show();
            }else {
                toast = Toast.makeText(this,array[9],Toast.LENGTH_SHORT);
                toast.show();
            }
        }

        GenerelleHopp[] array = null;
        boolean sorted = false;
        if(serie6 == null && serie5 != null){
            array = new GenerelleHopp[5];
            array[0] = serie1;
            array[1] = serie2;
            array[2] = serie3;
            array[3] = serie4;
            array[4] = serie5;
        }else if(serie5 == null && serie4 != null){
            array = new GenerelleHopp[4];
            array[0] = serie1;
            array[1] = serie2;
            array[2] = serie3;
            array[3] = serie4;
        }else if(serie4 == null && serie3 != null){
            array = new GenerelleHopp[3];
            array[0] = serie1;
            array[1] = serie2;
            array[2] = serie3;
        }else if(serie3 == null && serie2 != null){
            array = new GenerelleHopp[2];
            array[0] = serie1;
            array[1] = serie2;
        }else if(serie2 == null && serie1 != null){
            array = new GenerelleHopp[1];
            array[0] = serie1;
            sorted = true;
        }else if(serie1 == null && serie2 == null && serie3 == null && serie4 == null && serie5 == null && serie6 == null){
            return null;
        }else {
            array = new GenerelleHopp[6];
            array[0] = serie1;
            array[1] = serie2;
            array[2] = serie3;
            array[3] = serie4;
            array[4] = serie5;
            array[5] = serie6;
        }

        int i = 0;
        while (!sorted) {
            sorted = true;
            if(array.length > 1) {
                if (array[0].getVerdi().compareTo(array[1].getVerdi()) == 1) {
                    sorted = false;
                    GenerelleHopp temp = array[0];
                    array[0] = array[1];
                    array[1] = temp;
                }
            }
            if(array.length > 2) {
                if (array[1].getVerdi().compareTo(array[2].getVerdi()) == 1) {
                    sorted = false;
                    GenerelleHopp temp = array[1];
                    array[1] = array[2];
                    array[2] = temp;
                }
            }
            if(array.length > 3) {
                if (array[2].getVerdi().compareTo(array[3].getVerdi()) == 1) {
                    sorted = false;
                    GenerelleHopp temp = array[2];
                    array[2] = array[3];
                    array[3] = temp;
                }
            }
            if(array.length > 4) {
                if (array[3].getVerdi().compareTo(array[4].getVerdi()) == 1) {
                    sorted = false;
                    GenerelleHopp temp = array[3];
                    array[3] = array[4];
                    array[4] = temp;
                }
            }
            if(array.length > 5) {
                if (array[4].getVerdi().compareTo(array[5].getVerdi()) == 1) {
                    sorted = false;
                    GenerelleHopp temp = array[4];
                    array[4] = array[5];
                    array[5] = temp;
                }
            }
            if(i < array.length-1){
                i++;
            }else {
                i = 0;
            }
        }
        i = 0;
        if(array.length > 1){
        sorted = false;
        }
        while(!sorted){
            sorted = true;
            if(array.length > 1) {
                if (array[0].getAntallRotasjoner() > array[1].getAntallRotasjoner()) {
                    sorted = false;
                    GenerelleHopp temp = array[0];
                    array[0] = array[1];
                    array[1] = temp;
                }
            }
            if(array.length > 2) {
                if (array[1].getAntallRotasjoner() > array[2].getAntallRotasjoner()) {
                    sorted = false;
                    GenerelleHopp temp = array[1];
                    array[1] = array[2];
                    array[2] = temp;
                }
            }
            if(array.length > 3) {
                if (array[2].getAntallRotasjoner() > array[3].getAntallRotasjoner()) {
                    sorted = false;
                    GenerelleHopp temp = array[2];
                    array[2] = array[3];
                    array[3] = temp;
                }
            }
            if(array.length > 4) {
                if (array[3].getAntallRotasjoner() > array[4].getAntallRotasjoner()) {
                    sorted = false;
                    GenerelleHopp temp = array[3];
                    array[3] = array[4];
                    array[4] = temp;
                }
            }
            if(array.length > 5) {
                if (array[4].getAntallRotasjoner() > array[5].getAntallRotasjoner()) {
                    sorted = false;
                    GenerelleHopp temp = array[4];
                    array[4] = array[5];
                    array[5] = temp;
                }
            }

            if(i < array.length-1){
                i++;
            }else {
                i = 0;
            }
        }

        GenerelleHopp[] normalisert = new GenerelleHopp[6];
        if(array.length < 6){
            int ii = 0;
            while (ii < array.length){
                normalisert[ii] = array[ii];
                ii++;
            }

        }else {
            normalisert = array;
        }

        return normalisert;
    }


    public void buttonBackPressed(View view){
        onBackPressed();
    }

    public void onChangeRuleset(View view){
        Intent i = new Intent(this, RulesetSettingsAlternate.class);
            i.putExtra("valgtEn", valgtEn);
            i.putExtra("valgtTo", valgtTo);
            i.putExtra("valgtTre", valgtTre);
            i.putExtra("valgtFire", valgtFire);
            i.putExtra("valgtFem", valgtFem);
            i.putExtra("valgtSeks", valgtSeks);
            i.putExtra("hoppRedskap", hoppredskap);
            startActivityForResult(i,1);
    }
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 1) {
            if(resultCode == RESULT_OK) {
                //String strEditText = data.getStringExtra("editTextValue");
                valgtEn = data.getStringExtra("valgtEn");
                valgtTo = data.getStringExtra("valgtTo");
                valgtTre = data.getStringExtra("valgtTre");
                valgtFire = data.getStringExtra("valgtFire");
                valgtFem = data.getStringExtra("valgtFem");
                valgtSeks = data.getStringExtra("valgtSeks");
                hoppredskap = data.getStringExtra("hoppRedskap");

                Log.v("dfgkjødafgnjkø", "Kom tilbake med resultater, kjører kjoer igjen");

                kjoer();
            }
        }
    }

}
