package com.norwegianonapps.trampettcoachpremium;

import java.math.BigDecimal;

public class HestHopp extends Hopp{
    private Enum<Retning> retning;
    private BigDecimal skru;
    private Enum<Posisjon> posisjon;


    public HestHopp(Posisjon posisjon, BigDecimal skru,Enum<Retning> retning){
        this.retning = retning;
        this.posisjon = posisjon;
//        if(posisjon == Posisjon.PIKERT){
//            if(skru.compareTo(new BigDecimal("0.5")) > 0){
//                this.skru = new BigDecimal("0.5");
//                System.out.println("kan ikke sette mer enn 180 grader skru på pikerte saltoer");
//            }else {
//                this.skru = skru;
//            }
//        }else {
        this.skru = skru;
//        }
    }

    public HestHopp(Enum<Posisjon> posisjon,Enum<Retning> retning){
        this.retning = retning;
        this.skru = new BigDecimal("0.0");
        this.posisjon = posisjon;
    }

    public BigDecimal getSkru(){
        return skru;
    }
    public Enum<Posisjon> getPosisjon() {
        return posisjon;
    }
    public Enum<Retning> getRetning() {
        return retning;
    }


}
