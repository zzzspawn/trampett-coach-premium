package com.norwegianonapps.trampettcoachpremium;
import android.content.Context;

import java.math.BigDecimal;

public class MedHoppredskapSerie  extends GenerelleHopp{

    private HestHopp firstRotation;
    private HestHopp secondRotation;
    private HestHopp thirdRotation;
    private TilleggsVerdier tV;
    private BaseVerdier bv;
    private HestTilleggsVerdier hTV;
    private String name;
    private BigDecimal verdi;
    private Enum<Retning> retning;
    Context context;
    private String hoppredskap;

    public Enum<Retning> getRetning(){
        return retning;
    }

    private BigDecimal skruEn;
    private BigDecimal skruTo;
    private BigDecimal skruTre;

    public BigDecimal getSkruEn(){
        if(firstRotation != null) {
            return firstRotation.getSkru();
        }else {
            return null;
        }
    }
    public BigDecimal getSkruT0(){
        if(secondRotation != null) {
            return secondRotation.getSkru();
        }else {
            return null;
        }
    }
    public BigDecimal getSkruTre(){
        if(thirdRotation != null) {
            return thirdRotation.getSkru();
        }else {
            return null;
        }
    }

    @Override
    public String getHoppredskap() {
        return hoppredskap;
    }

    public BigDecimal getVerdi(){
        return verdi;
    }

    @Override
    public Enum<Posisjon> getPosisjonEn(){
        if(firstRotation != null){
            return firstRotation.getPosisjon();
        }else {
            return null;
        }
    }
    @Override
    public Enum<Posisjon> getPosisjonTo(){
        if(secondRotation != null){
            return secondRotation.getPosisjon();
        }else {
            return null;
        }
    }
    @Override
    public Enum<Posisjon> getPosisjonTre(){
        if(thirdRotation != null){
            return thirdRotation.getPosisjon();
        }else {
            return null;
        }
    }

    public MedHoppredskapSerie(Context context,HestHopp firstRotation){
        hoppredskap = "pegasus";
        this.context = context;
        this.firstRotation = firstRotation;
        settVerdi();
        settNavn();
        settAntallRotasjoner();
    }
    public MedHoppredskapSerie(Context context,HestHopp firstRotation, HestHopp secondRotation){
        hoppredskap = "pegasus";
        this.context = context;
        this.firstRotation = firstRotation;
        this.secondRotation = secondRotation;
        settVerdi();
        settNavn();
        settAntallRotasjoner();
    }
    public MedHoppredskapSerie(Context context,HestHopp firstRotation, HestHopp secondRotation, HestHopp thirdRotation){
        hoppredskap = "pegasus";
        this.context = context;
        this.firstRotation = firstRotation;
        this.secondRotation = secondRotation;
        this.thirdRotation = thirdRotation;
        settVerdi();
        settNavn();
        settAntallRotasjoner();
    }


    public void settNavn(){
        name = "";
        retning = firstRotation.getRetning();
        int antallRotasjoner = 0;
        if(firstRotation != null){
            antallRotasjoner++;
        }
        if(secondRotation != null){
            antallRotasjoner++;
        }
        if(thirdRotation != null){
            antallRotasjoner++;
        }

        if(antallRotasjoner == 1) {
            if (retning == Retning.FOROVER) {
                name = name + "u ";
                if(firstRotation.getSkru().compareTo(new BigDecimal(0)) != 0){
                    name = name + "v" + getSkru(firstRotation) + "";
                }else{
                    name = name + "v";
                }
            }else if (retning == Retning.BAKOVER){
                name = name + "y";
                if(firstRotation.getSkru().compareTo(new BigDecimal(0)) != 0){
                    name = name + "" + getSkru(firstRotation) + "";
                }else{
                    name = name + "";
                }
            }

        }else if(antallRotasjoner == 2) {
            if (retning == Retning.FOROVER) {
                name = name + "u ";
                name = name + getPosisjonName(secondRotation);


            }else if (retning == Retning.BAKOVER){
                name = name + "t ";
                name = name + getPosisjonName(secondRotation);
            }
            if(secondRotation.getSkru().compareTo(new BigDecimal(0))  != 0){
                name = name + "" + getSkru(secondRotation) + "";
            }
        }else if(antallRotasjoner == 3) {
            if (retning == Retning.FOROVER) {
                name = name + "u ";
                name = name + getPosisjonName(secondRotation);
                if(secondRotation.getSkru().compareTo(new BigDecimal(0))  != 0){
                    name = name + getSkru(secondRotation) + "";
                }
                name = name + getPosisjonName(thirdRotation);
                if(thirdRotation.getSkru().compareTo(new BigDecimal(0))  != 0){
                    name = name + getSkru(thirdRotation) + "";
                }
            }else if (retning == Retning.BAKOVER){
                name = name + "t ";
                name = name + getPosisjonName(secondRotation);
                if(secondRotation.getSkru().compareTo(new BigDecimal(0))  != 0){
                    name = name + getSkru(secondRotation) + "";
                }
                name = name + getPosisjonName(thirdRotation);
                if(thirdRotation.getSkru().compareTo(new BigDecimal(0))  != 0){
                    name = name + getSkru(thirdRotation) + "";
                }
            }

        }

    }
    public BigDecimal floatToDegrees(BigDecimal tall){
        return tall.multiply(new BigDecimal("360")).setScale(0);
    }

    public String getSkru(HestHopp salto){
        return floatToDegrees(salto.getSkru()) + "\u00B0 ";
    }

    public String getPosisjonName(HestHopp salto){
        if(salto.getPosisjon() == Posisjon.KROPPERT){
            if(retning == Retning.FOROVER) {
                return "k";
            }else {
                return "K";
            }
        }else if(salto.getPosisjon() == Posisjon.STRAK){
            if(retning == Retning.FOROVER) {
                return  "s";
            }else {
                return  "S";
            }
        }else if(salto.getPosisjon() == Posisjon.PIKERT){
            if(retning == Retning.FOROVER) {
                return "p";
            }else {
                return "P";
            }
        }
        return "feil";
    }


    public void settVerdi(){
        verdi = new BigDecimal("0.0");
        BigDecimal antallSkru = new BigDecimal("0.0");
        tV = new TilleggsVerdier(context);
        tV.importValues();
        bv = new BaseVerdier(context);
        bv.importValues();
        hTV = tV.getHestTilleggsVerdier();
        int antallSalto = 0;
        if(firstRotation != null){
            antallSalto++;
        }
        if(secondRotation != null){
            antallSalto++;
        }
        if(thirdRotation != null){
            antallSalto++;
        }
        BigDecimal bm = null;
        if(firstRotation.getRetning() == Retning.FOROVER) {

            if (antallSalto == 1) {
                bm = new BigDecimal(bv.getOverslagVerdi());
                verdi = verdi.add(bm);
                //verdi = verdi + getTilleggSaltoVerdi(firstRotation.getPosisjon(), 1, Retning.FOROVER);
                antallSkru = antallSkru.add(firstRotation.getSkru());
                verdi = verdi.add(getSkruVerdi(antallSkru, antallSalto, Retning.FOROVER));
            } else if (antallSalto == 2) {
                String totalVerdi = "";
                bm = new BigDecimal(bv.getOverslagSaltoVerdi());
                verdi =  verdi.add(bm);
                //skru greier her
                antallSkru = firstRotation.getSkru();
                verdi = verdi.add(getSkruVerdi(antallSkru, antallSalto, Retning.FOROVER));
                verdi =  verdi.add(getTilleggSaltoVerdi(secondRotation.getPosisjon(), antallSalto, Retning.FOROVER));
                verdi =  verdi.add(getTilleggSaltoVerdi(secondRotation.getPosisjon(), antallSalto, Retning.FOROVER));
                //skru greier her
                antallSkru = secondRotation.getSkru();
                verdi = verdi.add(getSkruVerdi(antallSkru, antallSalto, Retning.FOROVER));
            } else if (antallSalto == 3) {
                bm = new BigDecimal(bv.getOverslagDobbelSaltoVerdi());
                verdi = verdi.add(bm);
                antallSkru = firstRotation.getSkru();
                verdi = verdi.add(getSkruVerdi(antallSkru, antallSalto, Retning.FOROVER));
                verdi = verdi.add(getTilleggSaltoVerdi(secondRotation.getPosisjon(), antallSalto, Retning.FOROVER));
                verdi = verdi.add(getTilleggSaltoVerdi(secondRotation.getPosisjon(), antallSalto, Retning.FOROVER));
                antallSkru = secondRotation.getSkru();
                verdi = verdi.add(getSkruVerdi(antallSkru, antallSalto, Retning.FOROVER));
                verdi = verdi.add(getTilleggSaltoVerdi(thirdRotation.getPosisjon(), antallSalto, Retning.FOROVER));
                antallSkru = thirdRotation.getSkru();
                verdi = verdi.add(getSkruVerdi(antallSkru, antallSalto, Retning.FOROVER));
            }
        }else if(firstRotation.getRetning() == Retning.BAKOVER) {
            if (antallSalto == 1) {
                bm = new BigDecimal(bv.getOverslag90Pa90AvVerdi());
                verdi = verdi.add(bm);
                //verdi = verdi + getTilleggSaltoVerdi(firstRotation.getPosisjon(), 1, Retning.FOROVER);
                antallSkru = firstRotation.getSkru();
                verdi = verdi.add(getSkruVerdi(antallSkru, antallSalto, Retning.BAKOVER));
            } else if (antallSalto == 2) {
                bm = new BigDecimal(bv.getTsukaharaVerdi());
                verdi =  verdi.add(bm);
                //skru greier her
                antallSkru = firstRotation.getSkru();
                verdi = verdi.add(getSkruVerdi(antallSkru, antallSalto,  Retning.BAKOVER));
                verdi = verdi.add(getTilleggSaltoVerdi(secondRotation.getPosisjon(), antallSalto, Retning.BAKOVER));
                verdi = verdi.add(getTilleggSaltoVerdi(secondRotation.getPosisjon(), antallSalto, Retning.BAKOVER));
                //skru greier her
                antallSkru = secondRotation.getSkru();
                verdi = verdi.add(getSkruVerdi(antallSkru, antallSalto,  Retning.BAKOVER));
            } else if (antallSalto == 3) {
                bm = new BigDecimal(bv.getDobbelTsukaharaVerdi());
                verdi = verdi.add(bm);
                antallSkru = firstRotation.getSkru();
                verdi = verdi.add(getSkruVerdi(antallSkru, antallSalto, Retning.BAKOVER));
                verdi = verdi.add(getTilleggSaltoVerdi(secondRotation.getPosisjon(), antallSalto, Retning.BAKOVER));
                verdi = verdi.add(getTilleggSaltoVerdi(secondRotation.getPosisjon(), antallSalto, Retning.BAKOVER));
                antallSkru = secondRotation.getSkru();
                verdi = verdi.add(getSkruVerdi(antallSkru, antallSalto, Retning.BAKOVER));
                verdi = verdi.add(getTilleggSaltoVerdi(thirdRotation.getPosisjon(), antallSalto, Retning.BAKOVER));
                antallSkru = thirdRotation.getSkru();
                verdi = verdi.add(getSkruVerdi(antallSkru, antallSalto, Retning.BAKOVER));
            }
        }
    }

    public BigDecimal getSkruVerdi(BigDecimal antallSkru,int rotasjoner, Enum<Retning> retning){
        FileHandlerClass fileHandlerClass = new FileHandlerClass(context);
        BigDecimal skruVerdi = new BigDecimal(0.0f);
        BigDecimal helHalv = new BigDecimal(0.0f);
        BigDecimal halvannen = new BigDecimal(0.0f);
        BigDecimal dobbelSkru = new BigDecimal(0.0f);
        BigDecimal trippelSkru = new BigDecimal(0.0f);
        if(rotasjoner == 1) {
            if (fileHandlerClass.getRuleset().equalsIgnoreCase("ngtf")){
                if(retning == Retning.FOROVER) {
                    helHalv = new BigDecimal(hTV.getForoverOverslagHalvHelSkru());
                    halvannen = new BigDecimal(hTV.getForoverOverslagHalvannenSkru());
                    dobbelSkru = new BigDecimal(hTV.getForoverOverslagDobbelSkru());
                    trippelSkru = new BigDecimal(hTV.getForoverOverslagTrippelSkru());
                }else if (retning == Retning.BAKOVER){
                    helHalv = new BigDecimal(hTV.getBakoverHalvInnSaltoHalvHelSkru());
                    halvannen = new BigDecimal(hTV.getBakoverHalvInnSaltoHalvannenSkru());
                    dobbelSkru = new BigDecimal(hTV.getBakoverHalvInnSaltoDobbelSkru());
                    trippelSkru = new BigDecimal(hTV.getBakoverHalvInnSaltoTrippelSkru());
                }
            }
        }else if(rotasjoner == 2) {
            if(retning == Retning.FOROVER) {
                helHalv = new BigDecimal(hTV.getForoverSaltoHalvHelSkru());
                halvannen = new BigDecimal(hTV.getForoverSaltoHalvannenSkru());
                dobbelSkru = new BigDecimal(hTV.getForoverSaltoDobbelSkru());
                trippelSkru = new BigDecimal(hTV.getForoverSaltoTrippelSkru());
            }else if(retning == Retning.BAKOVER) {
                helHalv = new BigDecimal(hTV.getBakoverTSUHalvHelSkru());
                halvannen = new BigDecimal(hTV.getBakoverTSUHalvannenSkru());
                dobbelSkru = new BigDecimal(hTV.getBakoverTSUDobbelSkru());
                trippelSkru = new BigDecimal(hTV.getBakoverTSUTrippelSkru());
            }
        }else if(rotasjoner == 3) {
            if(retning == Retning.FOROVER) {
                helHalv = new BigDecimal(hTV.getForoverDobbelSaltoHalvHelSkru());
                halvannen = new BigDecimal(hTV.getForoverDobbelSaltoHalvannenSkru());
                dobbelSkru = new BigDecimal(hTV.getForoverDobbelSaltoDobbelSkru());
                trippelSkru = new BigDecimal(hTV.getForoverDobbelSaltoTrippelSkru());
            }else if(retning == Retning.BAKOVER) {
                helHalv = new BigDecimal(hTV.getBakoverTSUDobbelHalvHelSkru());
                halvannen = new BigDecimal(hTV.getBakoverTSUDobbelHalvannenSkru());
                dobbelSkru = new BigDecimal(hTV.getBakoverTSUDobbelDobbelSkru());
                trippelSkru = new BigDecimal(hTV.getBakoverTSUDobbelTrippelSkru());
            }
        }
        BigDecimal divider = new BigDecimal("0.5");
        BigDecimal result = antallSkru.divide(divider);
        int teller = result.intValueExact();

        int i = 0;
        if(fileHandlerClass.getRuleset().equalsIgnoreCase("ngtf")) {
            while (i < teller) {
                if (i < 2) {
                    skruVerdi = skruVerdi.add(helHalv);
                }
                if (i > 1 && i < 3) {
                    skruVerdi = skruVerdi.add(halvannen);
                }
                if (i > 2) {
                    skruVerdi = skruVerdi.add(dobbelSkru);
                }
                i++;
            }
        }else if(fileHandlerClass.getRuleset().equalsIgnoreCase("ueg")){
            while(i < teller) {
                if(i < 2) {
                    skruVerdi = skruVerdi.add(helHalv);

                }
                if(i > 1 && i < 3) {
                    skruVerdi = skruVerdi.add(halvannen);

                }
                if(i > 2 && i < 5) {
                    skruVerdi = skruVerdi.add(dobbelSkru);

                }if(i > 4) {
                    if ( !((i & 1) == 0 )) {
                        skruVerdi = skruVerdi.add(trippelSkru);
                    }
                }
                i++;
            }
        }
        return skruVerdi;
    }

    public BigDecimal getTilleggSaltoVerdi(Enum<Posisjon> posisjon, int antallRotasjoner, Retning retning){
        FileHandlerClass fileHandlerClass = new FileHandlerClass(context);
        BigDecimal verdi = new BigDecimal("0.0");
        if(antallRotasjoner == 1){
            return new BigDecimal("0.0");
        }else if(antallRotasjoner == 2){
            if(retning == Retning.FOROVER){
                if(posisjon == Posisjon.KROPPERT){
                }else if(posisjon == Posisjon.PIKERT){
                    verdi = verdi.add(new BigDecimal(hTV.getForoverSaltoPikert()));
                }else if(posisjon == Posisjon.STRAK){
                    verdi = verdi.add(new BigDecimal(hTV.getForoverSaltoStrak()));
                }
            }else if (retning == Retning.BAKOVER) {

                if (posisjon == Posisjon.KROPPERT) {
                } else if (posisjon == Posisjon.PIKERT) {
                    verdi = verdi.add(new BigDecimal(hTV.getBakoverTSUPikert()));
                } else if (posisjon == Posisjon.STRAK) {
                    verdi = verdi.add(new BigDecimal(hTV.getBakoverTSUStrak()));
                }

            }
        }else if(antallRotasjoner == 3){
            if(retning == Retning.FOROVER){
                if(posisjon == Posisjon.KROPPERT){
                }else if(posisjon == Posisjon.PIKERT){
                    verdi = verdi.add(new BigDecimal(hTV.getForoverDobbelSaltoPikert()));
                }else if(posisjon == Posisjon.STRAK){
                    if (fileHandlerClass.getRuleset().equalsIgnoreCase("ngtf")) {
                        verdi = verdi.add(new BigDecimal(hTV.getForoverDobbelSaltoStrak()));
                    }else if(fileHandlerClass.getRuleset().equalsIgnoreCase("ueg")){
                        verdi = verdi.add(new BigDecimal("0.0"));
                    }
                }
            }else if (retning == Retning.BAKOVER){
                if(posisjon == Posisjon.KROPPERT){
                }else if(posisjon == Posisjon.PIKERT){
                    verdi = verdi.add(new BigDecimal(hTV.getBakoverTSUDobbelPikert()));
                }else if(posisjon == Posisjon.STRAK){
                    if (fileHandlerClass.getRuleset().equalsIgnoreCase("ngtf")) {
                        verdi = verdi.add(new BigDecimal(hTV.getBakoverTSUDobbelStrak()));
                    }else if(fileHandlerClass.getRuleset().equalsIgnoreCase("ueg")){
                        verdi = verdi.add(new BigDecimal("0.0"));
                    }
                }
            }
        }
        return verdi;
    }

    public String getName() {
        return name;
    }

    public void settAntallRotasjoner(){

        int teller = 0;
        if(firstRotation != null){
            teller++;
        }
        if (secondRotation != null){
            teller++;
        }
        if(thirdRotation != null){
            teller++;
        }

        antallRotasjoner = teller;

    }
}
