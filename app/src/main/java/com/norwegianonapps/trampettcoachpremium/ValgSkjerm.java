package com.norwegianonapps.trampettcoachpremium;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Typeface;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

public class ValgSkjerm extends AppCompatActivity {
    int antallValgt;
    int maxValgbare;
    String[] buttonColors;
    String[] buttonText;
    Context context;
    List<Integer> buttonIds;
    int[] valgtArray;
    String buttonColor;
    Button doneButton;
    Button clearLastButton;
    Button clearAllButton;
    String valgtApparat;
    String valgt;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_valg_skjerm);
        Intent intent = getIntent();
        valgt = intent.getStringExtra("valgt");
        runner();
    }

    @Override
    protected void onResume() {
        super.onResume();
        LinearLayout sV = (LinearLayout) findViewById(R.id.listView);
        sV.removeAllViews();
        runner();
    }

    private void runner(){
        buttonColors = new String[] {"#9e9e9e","#c9c9c9","#cecece","#FFFFFF"};
        buttonText = new String[] {" a"," b"," c"," d"," e"," f"};
        context = getApplicationContext();

        FileHandlerClass fileHandlerClass = new FileHandlerClass(this);
        fileHandlerClass.saveChangeOccurred("false");
        fileHandlerClass.saveNumberOfSeries("0");

        setLanguages();
        antallValgt = 0;
        maxValgbare = 6;
        valgtArray = new int[maxValgbare];
        valgtArray[0] = 0;
        valgtArray[1] = 0;
        valgtArray[2] = 0;
        valgtArray[3] = 0;
        valgtArray[4] = 0;
        valgtArray[5] = 0;
        buttonIds = new ArrayList<>();
        //buttonColor = getResources().getColor(R.color.colorBackground);
        buttonColor = "#ffffff";
        doneButton = (Button) findViewById(R.id.buttonSelectScreen);
        clearLastButton = findViewById(R.id.clearLast);
        clearAllButton = findViewById(R.id.clearAll);
        doneButton.setEnabled(false);
        clearLastButton.setEnabled(false);
        clearAllButton.setEnabled(false);
        valgtApparat = "";
        if(valgt.equals("medhoppredskap")){
            valgtApparat = "hest";
            SerieManager sM = new SerieManager(this);
            sM.generateOverslag();
            sM.generateOverslagSaltoer();
            sM.generateOverslagDobbelSaltoer();
            sM.generateTSU();
            sM.generateDobbelTSU();
            List<MedHoppredskapSerie> medHoppredskapSerieList = sM.getMedHoppredskapList();
            List<GenerelleHopp> generelleHoppList = toGenerelleHopp(medHoppredskapSerieList);
            populateButtons(generelleHoppList);
        }else if(valgt.equals("utenhoppredskap")){
            valgtApparat = "tramp";
            SerieManager sM = new SerieManager(this);
            sM.generateEnkleSaltoer();
            sM.generateDobleSaltoer();
            sM.generateTripleSaltoer();
            sM.generateSpesialHopp();
            List<UtenHoppredskapSerie> utenHoppredskapSerieList = sM.getUtenHoppredskapList();
            List<SpesialHopp> spesialHoppList = sM.getSpesialHoppList();
            List<GenerelleHopp> generelleHoppList = toGenerelleHopp(utenHoppredskapSerieList,spesialHoppList);
            populateButtons(generelleHoppList);
        }


    }


    private List<GenerelleHopp> toGenerelleHopp(List<MedHoppredskapSerie> medHoppredskapSerieList) {
        List<GenerelleHopp> genList = new ArrayList<>();
        int i = 0;
        while(i < medHoppredskapSerieList.size()){
            genList.add(medHoppredskapSerieList.get(i));
            i++;
        }
        return genList;
    }
    private List<GenerelleHopp> toGenerelleHopp(List<UtenHoppredskapSerie> utenHoppredskapSerieList,List<SpesialHopp> spesialHoppList) {
        List<GenerelleHopp> genList = new ArrayList<>();
        int i = 0;
        while(i < spesialHoppList.size()){
            genList.add(spesialHoppList.get(i));
            i++;
        }
        i = 0;
        while(i < utenHoppredskapSerieList.size()){
            genList.add(utenHoppredskapSerieList.get(i));
            i++;
        }
        return genList;
    }


    View.OnClickListener clicks = new View.OnClickListener() {

        @Override
        public void onClick(View v) {
            if(antallValgt < maxValgbare) {
                Button b = (Button) v;
                String text = removeAfffix(b.getText().toString());
                String affix = getAffix(b.getText().toString());
                //b.setBackgroundColor(Color.parseColor(buttonColors[antallValgt]));
                String textHolder = removeAfffix(b.getText().toString());
                //textHolder = textHolder+buttonText[antallValgt];
                Log.v("asdsdgf","prev affix: " + affix);
                String nyAffix = getNewAffixPluss(affix);
                Log.v("asdsdgf","ny affix: " + affix);
                Log.v("asdsdgf","text: " + textHolder);
                b.setText(textHolder+nyAffix);

                valgtArray[antallValgt] = b.getId();
                if(antallValgt == 0){
                    //Button b1 = (Button) findViewById(R.id.hopp1);
                    //b1.setText(text);
                    //b1.setEnabled(true);
                }else if(antallValgt == 1){
                    //Button b2 = (Button) findViewById(R.id.hopp2);
                    //b2.setText(text);
                    //b2.setEnabled(true);
                }else if(antallValgt == 2){
                    //Button b3 = (Button) findViewById(R.id.hopp3);
                    //b3.setText(text);
                    //b3.setEnabled(true);
                }

                doneButton.setEnabled(true);
                clearLastButton.setEnabled(true);
                clearAllButton.setEnabled(true);
                antallValgt++;
                if(antallValgt == 1){
                    b.setBackgroundColor(Color.parseColor(buttonColors[2]));
                }else if(antallValgt == 2) {
                    b.setBackgroundColor(Color.parseColor(buttonColors[2]));
                    if (valgtArray[0] == valgtArray[1]) {
                        b.setBackgroundColor(Color.parseColor(buttonColors[2]));
                    } else {
                        Button b2 = (Button) findViewById(valgtArray[0]);
                        b2.setBackgroundColor(Color.parseColor(buttonColor));
                    }
                    b.setBackgroundColor(Color.parseColor(buttonColors[2]));
                }else if(antallValgt == 3){
                    b.setBackgroundColor(Color.parseColor(buttonColors[2]));
                    if (valgtArray[1] == valgtArray[2]) {
                        b.setBackgroundColor(Color.parseColor(buttonColors[2]));
                    }else if(valgtArray[0] == valgtArray[2]){
                        b.setBackgroundColor(Color.parseColor(buttonColors[2]));
                        Button b2 = (Button) findViewById(valgtArray[1]);
                        b2.setBackgroundColor(Color.parseColor(buttonColor));
                    }else {
                        Button b2 = (Button) findViewById(valgtArray[0]);
                        b2.setBackgroundColor(Color.parseColor(buttonColor));
                        Button b3 = (Button) findViewById(valgtArray[1]);
                        b3.setBackgroundColor(Color.parseColor(buttonColor));
                    }
                    b.setBackgroundColor(Color.parseColor(buttonColors[2]));
                }else if(antallValgt == 4){
                    b.setBackgroundColor(Color.parseColor(buttonColors[2]));
                    if(valgtArray[2] == valgtArray[3]){
                        b.setBackgroundColor(Color.parseColor(buttonColors[2]));
                        Button b1 = (Button) findViewById(valgtArray[2]);
                        b1.setBackgroundColor(Color.parseColor(buttonColor));
                        Button b0 = (Button) findViewById(valgtArray[0]);
                        b0.setBackgroundColor(Color.parseColor(buttonColor));
                    }else if (valgtArray[1] == valgtArray[3]) {
                        b.setBackgroundColor(Color.parseColor(buttonColors[2]));
                        Button b2 = (Button) findViewById(valgtArray[2]);
                        b2.setBackgroundColor(Color.parseColor(buttonColor));
                        Button b0 = (Button) findViewById(valgtArray[0]);
                        b0.setBackgroundColor(Color.parseColor(buttonColor));
                    }else if(valgtArray[0] == valgtArray[3]){
                        b.setBackgroundColor(Color.parseColor(buttonColors[2]));
                        Button b1 = (Button) findViewById(valgtArray[1]);
                        b1.setBackgroundColor(Color.parseColor(buttonColor));
                    }else {
                        Button b0 = (Button) findViewById(valgtArray[0]);
                        b0.setBackgroundColor(Color.parseColor(buttonColor));
                        Button b1 = (Button) findViewById(valgtArray[1]);
                        b1.setBackgroundColor(Color.parseColor(buttonColor));
                        Button b2 = (Button) findViewById(valgtArray[2]);
                        b2.setBackgroundColor(Color.parseColor(buttonColor));
                        b.setBackgroundColor(Color.parseColor(buttonColors[2]));
                    }
                    b.setBackgroundColor(Color.parseColor(buttonColors[2]));
                }else if(antallValgt == 5){
                    b.setBackgroundColor(Color.parseColor(buttonColors[2]));
                    if(valgtArray[3] == valgtArray[4]){
                        b.setBackgroundColor(Color.parseColor(buttonColors[2]));
                        Button b0 = (Button) findViewById(valgtArray[0]);
                        b0.setBackgroundColor(Color.parseColor(buttonColor));
                        Button b1 = (Button) findViewById(valgtArray[1]);
                        b1.setBackgroundColor(Color.parseColor(buttonColor));
                        Button b2 = (Button) findViewById(valgtArray[2]);
                        b2.setBackgroundColor(Color.parseColor(buttonColor));
                    }else if(valgtArray[2] == valgtArray[4]){
                        b.setBackgroundColor(Color.parseColor(buttonColors[2]));
                        Button b0 = (Button) findViewById(valgtArray[0]);
                        b0.setBackgroundColor(Color.parseColor(buttonColor));
                        Button b1 = (Button) findViewById(valgtArray[1]);
                        b1.setBackgroundColor(Color.parseColor(buttonColor));
                        Button b3 = (Button) findViewById(valgtArray[3]);
                        b3.setBackgroundColor(Color.parseColor(buttonColor));
                    }else if (valgtArray[1] == valgtArray[4]) {
                        b.setBackgroundColor(Color.parseColor(buttonColors[2]));
                        Button b0 = (Button) findViewById(valgtArray[0]);
                        b0.setBackgroundColor(Color.parseColor(buttonColor));
                        Button b2 = (Button) findViewById(valgtArray[2]);
                        b2.setBackgroundColor(Color.parseColor(buttonColor));
                        Button b3 = (Button) findViewById(valgtArray[3]);
                        b3.setBackgroundColor(Color.parseColor(buttonColor));
                    }else if(valgtArray[0] == valgtArray[4]){
                        b.setBackgroundColor(Color.parseColor(buttonColors[2]));
                        Button b1 = (Button) findViewById(valgtArray[1]);
                        b1.setBackgroundColor(Color.parseColor(buttonColor));
                        Button b2 = (Button) findViewById(valgtArray[2]);
                        b2.setBackgroundColor(Color.parseColor(buttonColor));
                        Button b3 = (Button) findViewById(valgtArray[3]);
                        b3.setBackgroundColor(Color.parseColor(buttonColor));
                    }else {
                        Button b0 = (Button) findViewById(valgtArray[0]);
                        b0.setBackgroundColor(Color.parseColor(buttonColor));
                        Button b1 = (Button) findViewById(valgtArray[1]);
                        b1.setBackgroundColor(Color.parseColor(buttonColor));
                        Button b2 = (Button) findViewById(valgtArray[2]);
                        b2.setBackgroundColor(Color.parseColor(buttonColor));
                        Button b3 = (Button) findViewById(valgtArray[3]);
                        b3.setBackgroundColor(Color.parseColor(buttonColor));
                        b.setBackgroundColor(Color.parseColor(buttonColors[2]));
                    }
                    b.setBackgroundColor(Color.parseColor(buttonColors[2]));
                }else if(antallValgt == 6){
                    b.setBackgroundColor(Color.parseColor(buttonColors[2]));
                    if(valgtArray[4] == valgtArray[5]){
                        b.setBackgroundColor(Color.parseColor(buttonColors[2]));
                        Button b0 = (Button) findViewById(valgtArray[0]);
                        b0.setBackgroundColor(Color.parseColor(buttonColor));
                        Button b1 = (Button) findViewById(valgtArray[1]);
                        b1.setBackgroundColor(Color.parseColor(buttonColor));
                        Button b2 = (Button) findViewById(valgtArray[2]);
                        b2.setBackgroundColor(Color.parseColor(buttonColor));
                        Button b3 = (Button) findViewById(valgtArray[3]);
                        b3.setBackgroundColor(Color.parseColor(buttonColor));
                    }else if(valgtArray[3] == valgtArray[5]){
                        b.setBackgroundColor(Color.parseColor(buttonColors[2]));
                        Button b0 = (Button) findViewById(valgtArray[0]);
                        b0.setBackgroundColor(Color.parseColor(buttonColor));
                        Button b1 = (Button) findViewById(valgtArray[1]);
                        b1.setBackgroundColor(Color.parseColor(buttonColor));
                        Button b2 = (Button) findViewById(valgtArray[2]);
                        b2.setBackgroundColor(Color.parseColor(buttonColor));
                        Button b4 = (Button) findViewById(valgtArray[4]);
                        b4.setBackgroundColor(Color.parseColor(buttonColor));
                    }else if(valgtArray[2] == valgtArray[5]){
                        b.setBackgroundColor(Color.parseColor(buttonColors[2]));
                        Button b0 = (Button) findViewById(valgtArray[0]);
                        b0.setBackgroundColor(Color.parseColor(buttonColor));
                        Button b1 = (Button) findViewById(valgtArray[1]);
                        b1.setBackgroundColor(Color.parseColor(buttonColor));
                        Button b3 = (Button) findViewById(valgtArray[3]);
                        b3.setBackgroundColor(Color.parseColor(buttonColor));
                        Button b4 = (Button) findViewById(valgtArray[4]);
                        b4.setBackgroundColor(Color.parseColor(buttonColor));
                    }else if (valgtArray[1] == valgtArray[5]) {
                        b.setBackgroundColor(Color.parseColor(buttonColors[2]));
                        Button b0 = (Button) findViewById(valgtArray[0]);
                        b0.setBackgroundColor(Color.parseColor(buttonColor));
                        Button b2 = (Button) findViewById(valgtArray[2]);
                        b2.setBackgroundColor(Color.parseColor(buttonColor));
                        Button b3 = (Button) findViewById(valgtArray[3]);
                        b3.setBackgroundColor(Color.parseColor(buttonColor));
                        Button b4 = (Button) findViewById(valgtArray[4]);
                        b4.setBackgroundColor(Color.parseColor(buttonColor));
                    }else if(valgtArray[0] == valgtArray[5]){
                        b.setBackgroundColor(Color.parseColor(buttonColors[2]));
                        Button b1 = (Button) findViewById(valgtArray[1]);
                        b1.setBackgroundColor(Color.parseColor(buttonColor));
                        Button b2 = (Button) findViewById(valgtArray[2]);
                        b2.setBackgroundColor(Color.parseColor(buttonColor));
                        Button b3 = (Button) findViewById(valgtArray[3]);
                        b3.setBackgroundColor(Color.parseColor(buttonColor));
                        Button b4 = (Button) findViewById(valgtArray[4]);
                        b4.setBackgroundColor(Color.parseColor(buttonColor));
                    }else {
                        Button b0 = (Button) findViewById(valgtArray[0]);
                        b0.setBackgroundColor(Color.parseColor(buttonColor));
                        Button b1 = (Button) findViewById(valgtArray[1]);
                        b1.setBackgroundColor(Color.parseColor(buttonColor));
                        Button b2 = (Button) findViewById(valgtArray[2]);
                        b2.setBackgroundColor(Color.parseColor(buttonColor));
                        Button b3 = (Button) findViewById(valgtArray[3]);
                        b3.setBackgroundColor(Color.parseColor(buttonColor));
                        Button b4 = (Button) findViewById(valgtArray[4]);
                        b4.setBackgroundColor(Color.parseColor(buttonColor));
                    }
                    b.setBackgroundColor(Color.parseColor(buttonColors[2]));
                }


            }else{
                TextClassGetter tCG = new TextClassGetter(getApplicationContext());
                String[] array = tCG.getText("ValgSkjerm");
                if(toast != null){

                    toast.cancel();
                    toast = Toast.makeText(context,array[4] +maxValgbare+array[5],Toast.LENGTH_SHORT);
                    //toast = Toast.makeText(context,"Only "+maxValgbare+" series possible to choose in free version",Toast.LENGTH_SHORT);
                    toast.show();
                }else {
                    toast = Toast.makeText(context,array[4]+maxValgbare+array[5],Toast.LENGTH_SHORT);
                    toast.show();
                }

            }
        }
    };




    public void populateButtons(List<GenerelleHopp> generelleHopp){
        LinearLayout sV = (LinearLayout) findViewById(R.id.listView);
        List<String> namesholder = new ArrayList<>();
        Typeface typeface = null;
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.O) {
            typeface = getResources().getFont(R.font.turntegnregular);
        }else {
            typeface = Typeface.createFromAsset(getAssets(),  "fonts/turntegnregular.ttf");
        }
        //SpannableStringBuilder SS = new SpannableStringBuilder("আমারநல்வரவு");
        int i = 0;
        while(i < generelleHopp.size()){
            if(!checkNames(generelleHopp.get(i).getName(),namesholder)) {
                namesholder.add(generelleHopp.get(i).getName());
                //TextView tv = new TextView(this);
                // tv.setText(" xdc");
                Button b = new Button(this);
                b.setTextSize(25);
                LinearLayout linearLayoutInside = new LinearLayout(this);
                linearLayoutInside.setOrientation(LinearLayout.HORIZONTAL);
                linearLayoutInside.setMinimumWidth(20);
                linearLayoutInside.setBackgroundColor(Color.parseColor(buttonColor));
                linearLayoutInside.setGravity(Gravity.CENTER_HORIZONTAL);
                b.setId(View.generateViewId());
                buttonIds.add(b.getId());
                //b.setText(medHoppredskapSerieList.get(i).getName());
                b.setOnClickListener(clicks);
                b.setBackgroundColor(Color.parseColor(buttonColor));
                //SS.setSpan(font, 0, medHoppredskapSerieList.get(i).getName().length(), Spannable.SPAN_INCLUSIVE_INCLUSIVE);
                b.setText(generelleHopp.get(i).getName());
                b.setTypeface(typeface);
                b.setAllCaps(false);
                b.setPadding(30,0,30,0);
                //linearLayoutInside.addView(b);
                //linearLayoutInside.addView(tv);

                sV.addView(b);
            }
            i++;
        }
    }

    Toast toast;


    public boolean checkNames(String name, List<String> nameList){
        int i = 0;
        while (i < nameList.size()){
            if(name.equals(nameList.get(i))){
                return true;
            }
            i++;
        }

        return false;
    }


    public void onHoppKnappPressed(View view){
        Button b = (Button) view;

        LinearLayout l = (LinearLayout) findViewById(R.id.listView);
        String text = b.getText().toString();
        boolean found = false;
        int i = 0;
        Button tempB = null;
        while(!found && i < buttonIds.size()) {
            Log.v("kjdfg","id: " + buttonIds.get(i));
            tempB = (Button) findViewById(buttonIds.get(i));
            String textHolder = tempB.getText().toString();
            String textTester;

            if(textHolder.charAt(textHolder.length()-1) == 'a' || textHolder.charAt(textHolder.length()-1) == 'b' || textHolder.charAt(textHolder.length()-1) == 'c'){
                textTester = textHolder.substring(0,textHolder.length()-2);
                Log.v("awsdfgb","-2 = " + textHolder.substring(0,textHolder.length()-2));
            }else {
                textTester = tempB.getText().toString();
                Log.v("awsdfgb","resten: " + tempB.getText().toString());
            }
            if(text.equals(textTester)){
                Log.v("awsdfgb","found: " + textTester);
                found = true;
            }
            i++;
        }
        b.setText("Some serie");
        antallValgt--;
        if(antallValgt <= 0){
            doneButton.setEnabled(false);
            clearAllButton.setEnabled(false);
            clearLastButton.setEnabled(false);
        }

        if(valgtArray[5] == tempB.getId()){
            valgtArray[5] = 0;
        }else if(valgtArray[4] == tempB.getId()){
            valgtArray[4] = valgtArray[5];
            valgtArray[5] = 0;
        }else if(valgtArray[3] == tempB.getId()){
            valgtArray[3] = valgtArray[4];
            valgtArray[4] = valgtArray[5];
            valgtArray[5] = 0;
        }else if(valgtArray[2] == tempB.getId()){
            valgtArray[2] = valgtArray[3];
            valgtArray[3] = valgtArray[4];
            valgtArray[4] = valgtArray[5];
            valgtArray[5] = 0;
        }else if(valgtArray[1] == tempB.getId()){
            valgtArray[1] = valgtArray[2];
            valgtArray[2] = valgtArray[3];
            valgtArray[3] = valgtArray[4];
            valgtArray[4] = valgtArray[5];
            valgtArray[5] = 0;
        }else if(valgtArray[0] == tempB.getId()){
            valgtArray[0] = valgtArray[1];
            valgtArray[1] = valgtArray[2];
            valgtArray[2] = valgtArray[3];
            valgtArray[3] = valgtArray[4];
            valgtArray[4] = valgtArray[5];
            valgtArray[5] = 0;
        }
        if(valgtArray[0] != 0) {
            Button bb1 = (Button) findViewById(R.id.hopp1);
            Button bv1 = (Button) findViewById(valgtArray[0]);
            bb1.setText(removeAfffix(bv1.getText().toString()));
        }else {
            Button bb1 = (Button) findViewById(R.id.hopp1);
            bb1.setText("Some serie");
            bb1.setEnabled(false);
        }
        if(valgtArray[1] != 0) {
            Button bb2 = (Button) findViewById(R.id.hopp2);
            Button bv2 = (Button) findViewById(valgtArray[1]);
            bb2.setText(removeAfffix(bv2.getText().toString()));
        }else {
            Button bb2 = (Button) findViewById(R.id.hopp2);
            bb2.setText("Some serie");
            bb2.setEnabled(false);
        }
        if(valgtArray[2] != 0) {
            Button bb3 = (Button) findViewById(R.id.hopp3);
            Button bv3 = (Button) findViewById(valgtArray[2]);
            bb3.setText(removeAfffix(bv3.getText().toString()));
        }else {
            Button bb3 = (Button) findViewById(R.id.hopp3);
            bb3.setText("Some serie");
            bb3.setEnabled(false);
        }
        setUpButtonColors();
        setUpButtonExtras();
    }

    public String removeAfffix(String text){
        String newText;
        if(text.length() > 1 && text.charAt(text.length()-1) == 'a' || text.charAt(text.length()-1) == 'b' || text.charAt(text.length()-1) == 'c' || text.charAt(text.length()-1) == 'd' || text.charAt(text.length()-1) == 'e' || text.charAt(text.length()-1) == 'f'){
            newText = text.substring(0,text.length()-2);
        }else {
            newText = text;
        }
        Log.v("kløhjk","newText: " + newText);
        return newText;
    }

    public void setUpButtonExtras(){
        int i = 0;
        int likeTeller = 0;
        while(i < buttonIds.size()){
            Button tempB = (Button) findViewById(buttonIds.get(i));
            String holder = removeAfffix(tempB.getText().toString());
            String affix = getAffix(tempB.getText().toString());
            String nyAffix = getNewAffixMinus(affix);
            Log.v("fkij","Holder: " + holder);
            if(tempB.getId() == valgtArray[5]){
                likeTeller = finnAntallLike(tempB.getId());
                String a;
                if(likeTeller == 1){
                    a = holder + " a";
                }else if(likeTeller == 2){
                    a = holder + " b";
                }else if(likeTeller == 3){
                    a = holder + " c";
                }else if(likeTeller == 4){
                    a = holder + " d";
                }else if(likeTeller == 5){
                    a = holder + " e";
                }else if(likeTeller == 6){
                    a = holder + " f";
                }else {
                    a = holder;
                }
                tempB.setText(a);
                tempB.setBackgroundColor(Color.parseColor(buttonColor));
            }else if(tempB.getId() == valgtArray[4]){
                likeTeller = finnAntallLike(tempB.getId());
                String a;
                if(likeTeller == 1){
                    a = holder + " a";
                }else if(likeTeller == 2){
                    a = holder + " b";
                }else if(likeTeller == 3){
                    a = holder + " c";
                }else if(likeTeller == 4){
                    a = holder + " d";
                }else if(likeTeller == 5){
                    a = holder + " e";
                }else if(likeTeller == 6){
                    a = holder + " f";
                }else {
                    a = holder;
                }
                tempB.setText(a);
                if(valgtArray[5] == 0){
                    tempB.setBackgroundColor(Color.parseColor(buttonColors[2]));
                }else {
                    tempB.setBackgroundColor(Color.parseColor(buttonColor));
                }
            }else if(tempB.getId() == valgtArray[3]){
                likeTeller = finnAntallLike(tempB.getId());
                String a;
                if(likeTeller == 1){
                    a = holder + " a";
                }else if(likeTeller == 2){
                    a = holder + " b";
                }else if(likeTeller == 3){
                    a = holder + " c";
                }else if(likeTeller == 4){
                    a = holder + " d";
                }else if(likeTeller == 5){
                    a = holder + " e";
                }else if(likeTeller == 6){
                    a = holder + " f";
                }else {
                    a = holder;
                }
                tempB.setText(a);
                if(valgtArray[4] == 0){
                    tempB.setBackgroundColor(Color.parseColor(buttonColors[2]));
                }else {
                    tempB.setBackgroundColor(Color.parseColor(buttonColor));
                }
            }else if(tempB.getId() == valgtArray[2]){
                likeTeller = finnAntallLike(tempB.getId());
                String a;
                if(likeTeller == 1){
                    a = holder + " a";
                }else if(likeTeller == 2){
                    a = holder + " b";
                }else if(likeTeller == 3){
                    a = holder + " c";
                }else if(likeTeller == 4){
                    a = holder + " d";
                }else if(likeTeller == 5){
                    a = holder + " e";
                }else if(likeTeller == 6){
                    a = holder + " f";
                }else {
                    a = holder;
                }
                tempB.setText(a);
                if(valgtArray[3] == 0){
                    tempB.setBackgroundColor(Color.parseColor(buttonColors[2]));
                }else {
                    tempB.setBackgroundColor(Color.parseColor(buttonColor));
                }
            }else if (tempB.getId() == valgtArray[1]){
                likeTeller = finnAntallLike(tempB.getId());
                String a;
                if(likeTeller == 1){
                    a = holder + " a";
                }else if(likeTeller == 2){
                    a = holder + " b";
                }else if(likeTeller == 3){
                    a = holder + " c";
                }else if(likeTeller == 4){
                    a = holder + " d";
                }else if(likeTeller == 5){
                    a = holder + " e";
                }else if(likeTeller == 6){
                    a = holder + " f";
                }else {
                    a = holder;
                }
                tempB.setText(a);
                if(valgtArray[2] == 0){
                    tempB.setBackgroundColor(Color.parseColor(buttonColors[2]));
                }else {
                    tempB.setBackgroundColor(Color.parseColor(buttonColor));
                }
            }else if (tempB.getId() == valgtArray[0]){
                likeTeller = finnAntallLike(tempB.getId());
                String a;
                if(likeTeller == 1){
                    a = holder + " a";
                }else if(likeTeller == 2){
                    a = holder + " b";
                }else if(likeTeller == 3){
                    a = holder + " c";
                }else if(likeTeller == 4){
                    a = holder + " d";
                }else if(likeTeller == 5){
                    a = holder + " e";
                }else if(likeTeller == 6){
                    a = holder + " f";
                }else {
                    a = holder;
                }
                tempB.setText(a);
                if(valgtArray[1] == 0){
                    tempB.setBackgroundColor(Color.parseColor(buttonColors[2]));
                }else {
                    tempB.setBackgroundColor(Color.parseColor(buttonColor));
                }
            }else{
                tempB.setText(holder);
            }
            i++;
        }
    }

    public void setUpButtonColors(){
        int i = 0;
        while(i < buttonIds.size()){
            Button tempB = (Button) findViewById(buttonIds.get(i));
            if(tempB.getId() == valgtArray[5]){
                tempB.setBackgroundColor(Color.parseColor(buttonColors[2]));
            }else if(tempB.getId() == valgtArray[4]){
                tempB.setBackgroundColor(Color.parseColor(buttonColors[2]));
            }else if(tempB.getId() == valgtArray[3]){
                tempB.setBackgroundColor(Color.parseColor(buttonColors[2]));
            }else if(tempB.getId() == valgtArray[2]){
                tempB.setBackgroundColor(Color.parseColor(buttonColors[2]));
            }else if (tempB.getId() == valgtArray[1]){
                tempB.setBackgroundColor(Color.parseColor(buttonColors[2]));
            }else if (tempB.getId() == valgtArray[0]){
                tempB.setBackgroundColor(Color.parseColor(buttonColors[2]));
            }else{
                tempB.setBackgroundColor(Color.parseColor(buttonColor));
            }
            i++;
        }
    }

    public void onDonePressed(View view){
        if(valgtArray[0] == 0 && valgtArray[1] == 0 && valgtArray[2] == 0 && valgtArray[3] == 0 && valgtArray[4] == 0 && valgtArray[5] == 0) {
            TextClassGetter tCG = new TextClassGetter(this);
            String[] array = tCG.getText("ValgSkjerm");
            if (toast != null) {
                toast.cancel();
                toast = Toast.makeText(this,array[3],Toast.LENGTH_SHORT);
                //toast = Toast.makeText(this,"Please choose a series",Toast.LENGTH_SHORT);
                toast.show();
            }else {
                toast = Toast.makeText(this,array[3],Toast.LENGTH_SHORT);
                toast.show();
            }
        }else {
            Intent i = new Intent(this, FinalScore.class);
            if (valgtArray[0] != 0) {
                Button b1 = (Button) findViewById(valgtArray[0]);
                String sub = b1.getText().toString().substring(0,b1.getText().length()-2);
                i.putExtra("valgtEn", sub);
            } else {
                i.putExtra("valgtEn", "NA");
            }

            if (valgtArray[1] != 0) {
                Button b2 = (Button) findViewById(valgtArray[1]);
                String sub = b2.getText().toString().substring(0,b2.getText().length()-2);
                i.putExtra("valgtTo", sub);
            } else {
                i.putExtra("valgtTo", "NA");
            }
            if (valgtArray[2] != 0) {
                Button b3 = (Button) findViewById(valgtArray[2]);
                String sub = b3.getText().toString().substring(0,b3.getText().length()-2);
                i.putExtra("valgtTre", sub);
            } else {
                i.putExtra("valgtTre", "NA");
            }
            if (valgtArray[3] != 0) {
                Button b4 = (Button) findViewById(valgtArray[3]);
                String sub = b4.getText().toString().substring(0,b4.getText().length()-2);
                i.putExtra("valgtFire", sub);
            } else {
                i.putExtra("valgtFire", "NA");
            }
            if (valgtArray[4] != 0) {
                Button b5 = (Button) findViewById(valgtArray[4]);
                String sub = b5.getText().toString().substring(0,b5.getText().length()-2);
                i.putExtra("valgtFem", sub);
            } else {
                i.putExtra("valgtFem", "NA");
            }
            if (valgtArray[5] != 0) {
                Button b6 = (Button) findViewById(valgtArray[5]);
                String sub = b6.getText().toString().substring(0,b6.getText().length()-2);
                i.putExtra("valgtSeks", sub);
            } else {
                i.putExtra("valgtSeks", "NA");
            }

            i.putExtra("hoppRedskap", valgtApparat);
            startActivity(i);
        }
    }

    public void onClearLastPressed(View view){
        LinearLayout l = (LinearLayout) findViewById(R.id.listView);
        boolean found = false;
        int i = 0;
        Button tempB = null;
        //b.setText("Some serie");
        antallValgt--;
        if(antallValgt <= 0){
            doneButton.setEnabled(false);
            clearAllButton.setEnabled(false);
            clearLastButton.setEnabled(false);
        }
        if(antallValgt == 5){
            valgtArray[5] = 0;
        }else if(antallValgt == 4){
            valgtArray[4] = 0;
            valgtArray[5] = 0;
        }else if(antallValgt == 3){
            valgtArray[3] = 0;
            valgtArray[4] = 0;
            valgtArray[5] = 0;
        }else if(antallValgt == 2){
            valgtArray[2] = 0;
            valgtArray[3] = 0;
            valgtArray[4] = 0;
            valgtArray[5] = 0;
        }else if(antallValgt == 1){
            valgtArray[1] = 0;
            valgtArray[2] = 0;
            valgtArray[3] = 0;
            valgtArray[4] = 0;
            valgtArray[5] = 0;
        }else if(antallValgt == 0){
            valgtArray[0] = 0;
            valgtArray[1] = 0;
            valgtArray[2] = 0;
            valgtArray[3] = 0;
            valgtArray[4] = 0;
            valgtArray[5] = 0;
        }
        setUpButtonColors();
        setUpButtonExtras();
    }

    public void onClearAllPressed(View view){
        antallValgt = 0;
        if(antallValgt <= 0){
            doneButton.setEnabled(false);
            clearAllButton.setEnabled(false);
            clearLastButton.setEnabled(false);
        }
        if(antallValgt == 5){
            valgtArray[5] = 0;
        }else if(antallValgt == 4){
            valgtArray[4] = 0;
            valgtArray[5] = 0;
        }else if(antallValgt == 3){
            valgtArray[3] = 0;
            valgtArray[4] = 0;
            valgtArray[5] = 0;
        }else if(antallValgt == 2){
            valgtArray[2] = 0;
            valgtArray[3] = 0;
            valgtArray[4] = 0;
            valgtArray[5] = 0;
        }else if(antallValgt == 1){
            valgtArray[1] = 0;
            valgtArray[2] = 0;
            valgtArray[3] = 0;
            valgtArray[4] = 0;
            valgtArray[5] = 0;
        }else if(antallValgt == 0){
            valgtArray[0] = 0;
            valgtArray[1] = 0;
            valgtArray[2] = 0;
            valgtArray[3] = 0;
            valgtArray[4] = 0;
            valgtArray[5] = 0;
        }
        setUpButtonColors();
        setUpButtonExtras();
    }

    public String getAffix(String text){
        String a;
        if(text.length() > 1) {
            //a = text.substring(text.length() - 1, text.length() - 1);
            a = " "+text.charAt(text.length()-1);
        }else {
            a = "";
        }
        Log.v("fdghbvvvv","a: " + a);
        return a;
    }


    public void setLanguages(){
        Button clearLastButton = findViewById(R.id.clearLast);
        Button clearAllButton = findViewById(R.id.clearAll);
        Button doneButton = findViewById(R.id.buttonSelectScreen);
        TextClassGetter tCG = new TextClassGetter(this);
        String[] array = tCG.getText("ValgSkjerm");
        clearLastButton.setText(array[0]);
        clearAllButton.setText(array[1]);
        doneButton.setText(array[2]);
    }

    public String getNewAffixPluss(String affix){
        Log.v("lkdfgud","affix: \"" + affix + "\"");
        String nyAffix ="";
        affix = affix;
        if(affix.equals(" a")){
            nyAffix = " b";
        }else if(affix.equals(" b")){
            nyAffix = " c";
        }else if(affix.equals(" c")){
            nyAffix = " d";
        }else if(affix.equals(" d")){
            nyAffix = " e";
        }else if(affix.equals(" e")){
            nyAffix = " f";
        }else {
            nyAffix = " a";
        }
        return nyAffix;
    }

    public String getNewAffixMinus(String affix){
        Log.v("lkdfgud","affix: \"" + affix + "\"");
        String nyAffix ="";
        affix = " "+affix;
        if(affix.equals(" f")){
            nyAffix = "e";
        }else if(affix.equals(" e")){
            nyAffix = "d";
        }else if(affix.equals(" d")){
            nyAffix = "c";
        }else if(affix.equals(" c")){
            nyAffix = "b";
        }else if(affix.equals(" b")){
            nyAffix = "a";
        }else {
            nyAffix = "";
        }
        return nyAffix;
    }

    public int finnAntallLike(int id){
        int i = 0;
        int teller = 0;
        while (i < valgtArray.length){
            if(id == valgtArray[i]){
                teller++;
            }
            i++;
        }
        return teller;
    }

    public void onInfoButtonPressed(View view){
        Intent i=new Intent(this,SignExplanations.class);
        startActivity(i);
    }

}
