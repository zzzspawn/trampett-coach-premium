package com.norwegianonapps.trampettcoachpremium;

import android.content.Context;

import java.math.BigDecimal;

public class UtenHoppredskapSerie  extends GenerelleHopp{

    private Salto firstSalto;
    private Salto secondSalto;
    private Salto thirdSalto;
    private TilleggsVerdier tV;
    private BaseVerdier bv;
    private TrampettTilleggsVerdier tTV;
    private String name;
    private BigDecimal verdi;
    Context context;
    private String hoppredskap;

    private BigDecimal skruEn;
    private BigDecimal skruTo;
    private BigDecimal skruTre;

    public BigDecimal getSkruEn(){
        return firstSalto.getSkru();
    }
    public BigDecimal getSkruT0(){
        if(secondSalto != null) {
            return secondSalto.getSkru();
        }else {
            return null;
        }
    }
    public BigDecimal getSkruTre(){
        if(thirdSalto != null) {
            return thirdSalto.getSkru();
        }else {
            return null;
        }
    }
    @Override
    public String getHoppredskap() {
        return hoppredskap;
    }

    public BigDecimal getVerdi(){
        if(verdi == null){
            return null;
        }else {
            return verdi;
        }
    }

    public String getName(){
        return name;
    }

    @Override
    public Enum<Posisjon> getPosisjonEn(){
        if(firstSalto != null){
            return firstSalto.getPosisjon();
        }else {
            return null;
        }
    }
    @Override
    public Enum<Posisjon> getPosisjonTo(){
        if(secondSalto != null){
            return secondSalto.getPosisjon();
        }else {
            return null;
        }
    }
    @Override
    public Enum<Posisjon> getPosisjonTre(){
        if(thirdSalto != null){
            return thirdSalto.getPosisjon();
        }else {
            return null;
        }
    }


    public UtenHoppredskapSerie(Context context,Salto firstSalto){
        hoppredskap = "Trampet";
        this.context = context;
        this.firstSalto = firstSalto;
        settVerdi();
        settNavn();
        settAntallRotasjoner();
    }
    public UtenHoppredskapSerie(Context context,Salto firstSalto, Salto secondSalto){
        hoppredskap = "Trampet";
        this.context = context;
        this.firstSalto = firstSalto;
        this.secondSalto = secondSalto;
        settVerdi();
        settNavn();
        settAntallRotasjoner();
    }
    public UtenHoppredskapSerie(Context context,Salto firstSalto, Salto secondSalto, Salto thirdSalto){
        hoppredskap = "Trampet";
        this.context = context;
        this.firstSalto = firstSalto;
        this.secondSalto = secondSalto;
        this.thirdSalto = thirdSalto;
        settVerdi();
        settNavn();
        settAntallRotasjoner();
    }

    public void settNavn(){
        String navn = "";
        BigDecimal antallSkru = new BigDecimal("0.00");
        int antallSalto = 0;
        if(firstSalto != null){
            antallSalto++;
            antallSkru = antallSkru.add(firstSalto.getSkru());
        }
        if(secondSalto != null){
            antallSalto++;
            antallSkru = antallSkru.add(secondSalto.getSkru());
        }
        if(thirdSalto != null){
            antallSalto++;
            antallSkru = antallSkru.add(thirdSalto.getSkru());
        }
        if(antallSalto == 1){
            if(firstSalto.getPosisjon() == Posisjon.KROPPERT){
                navn = navn+"k";
            }else if(firstSalto.getPosisjon() == Posisjon.PIKERT){
                navn = navn+"p";
            }else if(firstSalto.getPosisjon() == Posisjon.STRAK){
                navn = navn+"s";
            }

            if(antallSkru.compareTo(new BigDecimal("0")) != 0){
                navn = navn+"" + getSkru(firstSalto);
            }else {
                navn = navn+"";
            }
        }else if(antallSalto == 2){
            navn = navn+"";

            navn = navn + getPos(firstSalto);

            if (firstSalto.getSkru().compareTo(new BigDecimal("0")) != 0) {
                navn = navn + getSkru(firstSalto);
            }else {
                if(firstSalto.getPosisjon() == Posisjon.KROPPERT){
                    navn = navn + " ";
                }
            }

            navn = navn + getPos(secondSalto);

            if (secondSalto.getSkru().compareTo(new BigDecimal("0")) != 0) {
                navn = navn + getSkru(secondSalto);
            }

        }else if(antallSalto == 3){

            navn = navn+"";
            // System.out.println("If");
            navn = navn + getPos(firstSalto);
            if(firstSalto.getSkru().compareTo(new BigDecimal("0")) != 0){
                navn = navn + getSkru(firstSalto);
            }
            navn = navn + getPos(secondSalto);
            if(secondSalto.getSkru().compareTo(new BigDecimal("0")) != 0) {
                navn = navn + getSkru(secondSalto);
            }
            navn = navn + getPos(thirdSalto);
            if(thirdSalto.getSkru().compareTo(new BigDecimal("0")) != 0) {
                navn = navn + getSkru(thirdSalto);
            }

        }
        name = navn;
    }

    public BigDecimal floatToDegrees(BigDecimal tall){
        return tall.multiply(new BigDecimal("360")).setScale(0);
    }

    public String getSkru(Salto salto){
        return floatToDegrees(salto.getSkru()) + "\u00B0 ";
    }

    public String getPos(Salto salto){
        if(salto.getPosisjon() == Posisjon.KROPPERT){
            return "k";
        }else if (salto.getPosisjon() == Posisjon.PIKERT){
            return "p";
        }else if (salto.getPosisjon() == Posisjon.STRAK){
            return "s";
        }else{
            return "shiut";
        }
    }

    public void settVerdi(){


        verdi = new BigDecimal("0.00");
        BigDecimal antallSkru = new BigDecimal("0.00");
        tV = new TilleggsVerdier(context);
        tV.importValues();
        bv = new BaseVerdier(context);
        bv.importValues();
        tTV = tV.getTrampettTilleggsVerdier();
        int antallSalto = 0;
        if(firstSalto != null){
            antallSalto++;
        }
        if(secondSalto != null){
            antallSalto++;
        }
        if(thirdSalto != null){
            antallSalto++;
        }
        if(antallSalto == 1){

            verdi = verdi.add(new BigDecimal(bv.getSaltoVerdi()));

            verdi = verdi.add(getSaltoTilleggsVerdi(firstSalto.getPosisjon(),1));

            antallSkru = antallSkru.add(firstSalto.getSkru());

            verdi = verdi.add(getSkruVerdi(antallSkru, antallSalto));

        }else if(antallSalto == 2) {

            if (firstSalto.getPosisjon() == Posisjon.PIKERT || secondSalto.getPosisjon() == Posisjon.PIKERT) {
                if(firstSalto.getPosisjon() == Posisjon.PIKERT && firstSalto.getSkru().compareTo(new BigDecimal("0.5")) > 0){
                    verdi = verdi.add(new BigDecimal(bv.getDobbelSaltoVerdi()));
                    verdi = verdi.add(getSaltoTilleggsVerdi(Posisjon.STRAK, antallSalto));
                }else {
                    verdi = verdi.add(new BigDecimal(bv.getDobbelSaltoVerdi()));
                    verdi = verdi.add(getSaltoTilleggsVerdi(firstSalto.getPosisjon(), antallSalto));
                }
                //skru greier her
                antallSkru = antallSkru.add(firstSalto.getSkru());
                verdi = verdi.add(getSkruVerdi(antallSkru, antallSalto));
                if(secondSalto.getPosisjon() == Posisjon.PIKERT && secondSalto.getSkru().compareTo(new BigDecimal("0.5")) > 0){
                    verdi = verdi.add(getSaltoTilleggsVerdi(Posisjon.STRAK, antallSalto));
                }else {
                    verdi = verdi.add(getSaltoTilleggsVerdi(secondSalto.getPosisjon(), antallSalto));
                }
                //skru greier her
                antallSkru = antallSkru.add(secondSalto.getSkru());
                verdi = verdi.add(getSkruVerdi(antallSkru, antallSalto));


            } else{
                verdi = verdi.add(new BigDecimal(bv.getDobbelSaltoVerdi()));
                verdi = verdi.add(getSaltoTilleggsVerdi(firstSalto.getPosisjon(), antallSalto));
                //skru greier her
                antallSkru = antallSkru.add(firstSalto.getSkru());
                verdi = verdi.add(getSkruVerdi(antallSkru, antallSalto));
                verdi = verdi.add(getSaltoTilleggsVerdi(secondSalto.getPosisjon(), antallSalto));
                //skru greier her
                antallSkru = secondSalto.getSkru();
                verdi = verdi.add(getSkruVerdi(antallSkru, antallSalto));
            }
        }else if(antallSalto == 3){
            if (firstSalto.getPosisjon() == Posisjon.PIKERT || secondSalto.getPosisjon() == Posisjon.PIKERT|| thirdSalto.getPosisjon() == Posisjon.PIKERT) {

                if(firstSalto.getPosisjon() == Posisjon.PIKERT && firstSalto.getSkru().compareTo(new BigDecimal("0.5")) > 0){
                    verdi = verdi.add(new BigDecimal(bv.getTrippelSaltoVerdi()));
                    verdi = verdi.add(getSaltoTilleggsVerdi(Posisjon.STRAK, antallSalto));
                }else {
                    verdi = verdi.add(new BigDecimal(bv.getTrippelSaltoVerdi()));
                    verdi = verdi.add(getSaltoTilleggsVerdi(firstSalto.getPosisjon(), antallSalto));
                }

                //skru greier her
                antallSkru = antallSkru.add(firstSalto.getSkru());
                verdi = verdi.add(getSkruVerdi(antallSkru, antallSalto));

                if(secondSalto.getPosisjon() == Posisjon.PIKERT && secondSalto.getSkru().compareTo(new BigDecimal("0.5")) > 0){
                    verdi = verdi.add(getSaltoTilleggsVerdi(Posisjon.STRAK, antallSalto));
                }else {
                    verdi = verdi.add(getSaltoTilleggsVerdi(secondSalto.getPosisjon(), antallSalto));
                }

                //skru greier her
                antallSkru = antallSkru.add(secondSalto.getSkru());
                verdi = verdi.add(getSkruVerdi(antallSkru, antallSalto));

                if(thirdSalto.getPosisjon() == Posisjon.PIKERT && thirdSalto.getSkru().compareTo(new BigDecimal("0.5")) > 0){
                    verdi = verdi.add(getSaltoTilleggsVerdi(Posisjon.STRAK, antallSalto));
                }else {
                    verdi = verdi.add(getSaltoTilleggsVerdi(thirdSalto.getPosisjon(), antallSalto));
                }

                //skru greier her
                antallSkru = antallSkru.add(thirdSalto.getSkru());
                verdi = verdi.add(getSkruVerdi(antallSkru, antallSalto));


            } else {
                verdi = verdi.add(new BigDecimal(bv.getTrippelSaltoVerdi()));
                verdi = verdi.add(getSaltoTilleggsVerdi(firstSalto.getPosisjon(), antallSalto));
                antallSkru = firstSalto.getSkru();
                verdi = verdi.add(getSkruVerdi(antallSkru, antallSalto));
                verdi = verdi.add(getSaltoTilleggsVerdi(secondSalto.getPosisjon(), antallSalto));
                antallSkru = secondSalto.getSkru();
                verdi = verdi.add(getSkruVerdi(antallSkru, antallSalto));
                verdi = verdi.add(getSaltoTilleggsVerdi(thirdSalto.getPosisjon(), antallSalto));
                antallSkru = thirdSalto.getSkru();
                verdi = verdi.add(getSkruVerdi(antallSkru, antallSalto));
            }
        }
    }

    public BigDecimal getSkruVerdi(BigDecimal antallSkru, int rotasjoner){
        BigDecimal skruVerdi = new BigDecimal("0.0");
        BigDecimal helHalv = new BigDecimal("0.0");
        BigDecimal halvannen = new BigDecimal("0.0");
        BigDecimal dobbelSkru = new BigDecimal("0.0");
        BigDecimal trippelSkru = new BigDecimal("0.0");
        if(rotasjoner == 1) {
            helHalv = new BigDecimal(tTV.getEnkelHalvHelSkru());
            halvannen = new BigDecimal(tTV.getEnkelHalvannenSkru());
            dobbelSkru = new BigDecimal(tTV.getEnkelDobbelSkru());
            trippelSkru = new BigDecimal(tTV.getEnkelTrippelSkru());
        }else if(rotasjoner == 2) {
            helHalv = new BigDecimal(tTV.getDobbelHalvHelSkru());
            halvannen = new BigDecimal(tTV.getDobbelHalvannenSkru());
            dobbelSkru = new BigDecimal(tTV.getDobbelDobbelSkru());
            trippelSkru = new BigDecimal(tTV.getDobbelTrippelSkru());
        }else if(rotasjoner == 3) {
            helHalv = new BigDecimal(tTV.getTrippelHalvHelSkru());
            halvannen = new BigDecimal(tTV.getTrippelHalvannenSkru());
            dobbelSkru = new BigDecimal(tTV.getTrippelDobbelSkru());
            trippelSkru = new BigDecimal(tTV.getTrippelTrippelSkru());
        }
        BigDecimal divider = new BigDecimal("0.5");
        BigDecimal dividerTrippelSkru = new BigDecimal("1");
        BigDecimal result = antallSkru.divide(divider);
        int teller = result.intValueExact();
        FileHandlerClass fileHandlerClass = new FileHandlerClass(context);
        int i = 0;
        if(fileHandlerClass.getRuleset().equalsIgnoreCase("ngtf")){
            while(i < teller) {
                if(i < 2) {
                    skruVerdi = skruVerdi.add(helHalv);

                }
                if(i > 1 && i < 3) {
                    skruVerdi = skruVerdi.add(halvannen);

                }
                if(i > 2) {
                    skruVerdi = skruVerdi.add(dobbelSkru);

                }
                i++;
            }
        }else if(fileHandlerClass.getRuleset().equalsIgnoreCase("ueg")){
            while(i < teller) {
                if(i < 2) {
                    skruVerdi = skruVerdi.add(helHalv);
                }
                if(i > 1 && i < 3) {
                    skruVerdi = skruVerdi.add(halvannen);

                }
                if(i > 2 && i < 5) {
                    skruVerdi = skruVerdi.add(dobbelSkru);

                }if(i > 4) {
                    if ( !((i & 1) == 0 )) {
                        skruVerdi = skruVerdi.add(trippelSkru);
                    }
                }
                i++;
            }
        }
        return skruVerdi;
    }

    public BigDecimal getSaltoTilleggsVerdi(Enum<Posisjon> posisjonen, int rotasjoner){
        if(rotasjoner == 1) {
            if (posisjonen == Posisjon.KROPPERT) {
                return new BigDecimal("0.00");
            } else if (posisjonen == Posisjon.PIKERT) {
                return new BigDecimal(tTV.getEnkelPikert());
            } else if (posisjonen == Posisjon.STRAK) {
                return new BigDecimal(tTV.getEnkelStrak());
            }
        }else if(rotasjoner == 2) {
            if (posisjonen == Posisjon.KROPPERT) {
                return new BigDecimal("0.00");
            } else if (posisjonen == Posisjon.PIKERT) {
                return new BigDecimal(tTV.getDobbelPikert());
            } else if (posisjonen == Posisjon.STRAK) {
                return new BigDecimal(tTV.getDobbelStrak());
            }
        }else if(rotasjoner == 3) {
            if (posisjonen == Posisjon.KROPPERT) {
                return new BigDecimal("0.00");
            } else if (posisjonen == Posisjon.PIKERT) {
                return new BigDecimal(tTV.getTrippelPikert());
            } else if (posisjonen == Posisjon.STRAK) {
                FileHandlerClass fileHandlerClass = new FileHandlerClass(context);
                if(fileHandlerClass.getRuleset().equalsIgnoreCase("ngtf")) {
                    return new BigDecimal(tTV.getTrippelStrak());
                }else if(fileHandlerClass.getRuleset().equalsIgnoreCase("ueg")) {
                    return new BigDecimal("0.0");
                }
            }
        }
        return new BigDecimal("0.0");
    }

    public void settAntallRotasjoner(){

        int teller = 0;
        if(firstSalto != null){
            teller++;
        }
        if (secondSalto != null){
            teller++;
        }
        if(thirdSalto != null){
            teller++;
        }

        antallRotasjoner = teller;

    }

}
