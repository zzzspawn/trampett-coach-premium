package com.norwegianonapps.trampettcoachpremium;

import java.math.BigDecimal;

public class SpesialHopp extends GenerelleHopp{
    private String name;
    private BigDecimal verdi;
    private String tegn;
    boolean spesialHopp;
    String hoppredskap;
    public SpesialHopp(String navn, BigDecimal verdi,String tegn){
        hoppredskap = "hest";
        this.name = navn;
        this.verdi = verdi;
        this.tegn = tegn;
        spesialHopp = true;
    }

    public BigDecimal getVerdi() {return verdi;}

    public String getName() {
        return name;
    }

    public boolean isSpesialHopp(){
        return spesialHopp;
    }

    public void settHoppredskap(String redskap){
        hoppredskap = redskap;
    }

    public String getHoppredskap(){
        return hoppredskap;
    }

}
