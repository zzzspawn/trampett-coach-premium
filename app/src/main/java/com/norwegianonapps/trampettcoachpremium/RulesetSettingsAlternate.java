package com.norwegianonapps.trampettcoachpremium;

import android.content.Context;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.TextView;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.Reader;

public class RulesetSettingsAlternate extends AppCompatActivity {
    FileHandlerClass fHC;
    String valgtEn;
    String valgtTo;
    String valgtTre;
    String valgtFire;
    String valgtFem;
    String valgtSeks;
    String hoppredskap;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ruleset_settings);

        Intent intent = getIntent();
        valgtEn = intent.getStringExtra("valgtEn");
        valgtTo = intent.getStringExtra("valgtTo");
        valgtTre = intent.getStringExtra("valgtTre");
        valgtFire = intent.getStringExtra("valgtFire");
        valgtFem = intent.getStringExtra("valgtFem");
        valgtSeks = intent.getStringExtra("valgtSeks");
        hoppredskap = intent.getStringExtra("hoppRedskap");

        fHC = new FileHandlerClass(this);
        setLanguages();
        setUpCheckboxes();
    }

    public void setUpCheckboxes(){
        CheckBox ueg = findViewById(R.id.UEGCheckBox);
        CheckBox ngtf = findViewById(R.id.NGTFCheckBox);
        CheckBox hordaland = findViewById(R.id.HordalandCheckBox);

        String ruleset = fHC.getRuleset();

        if(ruleset.equals("UEG")){
            ueg.setChecked(true);
            ngtf.setChecked(false);
            hordaland.setChecked(false);
        }else if (ruleset.equals("NGTF")){
            ueg.setChecked(false);
            ngtf.setChecked(true);
            hordaland.setChecked(false);
        }else if (ruleset.equals("Hordaland")){
            ueg.setChecked(false);
            ngtf.setChecked(false);
            hordaland.setChecked(true);
        }
    }
    public void buttonBackPressed(View view){
        Intent i = new Intent();
        i.putExtra("valgtEn", valgtEn);
        i.putExtra("valgtTo", valgtTo);
        i.putExtra("valgtTre", valgtTre);
        i.putExtra("valgtFire", valgtFire);
        i.putExtra("valgtFem", valgtFem);
        i.putExtra("valgtSeks", valgtSeks);
        i.putExtra("hoppRedskap", hoppredskap);
        setResult(RESULT_OK, i);
        finish();
    }

    @Override
    public void onBackPressed() {
        Intent i = new Intent();
        i.putExtra("valgtEn", valgtEn);
        i.putExtra("valgtTo", valgtTo);
        i.putExtra("valgtTre", valgtTre);
        i.putExtra("valgtFire", valgtFire);
        i.putExtra("valgtFem", valgtFem);
        i.putExtra("valgtSeks", valgtSeks);
        i.putExtra("hoppRedskap", hoppredskap);
        setResult(RESULT_OK, i);
        finish();
    }

    public void onCheckButtonPressed(View view){

        CheckBox checkBox = findViewById(view.getId());

        CheckBox ueg = findViewById(R.id.UEGCheckBox);
        CheckBox ngtf = findViewById(R.id.NGTFCheckBox);
        CheckBox hordaland = findViewById(R.id.HordalandCheckBox);

        if(ueg.getId() == checkBox.getId()){
            ueg.setChecked(true);
            ngtf.setChecked(false);
            hordaland.setChecked(false);
            fHC.savePressedRuleset("UEG");
        }else if(ngtf.getId() == checkBox.getId()){
            ueg.setChecked(false);
            ngtf.setChecked(true);
            hordaland.setChecked(false);
            fHC.savePressedRuleset("NGTF");
        }else if(hordaland.getId() == checkBox.getId()){
            ueg.setChecked(false);
            ngtf.setChecked(false);
            hordaland.setChecked(true);
            fHC.savePressedRuleset("Hordaland");
        }


    }


    public void setLanguages(){
        TextView rulesetTitle = findViewById(R.id.rulesetTitle);
        CheckBox UEGCheckBox = findViewById(R.id.UEGCheckBox);
        CheckBox NGTFCheckBox = findViewById(R.id.NGTFCheckBox);
        CheckBox HordalandCheckBox = findViewById(R.id.HordalandCheckBox);
        Button backButtonRuleset = findViewById(R.id.backButtonRuleset);
        //CheckBox customButton = findViewById(R.id.customTegnButton);

        TextClassGetter tCG = new TextClassGetter(this);
        String[] array = tCG.getText("RulesetSettings");
        rulesetTitle.setText(array[0]);
        UEGCheckBox.setText(array[1]);
        NGTFCheckBox.setText(array[2]);
        HordalandCheckBox.setText(array[3]);
        backButtonRuleset.setText(array[4]);
        //customButton.setText(array[4]);
    }



}
