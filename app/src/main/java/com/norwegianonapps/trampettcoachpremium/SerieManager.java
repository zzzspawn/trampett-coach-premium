package com.norwegianonapps.trampettcoachpremium;
import android.content.Context;
import android.util.Log;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

public class SerieManager {

    List<UtenHoppredskapSerie> trampList;
    List<MedHoppredskapSerie> hestList;
    List<SpesialHopp> spesialHoppList;
    String maxMuligSkru;
    Context context;
    FileHandlerClass fileHandlerClass;
    public SerieManager(Context context){
        this.context = context;
        trampList = new ArrayList<>();
        hestList = new ArrayList<>();
        spesialHoppList = new ArrayList<>();
        maxMuligSkru = "5";
        fileHandlerClass = new FileHandlerClass(context);
    }


    public void generateSpesialHopp(){
        if(fileHandlerClass.getRuleset().equalsIgnoreCase("ngtf")) {
            SpesialHopp strekkHopp = new SpesialHopp("l", new BigDecimal("0.0"), "Strekkhopp");
            SpesialHopp xHopp = new SpesialHopp("x", new BigDecimal("0.0"), "X-Hopp");
            SpesialHopp splitthopp = new SpesialHopp("w", new BigDecimal("0.05"), "Splitthopp");
            spesialHoppList.add(strekkHopp);
            spesialHoppList.add(xHopp);
            spesialHoppList.add(splitthopp);
        }
    }
    public void generateEnkleSaltoer(){
        //legg inn kroppert,Kroppert:
        leggTilEnkle(Posisjon.KROPPERT);
        //legg inn kroppert,strak:
        leggTilEnkle(Posisjon.PIKERT);
        //Legg inn pikert,pikert
        leggTilEnkle(Posisjon.STRAK);
    }
    public void generateDobleSaltoer(){

        //legg inn kroppert,Kroppert:
        leggTilDoble(Posisjon.KROPPERT, Posisjon.KROPPERT);
        //Legg inn pikert,pikert
        leggTilDoble(Posisjon.PIKERT, Posisjon.PIKERT);
        //Legg inn strak,kroppert
        leggTilDoble(Posisjon.STRAK, Posisjon.STRAK);
        //Legg inn strak,kroppert
        leggTilDoble(Posisjon.STRAK, Posisjon.KROPPERT);
    }
    public void generateTripleSaltoer() {
        Salto salto1 = new Salto(Posisjon.KROPPERT,new BigDecimal("0.0"));
        Salto salto2 = new Salto(Posisjon.KROPPERT,new BigDecimal("0.0"));
        Salto salto3 = new Salto(Posisjon.KROPPERT,new BigDecimal("0.5"));
        UtenHoppredskapSerie uhR = new UtenHoppredskapSerie(context,salto1,salto2,salto3);
        trampList.add(uhR);

        salto1 = new Salto(Posisjon.PIKERT,new BigDecimal("0.0"));
        salto2 = new Salto(Posisjon.PIKERT,new BigDecimal("0.0"));
        salto3 = new Salto(Posisjon.PIKERT,new BigDecimal("0.5"));
        uhR = new UtenHoppredskapSerie(context,salto1,salto2,salto3);
        //System.out.println(salto3.getSkru());
        //System.out.println();
        trampList.add(uhR);

        salto1 = new Salto(Posisjon.PIKERT,new BigDecimal("0.0"));
        salto2 = new Salto(Posisjon.PIKERT,new BigDecimal("0.0"));
        salto3 = new Salto(Posisjon.PIKERT,new BigDecimal("1.5"));
        uhR = new UtenHoppredskapSerie(context,salto1,salto2,salto3);
        trampList.add(uhR);

        if(fileHandlerClass.getRuleset().equalsIgnoreCase("ngtf")) {
            salto1 = new Salto(Posisjon.PIKERT, new BigDecimal("0.0"));
            salto2 = new Salto(Posisjon.PIKERT, new BigDecimal("0.0"));
            salto3 = new Salto(Posisjon.STRAK, new BigDecimal("1.5"));
            uhR = new UtenHoppredskapSerie(context, salto1, salto2, salto3);
            trampList.add(uhR);
        }

        salto1 = new Salto(Posisjon.KROPPERT,new BigDecimal("1.0"));
        salto2 = new Salto(Posisjon.KROPPERT,new BigDecimal("0.5"));
        salto3 = new Salto(Posisjon.KROPPERT,new BigDecimal("0.0"));
        uhR = new UtenHoppredskapSerie(context,salto1,salto2,salto3);
        trampList.add(uhR);

        salto1 = new Salto(Posisjon.KROPPERT,new BigDecimal("1.0"));
        salto2 = new Salto(Posisjon.KROPPERT,new BigDecimal("1.0"));
        salto3 = new Salto(Posisjon.KROPPERT,new BigDecimal("0.5"));
        uhR = new UtenHoppredskapSerie(context,salto1,salto2,salto3);
        trampList.add(uhR);

        salto1 = new Salto(Posisjon.KROPPERT,new BigDecimal("1.0"));
        salto2 = new Salto(Posisjon.KROPPERT,new BigDecimal("1.0"));
        salto3 = new Salto(Posisjon.KROPPERT,new BigDecimal("1.5"));
        uhR = new UtenHoppredskapSerie(context,salto1,salto2,salto3);
        trampList.add(uhR);

        if(fileHandlerClass.getRuleset().equalsIgnoreCase("ngtf")) {
            Log.v("gflkjhklo", "Genererer trippel med strak");
            salto1 = new Salto(Posisjon.STRAK, new BigDecimal("0.0"));
            salto2 = new Salto(Posisjon.STRAK, new BigDecimal("0.0"));
            salto3 = new Salto(Posisjon.STRAK, new BigDecimal("0.5"));
            uhR = new UtenHoppredskapSerie(context, salto1, salto2, salto3);
            trampList.add(uhR);

            salto1 = new Salto(Posisjon.STRAK, new BigDecimal("1.0"));
            salto2 = new Salto(Posisjon.STRAK, new BigDecimal("0.5"));
            salto3 = new Salto(Posisjon.STRAK, new BigDecimal("0.0"));
            uhR = new UtenHoppredskapSerie(context, salto1, salto2, salto3);
            trampList.add(uhR);

            salto1 = new Salto(Posisjon.STRAK, new BigDecimal("1.0"));
            salto2 = new Salto(Posisjon.STRAK, new BigDecimal("1.0"));
            salto3 = new Salto(Posisjon.STRAK, new BigDecimal("0.5"));
            uhR = new UtenHoppredskapSerie(context, salto1, salto2, salto3);
            trampList.add(uhR);

            salto1 = new Salto(Posisjon.STRAK, new BigDecimal("1.0"));
            salto2 = new Salto(Posisjon.STRAK, new BigDecimal("1.0"));
            salto3 = new Salto(Posisjon.STRAK, new BigDecimal("1.5"));
            uhR = new UtenHoppredskapSerie(context, salto1, salto2, salto3);
            trampList.add(uhR);
        }

    }

    public void generateOverslag(){
        leggTilOverslag();
    }
    public void generateOverslagSaltoer(){
        //legg inn kroppert,Kroppert:
        leggTilEnkleForover(Posisjon.KROPPERT);
        //legg inn kroppert,strak:
        leggTilEnkleForover(Posisjon.PIKERT);
        //Legg inn pikert,pikert
        leggTilEnkleForover(Posisjon.STRAK);
    }
    public void generateOverslagDobbelSaltoer(){
        leggTilDobleForover(Posisjon.KROPPERT, Posisjon.KROPPERT);
        leggTilDobleForover(Posisjon.PIKERT, Posisjon.PIKERT);
        if(fileHandlerClass.getRuleset().equalsIgnoreCase("ngtf")) {
            leggTilDobleForover(Posisjon.STRAK, Posisjon.KROPPERT);
            leggTilDobleForover(Posisjon.STRAK, Posisjon.STRAK);
        }
    }
    public void generateTSU(){
        leggTilEnkleBakover(Posisjon.KROPPERT);
        leggTilEnkleBakover(Posisjon.PIKERT);
        leggTilEnkleBakover(Posisjon.STRAK);
    }
    public void generateDobbelTSU(){
        leggTilDobleBakover(Posisjon.KROPPERT,Posisjon.KROPPERT);
        leggTilDobleBakover(Posisjon.PIKERT,Posisjon.PIKERT);
        if(fileHandlerClass.getRuleset().equalsIgnoreCase("ngtf")) {
            leggTilDobleBakover(Posisjon.STRAK, Posisjon.STRAK);
        }
    }


    public void leggTilEnkle(Enum<Posisjon> posisjon){
        BigDecimal skru = new BigDecimal("0.0");
        UtenHoppredskapSerie uhR = null;
        if(posisjon == Posisjon.KROPPERT){
            Salto salto = new Salto(posisjon, new BigDecimal("0.0"));
            uhR = new UtenHoppredskapSerie(context,salto);
            trampList.add(uhR);

            salto = new Salto(posisjon, new BigDecimal("0.5"));
            uhR = new UtenHoppredskapSerie(context,salto);
            trampList.add(uhR);

            salto = new Salto(posisjon, new BigDecimal("1.0"));
            uhR = new UtenHoppredskapSerie(context,salto);
            trampList.add(uhR);

        }else if(posisjon == Posisjon.PIKERT){

            Salto salto = new Salto(posisjon, new BigDecimal("0.0"));
            uhR = new UtenHoppredskapSerie(context,salto);
            trampList.add(uhR);

            salto = new Salto(posisjon, new BigDecimal("0.5"));
            uhR = new UtenHoppredskapSerie(context,salto);
            trampList.add(uhR);

        }else if(posisjon == Posisjon.STRAK){

            Salto salto = new Salto(posisjon, new BigDecimal("0.0"));
            uhR = new UtenHoppredskapSerie(context,salto);
            trampList.add(uhR);

            salto = new Salto(posisjon, new BigDecimal("0.5"));
            uhR = new UtenHoppredskapSerie(context,salto);
            trampList.add(uhR);

            salto = new Salto(posisjon, new BigDecimal("1.0"));
            uhR = new UtenHoppredskapSerie(context,salto);
            trampList.add(uhR);

            salto = new Salto(posisjon, new BigDecimal("1.5"));
            uhR = new UtenHoppredskapSerie(context,salto);
            trampList.add(uhR);

            salto = new Salto(posisjon, new BigDecimal("2.0"));
            uhR = new UtenHoppredskapSerie(context,salto);
            trampList.add(uhR);

            salto = new Salto(posisjon, new BigDecimal("2.5"));
            uhR = new UtenHoppredskapSerie(context,salto);
            trampList.add(uhR);

            salto = new Salto(posisjon, new BigDecimal("3.0"));
            uhR = new UtenHoppredskapSerie(context,salto);
            trampList.add(uhR);

            salto = new Salto(posisjon, new BigDecimal("3.5"));
            uhR = new UtenHoppredskapSerie(context,salto);
            trampList.add(uhR);


        }
    }
    public void leggTilDoble(Enum<Posisjon> posisjon1, Enum<Posisjon> posisjon2) {
        UtenHoppredskapSerie uhR = null;
        if((posisjon1 == Posisjon.KROPPERT && posisjon2 == Posisjon.KROPPERT)){

            Salto salto1 = new Salto(posisjon1,new BigDecimal("0.0"));
            Salto salto2 = new Salto(posisjon2,new BigDecimal("0.0"));
            uhR = new UtenHoppredskapSerie(context,salto1, salto2);
            trampList.add(uhR);

            salto1 = new Salto(posisjon1,new BigDecimal("0.0"));
            salto2 = new Salto(posisjon2,new BigDecimal("0.5"));
            uhR = new UtenHoppredskapSerie(context,salto1, salto2);
            trampList.add(uhR);


            salto1 = new Salto(posisjon1,new BigDecimal("0.0"));
            salto2 = new Salto(posisjon2,new BigDecimal("1.5"));
            uhR = new UtenHoppredskapSerie(context,salto1, salto2);
            trampList.add(uhR);


            salto1 = new Salto(posisjon1,new BigDecimal("0.5"));
            salto2 = new Salto(posisjon2,new BigDecimal("0.0"));
            uhR = new UtenHoppredskapSerie(context,salto1, salto2);
            trampList.add(uhR);


            salto1 = new Salto(posisjon1,new BigDecimal("0.5"));
            salto2 = new Salto(posisjon2,new BigDecimal("1.0"));
            uhR = new UtenHoppredskapSerie(context,salto1, salto2);
            trampList.add(uhR);

            salto1 = new Salto(posisjon1,new BigDecimal("1.0"));
            salto2 = new Salto(posisjon2,new BigDecimal("0.5"));
            uhR = new UtenHoppredskapSerie(context,salto1, salto2);
            trampList.add(uhR);

            salto1 = new Salto(posisjon1,new BigDecimal("1.0"));
            salto2 = new Salto(posisjon2,new BigDecimal("1.5"));
            uhR = new UtenHoppredskapSerie(context,salto1, salto2);
            trampList.add(uhR);

        }else if(posisjon1 == Posisjon.PIKERT && posisjon2 == Posisjon.PIKERT){

            Salto salto1 = new Salto(posisjon1,new BigDecimal("0.0"));
            Salto salto2 = new Salto(posisjon2,new BigDecimal("0.0"));
            uhR = new UtenHoppredskapSerie(context,salto1, salto2);
            trampList.add(uhR);

            salto1 = new Salto(posisjon1,new BigDecimal("0.0"));
            salto2 = new Salto(posisjon2,new BigDecimal("0.5"));
            uhR = new UtenHoppredskapSerie(context,salto1, salto2);
            trampList.add(uhR);

            salto1 = new Salto(posisjon1,new BigDecimal("0.0"));
            salto2 = new Salto(posisjon2,new BigDecimal("1.5"));
            uhR = new UtenHoppredskapSerie(context,salto1, salto2);
            trampList.add(uhR);

        }else if (posisjon1 == Posisjon.STRAK && posisjon2 == Posisjon.STRAK){
            Salto salto1 = new Salto(posisjon1,new BigDecimal("0.0"));
            Salto salto2 = new Salto(posisjon2,new BigDecimal("0.0"));
            uhR = new UtenHoppredskapSerie(context,salto1, salto2);
            trampList.add(uhR);

            salto1 = new Salto(posisjon1,new BigDecimal("0.0"));
            salto2 = new Salto(posisjon2,new BigDecimal("0.5"));
            uhR = new UtenHoppredskapSerie(context,salto1, salto2);
            trampList.add(uhR);

            salto1 = new Salto(posisjon1,new BigDecimal("0.0"));
            salto2 = new Salto(posisjon2,new BigDecimal("1.5"));
            uhR = new UtenHoppredskapSerie(context,salto1, salto2);
            trampList.add(uhR);


            salto1 = new Salto(posisjon1,new BigDecimal("0.5"));
            salto2 = new Salto(posisjon2,new BigDecimal("0.0"));
            uhR = new UtenHoppredskapSerie(context,salto1, salto2);
            trampList.add(uhR);

            salto1 = new Salto(posisjon1,new BigDecimal("1.0"));
            salto2 = new Salto(posisjon2,new BigDecimal("0.5"));
            uhR = new UtenHoppredskapSerie(context,salto1, salto2);
            trampList.add(uhR);

            salto1 = new Salto(posisjon1,new BigDecimal("1.0"));
            salto2 = new Salto(posisjon2,new BigDecimal("1.5"));
            uhR = new UtenHoppredskapSerie(context,salto1, salto2);
            trampList.add(uhR);

            salto1 = new Salto(posisjon1,new BigDecimal("1.0"));
            salto2 = new Salto(posisjon2,new BigDecimal("2.5"));
            uhR = new UtenHoppredskapSerie(context,salto1, salto2);
            trampList.add(uhR);

            salto1 = new Salto(posisjon1,new BigDecimal("1.5"));
            salto2 = new Salto(posisjon2,new BigDecimal("2.0"));
            uhR = new UtenHoppredskapSerie(context,salto1, salto2);
            trampList.add(uhR);

        }
        // else if (posisjon1 == Posisjon.STRAK && posisjon2 == Posisjon.KROPPERT){

        //   Salto salto1 = new Salto(posisjon1,new BigDecimal("0.5"));
        // Salto salto2 = new Salto(posisjon2,new BigDecimal("0.0"));
        //uhR = new UtenHoppredskapSerie(context,salto1, salto2);
        //trampList.add(uhR);

        //}

    }
    public void leggTilOverslag(){

        //Overslag
        HestHopp hestHopp = new HestHopp(Posisjon.STRAK,new BigDecimal("0.0"),Retning.FOROVER);
        MedHoppredskapSerie mHS = new MedHoppredskapSerie(context,hestHopp);
        hestList.add(mHS);
        if(fileHandlerClass.getRuleset().equalsIgnoreCase("ngtf")) {
            hestHopp = new HestHopp(Posisjon.STRAK, new BigDecimal("0.5"), Retning.FOROVER);
            mHS = new MedHoppredskapSerie(context, hestHopp);
            hestList.add(mHS);

            hestHopp = new HestHopp(Posisjon.STRAK, new BigDecimal("1.0"), Retning.FOROVER);
            mHS = new MedHoppredskapSerie(context, hestHopp);
            hestList.add(mHS);

            hestHopp = new HestHopp(Posisjon.STRAK, new BigDecimal("1.5"), Retning.FOROVER);
            mHS = new MedHoppredskapSerie(context, hestHopp);
            hestList.add(mHS);

        }

        //90på90av
        hestHopp = new HestHopp(Posisjon.STRAK,new BigDecimal("0.0"),Retning.BAKOVER);
        mHS = new MedHoppredskapSerie(context,hestHopp);
        hestList.add(mHS);

    }
    public void leggTilEnkleForover(Posisjon posisjon){
        BigDecimal skru = new BigDecimal("0.0");
        if(posisjon == Posisjon.KROPPERT){

            HestHopp hestHopp1 = new HestHopp(Posisjon.STRAK,Retning.FOROVER);
            HestHopp hestHopp2 = new HestHopp(posisjon,Retning.FOROVER);
            MedHoppredskapSerie mHS = new MedHoppredskapSerie(context,hestHopp1,hestHopp2);
            hestList.add(mHS);

            hestHopp1 = new HestHopp(Posisjon.STRAK,Retning.FOROVER);
            hestHopp2 = new HestHopp(posisjon,new BigDecimal("0.5"),Retning.FOROVER);
            mHS = new MedHoppredskapSerie(context,hestHopp1,hestHopp2);
            hestList.add(mHS);

            hestHopp1 = new HestHopp(Posisjon.STRAK,Retning.FOROVER);
            hestHopp2 = new HestHopp(posisjon,new BigDecimal("1.5"),Retning.FOROVER);
            mHS = new MedHoppredskapSerie(context,hestHopp1,hestHopp2);
            hestList.add(mHS);

        }else if(posisjon == Posisjon.PIKERT){

            HestHopp hestHopp1 = new HestHopp(Posisjon.STRAK,Retning.FOROVER);
            HestHopp hestHopp2 = new HestHopp(posisjon,Retning.FOROVER);
            MedHoppredskapSerie mHS = new MedHoppredskapSerie(context,hestHopp1,hestHopp2);
            hestList.add(mHS);

            hestHopp1 = new HestHopp(Posisjon.STRAK,Retning.FOROVER);
            hestHopp2 = new HestHopp(posisjon,new BigDecimal("0.5"),Retning.FOROVER);
            mHS = new MedHoppredskapSerie(context,hestHopp1,hestHopp2);
            hestList.add(mHS);

        }else if(posisjon == Posisjon.STRAK){
            HestHopp hestHopp1 = new HestHopp(Posisjon.STRAK,Retning.FOROVER);
            HestHopp hestHopp2 = new HestHopp(posisjon,Retning.FOROVER);
            MedHoppredskapSerie mHS = new MedHoppredskapSerie(context,hestHopp1,hestHopp2);
            hestList.add(mHS);

            hestHopp1 = new HestHopp(Posisjon.STRAK,Retning.FOROVER);
            hestHopp2 = new HestHopp(posisjon,new BigDecimal("0.5"),Retning.FOROVER);
            mHS = new MedHoppredskapSerie(context,hestHopp1,hestHopp2);
            hestList.add(mHS);

            hestHopp1 = new HestHopp(Posisjon.STRAK,Retning.FOROVER);
            hestHopp2 = new HestHopp(posisjon,new BigDecimal("1.5"),Retning.FOROVER);
            mHS = new MedHoppredskapSerie(context,hestHopp1,hestHopp2);
            hestList.add(mHS);

            hestHopp1 = new HestHopp(Posisjon.STRAK,Retning.FOROVER);
            hestHopp2 = new HestHopp(posisjon,new BigDecimal("2.5"),Retning.FOROVER);
            mHS = new MedHoppredskapSerie(context,hestHopp1,hestHopp2);
            hestList.add(mHS);

            hestHopp1 = new HestHopp(Posisjon.STRAK,Retning.FOROVER);
            hestHopp2 = new HestHopp(posisjon,new BigDecimal("3.5"),Retning.FOROVER);
            mHS = new MedHoppredskapSerie(context,hestHopp1,hestHopp2);
            hestList.add(mHS);
        }
    }
    public void leggTilDobleForover(Posisjon posisjon1,Posisjon posisjon2){

        if((posisjon1 == Posisjon.KROPPERT && posisjon2 == Posisjon.KROPPERT)){

            HestHopp hestHopp1 = new HestHopp(Posisjon.STRAK,Retning.FOROVER);
            HestHopp hestHopp2 = new HestHopp(posisjon1,Retning.FOROVER);
            HestHopp hestHopp3 = new HestHopp(posisjon2,new BigDecimal("0.5"),Retning.FOROVER);
            MedHoppredskapSerie mHS = new MedHoppredskapSerie(context,hestHopp1,hestHopp2,hestHopp3);
            hestList.add(mHS);

            hestHopp1 = new HestHopp(Posisjon.STRAK,Retning.FOROVER);
            hestHopp2 = new HestHopp(posisjon1,Retning.FOROVER);
            hestHopp3 = new HestHopp(posisjon2,new BigDecimal("1.5"),Retning.FOROVER);
            mHS = new MedHoppredskapSerie(context,hestHopp1,hestHopp2,hestHopp3);
            hestList.add(mHS);

            hestHopp1 = new HestHopp(Posisjon.STRAK,Retning.FOROVER);
            hestHopp2 = new HestHopp(posisjon1,new BigDecimal("1.0"),Retning.FOROVER);
            hestHopp3 = new HestHopp(posisjon2,new BigDecimal("0.5"),Retning.FOROVER);
            mHS = new MedHoppredskapSerie(context,hestHopp1,hestHopp2,hestHopp3);
            hestList.add(mHS);

            hestHopp1 = new HestHopp(Posisjon.STRAK,Retning.FOROVER);
            hestHopp2 = new HestHopp(posisjon1,new BigDecimal("1.0"),Retning.FOROVER);
            hestHopp3 = new HestHopp(posisjon2,new BigDecimal("1.5"),Retning.FOROVER);
            mHS = new MedHoppredskapSerie(context,hestHopp1,hestHopp2,hestHopp3);
            hestList.add(mHS);

        }else if(posisjon1 == Posisjon.PIKERT && posisjon2 == Posisjon.PIKERT){

            HestHopp hestHopp1 = new HestHopp(Posisjon.STRAK,Retning.FOROVER);
            HestHopp hestHopp2 = new HestHopp(posisjon1,Retning.FOROVER);
            HestHopp hestHopp3 = new HestHopp(posisjon2,new BigDecimal("0.5"),Retning.FOROVER);
            MedHoppredskapSerie mHS = new MedHoppredskapSerie(context,hestHopp1,hestHopp2,hestHopp3);
            hestList.add(mHS);

            hestHopp1 = new HestHopp(Posisjon.STRAK,Retning.FOROVER);
            hestHopp2 = new HestHopp(posisjon1,Retning.FOROVER);
            hestHopp3 = new HestHopp(posisjon2,new BigDecimal("1.5"),Retning.FOROVER);
            mHS = new MedHoppredskapSerie(context,hestHopp1,hestHopp2,hestHopp3);
            hestList.add(mHS);

        }else if(posisjon1 == Posisjon.STRAK && posisjon2 == Posisjon.KROPPERT){
            HestHopp hestHopp1 = new HestHopp(Posisjon.STRAK,Retning.FOROVER);
            HestHopp hestHopp2 = new HestHopp(posisjon1,new BigDecimal("0.5"),Retning.FOROVER);
            HestHopp hestHopp3 = new HestHopp(posisjon2,Retning.FOROVER);
            MedHoppredskapSerie mHS = new MedHoppredskapSerie(context,hestHopp1,hestHopp2,hestHopp3);
            hestList.add(mHS);

            hestHopp1 = new HestHopp(Posisjon.STRAK,Retning.FOROVER);
            hestHopp2 = new HestHopp(posisjon1,new BigDecimal("0.5"),Retning.FOROVER);
            hestHopp3 = new HestHopp(posisjon2,new BigDecimal("1.0"),Retning.FOROVER);
            mHS = new MedHoppredskapSerie(context,hestHopp1,hestHopp2,hestHopp3);
            hestList.add(mHS);

        }else if(posisjon1 == Posisjon.STRAK && posisjon2 == Posisjon.STRAK){

            HestHopp hestHopp1 = new HestHopp(Posisjon.STRAK,Retning.FOROVER);
            HestHopp hestHopp2 = new HestHopp(posisjon1,Retning.FOROVER);
            HestHopp hestHopp3 = new HestHopp(posisjon2,new BigDecimal("0.5"),Retning.FOROVER);
            MedHoppredskapSerie mHS = new MedHoppredskapSerie(context,hestHopp1,hestHopp2,hestHopp3);
            hestList.add(mHS);

        }

    }
    public void leggTilEnkleBakover(Posisjon posisjon){

        if(posisjon == Posisjon.KROPPERT){
            HestHopp hestHopp1 = new HestHopp(Posisjon.STRAK,Retning.BAKOVER);
            HestHopp hestHopp2 = new HestHopp(posisjon,Retning.BAKOVER);
            MedHoppredskapSerie mHS = new MedHoppredskapSerie(context,hestHopp1,hestHopp2);
            hestList.add(mHS);

            hestHopp1 = new HestHopp(Posisjon.STRAK,Retning.BAKOVER);
            hestHopp2 = new HestHopp(posisjon,new BigDecimal("1.0"),Retning.BAKOVER);
            mHS = new MedHoppredskapSerie(context,hestHopp1,hestHopp2);
            hestList.add(mHS);

        }else if(posisjon == Posisjon.PIKERT){
            HestHopp hestHopp1 = new HestHopp(Posisjon.STRAK,Retning.BAKOVER);
            HestHopp hestHopp2 = new HestHopp(posisjon,Retning.BAKOVER);
            MedHoppredskapSerie mHS = new MedHoppredskapSerie(context,hestHopp1,hestHopp2);
            hestList.add(mHS);
        }else if(posisjon == Posisjon.STRAK){
            HestHopp hestHopp1 = new HestHopp(Posisjon.STRAK,Retning.BAKOVER);
            HestHopp hestHopp2 = new HestHopp(posisjon,Retning.BAKOVER);
            MedHoppredskapSerie mHS = new MedHoppredskapSerie(context,hestHopp1,hestHopp2);
            hestList.add(mHS);

            hestHopp1 = new HestHopp(Posisjon.STRAK,Retning.BAKOVER);
            hestHopp2 = new HestHopp(posisjon,new BigDecimal("1.0"),Retning.BAKOVER);
            mHS = new MedHoppredskapSerie(context,hestHopp1,hestHopp2);
            hestList.add(mHS);

            hestHopp1 = new HestHopp(Posisjon.STRAK,Retning.BAKOVER);
            hestHopp2 = new HestHopp(posisjon,new BigDecimal("2.0"),Retning.BAKOVER);
            mHS = new MedHoppredskapSerie(context,hestHopp1,hestHopp2);
            hestList.add(mHS);
        }
    }
    public void leggTilDobleBakover(Posisjon posisjon1,Posisjon posisjon2){

        if(posisjon1 == Posisjon.KROPPERT && posisjon2 == Posisjon.KROPPERT){

            HestHopp hestHopp1 = new HestHopp(Posisjon.STRAK,Retning.BAKOVER);
            HestHopp hestHopp2 = new HestHopp(posisjon1,Retning.BAKOVER);
            HestHopp hestHopp3 = new HestHopp(posisjon2,Retning.BAKOVER);
            MedHoppredskapSerie mHS = new MedHoppredskapSerie(context,hestHopp1,hestHopp2,hestHopp3);
            hestList.add(mHS);

            hestHopp1 = new HestHopp(Posisjon.STRAK,Retning.BAKOVER);
            hestHopp2 = new HestHopp(posisjon1,Retning.BAKOVER);
            hestHopp3 = new HestHopp(posisjon2,new BigDecimal("1.0"),Retning.BAKOVER);
            mHS = new MedHoppredskapSerie(context,hestHopp1,hestHopp2,hestHopp3);
            hestList.add(mHS);

        }else if(posisjon1 == Posisjon.PIKERT && posisjon2 == Posisjon.PIKERT){
            HestHopp hestHopp1 = new HestHopp(Posisjon.STRAK,Retning.BAKOVER);
            HestHopp hestHopp2 = new HestHopp(posisjon1,Retning.BAKOVER);
            HestHopp hestHopp3 = new HestHopp(posisjon2,Retning.BAKOVER);
            MedHoppredskapSerie mHS = new MedHoppredskapSerie(context,hestHopp1,hestHopp2,hestHopp3);
            hestList.add(mHS);
        }else if(posisjon1 == Posisjon.STRAK && posisjon2 == Posisjon.STRAK){

            HestHopp hestHopp1 = new HestHopp(Posisjon.STRAK,Retning.BAKOVER);
            HestHopp hestHopp2 = new HestHopp(posisjon1,Retning.BAKOVER);
            HestHopp hestHopp3 = new HestHopp(posisjon2,Retning.BAKOVER);
            MedHoppredskapSerie mHS = new MedHoppredskapSerie(context,hestHopp1,hestHopp2,hestHopp3);
            hestList.add(mHS);

            hestHopp1 = new HestHopp(Posisjon.STRAK,Retning.BAKOVER);
            hestHopp2 = new HestHopp(posisjon1,Retning.BAKOVER);
            hestHopp3 = new HestHopp(posisjon2,new BigDecimal("1.0"),Retning.BAKOVER);
            mHS = new MedHoppredskapSerie(context,hestHopp1,hestHopp2,hestHopp3);
            hestList.add(mHS);

        }
    }


    public List<UtenHoppredskapSerie> getUtenHoppredskapList(){
        return trampList;
    }
    public List<MedHoppredskapSerie> getMedHoppredskapList() {
        return hestList;
    }
    public List<SpesialHopp> getSpesialHoppList() {
        return spesialHoppList;
    }

    SpesialHopp customHoppSpesial;
    UtenHoppredskapSerie customHoppUHR;
    MedHoppredskapSerie customHoppMHR;
    public void customHopp(String name,String value,Retning retning,String apparatus, Hopp salto0, Hopp salto1, Hopp salto2, Hopp salto3, int nrOfRotations) {

        if(apparatus.equals("Pegasus")){

            if(nrOfRotations == 0){
                customHoppMHR = new MedHoppredskapSerie(context,new HestHopp(Posisjon.STRAK,retning));
            }if(nrOfRotations == 1){
                if(salto1.getSkru() == null){
                    customHoppMHR = new MedHoppredskapSerie(context, new HestHopp(Posisjon.STRAK, retning), new HestHopp(salto1.getPosisjon(), retning));
                }else {
                    customHoppMHR = new MedHoppredskapSerie(context, new HestHopp(Posisjon.STRAK, retning), new HestHopp((Posisjon) salto1.getPosisjon(), salto1.getSkru(), retning));
                }
            }else if(nrOfRotations == 2){
                if(salto1.getSkru() == null && salto2.getSkru() != null){
                    customHoppMHR = new MedHoppredskapSerie(context,new HestHopp(Posisjon.STRAK,retning),new HestHopp((Posisjon) salto1.getPosisjon(),retning),new HestHopp((Posisjon) salto2.getPosisjon(),salto2.getSkru(),retning));
                }else if(salto1.getSkru() != null && salto2.getSkru() == null){
                    customHoppMHR = new MedHoppredskapSerie(context,new HestHopp(Posisjon.STRAK,retning),new HestHopp((Posisjon) salto1.getPosisjon(),salto1.getSkru(),retning),new HestHopp((Posisjon) salto2.getPosisjon(),retning));
                }else if(salto1.getSkru() == null && salto2.getSkru() == null){
                    customHoppMHR = new MedHoppredskapSerie(context,new HestHopp(Posisjon.STRAK,retning),new HestHopp((Posisjon) salto1.getPosisjon(),retning),new HestHopp((Posisjon) salto2.getPosisjon(),retning));
                }else {
                    customHoppMHR = new MedHoppredskapSerie(context, new HestHopp(Posisjon.STRAK, retning), new HestHopp((Posisjon) salto1.getPosisjon(), salto1.getSkru(), retning), new HestHopp((Posisjon) salto2.getPosisjon(), salto2.getSkru(), retning));
                }
            }

        }else if(apparatus.equals("Trampet")){

            if(nrOfRotations == 0){
                Log.v("shitstormalpha","name: " + name);
                Log.v("shitstormalpha","value: " + value);
                customHoppSpesial = new SpesialHopp(name, new BigDecimal(value),name);
                Log.v("shitstormalpha","name: " + customHoppSpesial.getName());
                Log.v("shitstormalpha","value: " + customHoppSpesial.getVerdi());
            }if(nrOfRotations == 1){
                customHoppUHR = new UtenHoppredskapSerie(context,(Salto) salto1);
            }else if(nrOfRotations == 2){
                customHoppUHR = new UtenHoppredskapSerie(context,(Salto)salto1,(Salto)salto2);
            }else if(nrOfRotations == 3){
                customHoppUHR = new UtenHoppredskapSerie(context,(Salto)salto1,(Salto)salto2,(Salto)salto3);
            }

        }
    }

    public GenerelleHopp getCustomHopp(){
        GenerelleHopp genereltHopp = null;
        if(customHoppSpesial != null){
            Log.v("shitstormalpha","Kjørte");
            genereltHopp = customHoppSpesial;
            Log.v("shitstormalpha","gen verdi: " + genereltHopp.getVerdi());
        }else if(customHoppUHR != null){
            genereltHopp = customHoppUHR;
        }else if(customHoppMHR != null){
            genereltHopp = customHoppMHR;
        }
        return genereltHopp;
    }
}
