package com.norwegianonapps.trampettcoachpremium;

import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import android.graphics.Typeface;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

public class AllCustom extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_all_custom);


        setUpLinearLayoutWithAllSeries();

    }










    public void setUpLinearLayoutWithAllSeries(){

        List<GenerelleHopp> alleGenerelleHopp = new ArrayList<>();



        FileHandlerClass fileHandlerClass = new FileHandlerClass(this);

        List<String> list = fileHandlerClass.getAllSeries();
        HoppStringConverter hoppStringConverter = new HoppStringConverter(this);
        LinearLayout linearLayout = findViewById(R.id.linlayAllCustomSeries);

        Log.v("vbcvbcxxx","List size: " + list.size());
        int i = 0;
        while (i < list.size()){
            alleGenerelleHopp.add(hoppStringConverter.getGenereltHoppFromString(list.get(i)));
            i++;
        }

        i = 0;
        while (i < alleGenerelleHopp.size()){

            TextView tvTemp = new TextView(this);
            GenerelleHopp g = alleGenerelleHopp.get(i);
            if(g == null){
                Log.v("vbcvbcxxx","null");
            }

            tvTemp.setText(alleGenerelleHopp.get(i).getName());

            if(!alleGenerelleHopp.get(i).isSpesialHopp()){
                Typeface typeface = null;
                if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.O) {
                    typeface = getResources().getFont(R.font.turntegnregular);
                } else {
                    typeface = Typeface.createFromAsset(getAssets(), "fonts/turntegnregular.ttf");
                }
                tvTemp.setTypeface(typeface);
            }

            TextView tvValueTemp = new TextView(this);

            tvValueTemp.setText("Value: " + alleGenerelleHopp.get(i).getVerdi());

            LinearLayout linlay = new LinearLayout(this);
            linlay.setOrientation(LinearLayout.HORIZONTAL);
            linlay.addView(tvTemp);
            linlay.addView(tvValueTemp);
            linearLayout.addView(linlay);

            i++;
        }





    }


}
