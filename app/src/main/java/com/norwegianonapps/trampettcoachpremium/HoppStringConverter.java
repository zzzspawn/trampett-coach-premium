package com.norwegianonapps.trampettcoachpremium;

import android.content.Context;
import android.util.Log;

import java.math.BigDecimal;

/**
 * Created by zzzsp on 07-Nov-17.
 */

public class HoppStringConverter {
    Context context;
    public HoppStringConverter(Context context){
        this.context = context;
    }



    public String getStringFromGenereltHopp(GenerelleHopp serie){

        String hoppredskap = serie.getHoppredskap();
        String name = serie.getName();
        //String antallRotasjoner = serie.getAntallRotasjoner();
        String posisjonEn = "";
        String posisjonTo = "";
        String posisjonTre = "";
        String verdi = "";
        String spesialHopp = "";
        String skruEn = "";
        String skruTo = "";
        String skruTre = "";
        String retning = "";
        String line ="";
        if(serie.getHoppredskap().equalsIgnoreCase("trampet")) {
            line = serie.getHoppredskap() + "@" + serie.getName() + "@" + serie.getAntallRotasjoner() + "@" + serie.getPosisjonEn() + "@" + serie.getPosisjonTo() +
                    "@" + serie.getPosisjonTre() + "@" + serie.getVerdi() + "@" + serie.isSpesialHopp() + "@" + serie.getSkruEn() +
                    "@" + serie.getSkruT0() + "@" + serie.getSkruTre() + "@" + serie.getRetning();
        }else {
            int rotasjoner = serie.getAntallRotasjoner()-1;

            line = serie.getHoppredskap() + "@" + serie.getName() + "@" + rotasjoner + "@" + serie.getPosisjonEn() + "@" + serie.getPosisjonTo() +
                    "@" + serie.getPosisjonTre() + "@" + serie.getVerdi() + "@" + serie.isSpesialHopp() + "@" + serie.getSkruEn() +
                    "@" + serie.getSkruT0() + "@" + serie.getSkruTre() + "@" + serie.getRetning();
        }
        return line;
    }

    public GenerelleHopp getGenereltHoppFromString(String line){

        GenerelleHopp generelleHopp = null;
        String hoppredskap = "";
        String name = "";
        String antallRotasjoner = "";
        String posisjonEn = "";
        String posisjonTo = "";
        String posisjonTre = "";
        String verdi = "";
        String spesialHopp = "";
        String skruEn = "";
        String skruTo = "";
        String skruTre = "";
        String retning = "";
        int tellerPosisjon = 0;
        int i = 0;
        while(i < line.length()){
            if(line.charAt(i) == '@'){
                tellerPosisjon++;
                i++;
            }
            if(tellerPosisjon == 0){
                hoppredskap = hoppredskap + line.charAt(i);
            }else if(tellerPosisjon == 1){
                name = name + line.charAt(i);
            }else if(tellerPosisjon == 2){
                antallRotasjoner = antallRotasjoner + line.charAt(i);
            }else if(tellerPosisjon == 3){
                posisjonEn = posisjonEn + line.charAt(i);
            }else if(tellerPosisjon == 4){
                posisjonTo = posisjonTo + line.charAt(i);
            }else if(tellerPosisjon == 5){
                posisjonTre = posisjonTre + line.charAt(i);
            }else if(tellerPosisjon == 6){
                verdi = verdi + line.charAt(i);
            }else if(tellerPosisjon == 7){
                spesialHopp = spesialHopp + line.charAt(i);
            }else if(tellerPosisjon == 8){
                skruEn = skruEn + line.charAt(i);
            }else if(tellerPosisjon == 9){
                skruTo = skruTo + line.charAt(i);
            }else if(tellerPosisjon == 10){
                skruTre = skruTre + line.charAt(i);
            }else if(tellerPosisjon == 11){
                retning = retning + line.charAt(i);
            }

            i++;
        }




        if(hoppredskap.equalsIgnoreCase("trampet")){

            if(spesialHopp.equalsIgnoreCase("true")){
                SpesialHopp spesialHoppActual = new SpesialHopp(name,new BigDecimal(verdi),name);
                generelleHopp = spesialHoppActual;
            }else {
                if(antallRotasjoner.equalsIgnoreCase("1")){
                    Salto salto1 = null;
                    if(posisjonEn.equalsIgnoreCase("KROPPERT")){
                        if(!skruEn.equalsIgnoreCase("null") || !skruEn.equalsIgnoreCase("")){
                            salto1 = new Salto(Posisjon.KROPPERT, new BigDecimal(skruEn));
                        }else {
                            salto1 = new Salto(Posisjon.KROPPERT);
                        }
                    }else if(posisjonEn.equalsIgnoreCase("PIKERT")){
                        if(!skruEn.equalsIgnoreCase("null") || !skruEn.equalsIgnoreCase("")){
                            salto1 = new Salto(Posisjon.PIKERT, new BigDecimal(skruEn));
                        }else {
                            salto1 = new Salto(Posisjon.PIKERT);
                        }
                    }else if(posisjonEn.equalsIgnoreCase("Strak")){
                        if(!skruEn.equalsIgnoreCase("null") || !skruEn.equalsIgnoreCase("")){
                            salto1 = new Salto(Posisjon.STRAK, new BigDecimal(skruEn));
                        }else {
                            salto1 = new Salto(Posisjon.STRAK);
                        }
                    }

                    UtenHoppredskapSerie utenHoppredskapSerie = new UtenHoppredskapSerie(context,salto1);
                    generelleHopp = utenHoppredskapSerie;

                }else if(antallRotasjoner.equalsIgnoreCase("2")){
                    Salto salto1 = null;
                    if(posisjonEn.equalsIgnoreCase("KROPPERT")){
                        if(!skruEn.equalsIgnoreCase("null") || !skruEn.equalsIgnoreCase("")){
                            salto1 = new Salto(Posisjon.KROPPERT, new BigDecimal(skruEn));
                        }else {
                            salto1 = new Salto(Posisjon.KROPPERT);
                        }
                    }else if(posisjonEn.equalsIgnoreCase("PIKERT")){
                        if(!skruEn.equalsIgnoreCase("null") || !skruEn.equalsIgnoreCase("")){
                            salto1 = new Salto(Posisjon.PIKERT, new BigDecimal(skruEn));
                        }else {
                            salto1 = new Salto(Posisjon.PIKERT);
                        }
                    }else if(posisjonEn.equalsIgnoreCase("Strak")){
                        if(!skruEn.equalsIgnoreCase("null") || !skruEn.equalsIgnoreCase("")){
                            salto1 = new Salto(Posisjon.STRAK, new BigDecimal(skruEn));
                        }else {
                            salto1 = new Salto(Posisjon.STRAK);
                        }
                    }



                    Salto salto2 = null;
                    if(posisjonTo.equalsIgnoreCase("KROPPERT")){
                        if(!skruTo.equalsIgnoreCase("null") || !skruTo.equalsIgnoreCase("")){
                            salto2 = new Salto(Posisjon.KROPPERT, new BigDecimal(skruTo));
                        }else {
                            salto2 = new Salto(Posisjon.KROPPERT);
                        }
                    }else if(posisjonTo.equalsIgnoreCase("PIKERT")){
                        if(!skruTo.equalsIgnoreCase("null") || !skruTo.equalsIgnoreCase("")){
                            salto2 = new Salto(Posisjon.PIKERT, new BigDecimal(skruTo));
                        }else {
                            salto2 = new Salto(Posisjon.PIKERT);
                        }
                    }else if(posisjonTo.equalsIgnoreCase("Strak")){
                        if(!skruTo.equalsIgnoreCase("null") || !skruTo.equalsIgnoreCase("")){
                            salto2 = new Salto(Posisjon.STRAK, new BigDecimal(skruTo));
                        }else {
                            salto2 = new Salto(Posisjon.STRAK);
                        }
                    }

                    UtenHoppredskapSerie utenHoppredskapSerie = new UtenHoppredskapSerie(context,salto1,salto2);
                    generelleHopp = utenHoppredskapSerie;

                }else if(antallRotasjoner.equalsIgnoreCase("3")){
                    Salto salto1 = null;
                    if(posisjonEn.equalsIgnoreCase("KROPPERT")){
                        if(!skruEn.equalsIgnoreCase("null") || !skruEn.equalsIgnoreCase("")){
                            salto1 = new Salto(Posisjon.KROPPERT, new BigDecimal(skruEn));
                        }else {
                            salto1 = new Salto(Posisjon.KROPPERT);
                        }
                    }else if(posisjonEn.equalsIgnoreCase("PIKERT")){
                        if(!skruEn.equalsIgnoreCase("null") || !skruEn.equalsIgnoreCase("")){
                            salto1 = new Salto(Posisjon.PIKERT, new BigDecimal(skruEn));
                        }else {
                            salto1 = new Salto(Posisjon.PIKERT);
                        }
                    }else if(posisjonEn.equalsIgnoreCase("Strak")){
                        if(!skruEn.equalsIgnoreCase("null") || !skruEn.equalsIgnoreCase("")){
                            salto1 = new Salto(Posisjon.STRAK, new BigDecimal(skruEn));
                        }else {
                            salto1 = new Salto(Posisjon.STRAK);
                        }
                    }



                    Salto salto2 = null;
                    if(posisjonTo.equalsIgnoreCase("KROPPERT")){
                        if(!skruTo.equalsIgnoreCase("null") || !skruTo.equalsIgnoreCase("")){
                            salto2 = new Salto(Posisjon.KROPPERT, new BigDecimal(skruTo));
                        }else {
                            salto2 = new Salto(Posisjon.KROPPERT);
                        }
                    }else if(posisjonTo.equalsIgnoreCase("PIKERT")){
                        if(!skruTo.equalsIgnoreCase("null") || !skruTo.equalsIgnoreCase("")){
                            salto2 = new Salto(Posisjon.PIKERT, new BigDecimal(skruTo));
                        }else {
                            salto2 = new Salto(Posisjon.PIKERT);
                        }
                    }else if(posisjonTo.equalsIgnoreCase("Strak")){
                        if(!skruTo.equalsIgnoreCase("null") || !skruTo.equalsIgnoreCase("")){
                            salto2 = new Salto(Posisjon.STRAK, new BigDecimal(skruTo));
                        }else {
                            salto2 = new Salto(Posisjon.STRAK);
                        }
                    }

                    Salto salto3 = null;
                    if(posisjonTre.equalsIgnoreCase("KROPPERT")){
                        if(!skruTo.equalsIgnoreCase("null") || !skruTo.equalsIgnoreCase("")){
                            salto3 = new Salto(Posisjon.KROPPERT, new BigDecimal(skruTre));
                        }else {
                            salto3 = new Salto(Posisjon.KROPPERT);
                        }
                    }else if(posisjonTre.equalsIgnoreCase("PIKERT")){
                        if(!skruTo.equalsIgnoreCase("null") || !skruTo.equalsIgnoreCase("")){
                            salto3 = new Salto(Posisjon.PIKERT, new BigDecimal(skruTre));
                        }else {
                            salto3 = new Salto(Posisjon.PIKERT);
                        }
                    }else if(posisjonTre.equalsIgnoreCase("STRAK")){
                        if(!skruTo.equalsIgnoreCase("null") || !skruTo.equalsIgnoreCase("")){
                            salto3 = new Salto(Posisjon.STRAK, new BigDecimal(skruTre));
                        }else {
                            salto3 = new Salto(Posisjon.STRAK);
                        }
                    }

                    UtenHoppredskapSerie utenHoppredskapSerie = new UtenHoppredskapSerie(context,salto1,salto2,salto3);
                    generelleHopp = utenHoppredskapSerie;

                }

            }

        }else if(hoppredskap.equalsIgnoreCase("Pegasus")) {
            Retning retningH = null;
            if (retning.equalsIgnoreCase(Retning.FOROVER.toString())) {
                retningH = Retning.FOROVER;
            }else if (retning.equalsIgnoreCase(Retning.BAKOVER.toString())) {
                retningH = Retning.BAKOVER;
            }


            if (antallRotasjoner.equalsIgnoreCase("0")) {
                HestHopp hestHopp1 = null;
                if (posisjonEn.equalsIgnoreCase("KROPPERT")) {
                    if (!skruEn.equalsIgnoreCase("null") || !skruEn.equalsIgnoreCase("")) {
                        hestHopp1 = new HestHopp(Posisjon.KROPPERT, new BigDecimal(skruEn), retningH);
                    } else {
                        hestHopp1 = new HestHopp(Posisjon.KROPPERT, retningH);
                    }
                } else if (posisjonEn.equalsIgnoreCase("PIKERT")) {
                    if (!skruEn.equalsIgnoreCase("null") || !skruEn.equalsIgnoreCase("")) {
                        hestHopp1 = new HestHopp(Posisjon.PIKERT, new BigDecimal(skruEn), retningH);
                    } else {
                        hestHopp1 = new HestHopp(Posisjon.PIKERT, retningH);
                    }
                } else if (posisjonEn.equalsIgnoreCase("Strak")) {
                    if (!skruEn.equalsIgnoreCase("null") || !skruEn.equalsIgnoreCase("")) {
                        hestHopp1 = new HestHopp(Posisjon.STRAK, new BigDecimal(skruEn), retningH);
                    } else {
                        hestHopp1 = new HestHopp(Posisjon.STRAK, retningH);
                    }
                }

                MedHoppredskapSerie medHoppredskapSerie = new MedHoppredskapSerie(context, hestHopp1);
                generelleHopp = medHoppredskapSerie;


            } else if (antallRotasjoner.equalsIgnoreCase("1")) {

                HestHopp hestHopp1 = null;
                if (posisjonEn.equalsIgnoreCase("KROPPERT")) {
                    if (!skruEn.equalsIgnoreCase("null") || !skruEn.equalsIgnoreCase("")) {
                        hestHopp1 = new HestHopp(Posisjon.KROPPERT, new BigDecimal(skruEn), retningH);
                    } else {
                        hestHopp1 = new HestHopp(Posisjon.KROPPERT, retningH);
                    }
                } else if (posisjonEn.equalsIgnoreCase("PIKERT")) {
                    if (!skruEn.equalsIgnoreCase("null") || !skruEn.equalsIgnoreCase("")) {
                        hestHopp1 = new HestHopp(Posisjon.PIKERT, new BigDecimal(skruEn), retningH);
                    } else {
                        hestHopp1 = new HestHopp(Posisjon.PIKERT, retningH);
                    }
                } else if (posisjonEn.equalsIgnoreCase("Strak")) {
                    if (!skruEn.equalsIgnoreCase("null") || !skruEn.equalsIgnoreCase("")) {
                        hestHopp1 = new HestHopp(Posisjon.STRAK, new BigDecimal(skruEn), retningH);
                    } else {
                        hestHopp1 = new HestHopp(Posisjon.STRAK, retningH);
                    }
                }

                HestHopp hestHopp2 = null;
                if (posisjonTo.equalsIgnoreCase("KROPPERT")) {
                    if (!skruTo.equalsIgnoreCase("null") || !skruTo.equalsIgnoreCase("")) {
                        hestHopp2 = new HestHopp(Posisjon.KROPPERT, new BigDecimal(skruTo), retningH);
                    } else {
                        hestHopp2 = new HestHopp(Posisjon.KROPPERT, retningH);
                    }
                } else if (posisjonTo.equalsIgnoreCase("PIKERT")) {
                    if (!skruTo.equalsIgnoreCase("null") || !skruTo.equalsIgnoreCase("")) {
                        hestHopp2 = new HestHopp(Posisjon.PIKERT, new BigDecimal(skruTo), retningH);
                    } else {
                        hestHopp2 = new HestHopp(Posisjon.PIKERT, retningH);
                    }
                } else if (posisjonTo.equalsIgnoreCase("Strak")) {
                    if (!skruTo.equalsIgnoreCase("null") || !skruTo.equalsIgnoreCase("")) {
                        hestHopp2 = new HestHopp(Posisjon.STRAK, new BigDecimal(skruTo), retningH);
                    } else {
                        hestHopp2 = new HestHopp(Posisjon.STRAK, retningH);
                    }
                }

                MedHoppredskapSerie medHoppredskapSerie = new MedHoppredskapSerie(context, hestHopp1, hestHopp2);
                generelleHopp = medHoppredskapSerie;

            } else if (antallRotasjoner.equalsIgnoreCase("2")) {
                HestHopp hestHopp1 = null;
                if (posisjonEn.equalsIgnoreCase("KROPPERT")) {
                    if (!skruEn.equalsIgnoreCase("null") || !skruEn.equalsIgnoreCase("")) {
                        hestHopp1 = new HestHopp(Posisjon.STRAK, new BigDecimal(skruEn), retningH);
                    } else {
                        hestHopp1 = new HestHopp(Posisjon.STRAK, retningH);
                    }
                } else if (posisjonEn.equalsIgnoreCase("PIKERT")) {
                    if (!skruEn.equalsIgnoreCase("null") || !skruEn.equalsIgnoreCase("")) {
                        hestHopp1 = new HestHopp(Posisjon.STRAK, new BigDecimal(skruEn), retningH);
                    } else {
                        hestHopp1 = new HestHopp(Posisjon.STRAK, retningH);
                    }
                } else if (posisjonEn.equalsIgnoreCase("Strak")) {
                    if (!skruEn.equalsIgnoreCase("null") || !skruEn.equalsIgnoreCase("")) {
                        hestHopp1 = new HestHopp(Posisjon.STRAK, new BigDecimal(skruEn), retningH);
                    } else {
                        hestHopp1 = new HestHopp(Posisjon.STRAK, retningH);
                    }
                }

                HestHopp hestHopp2 = null;
                if (posisjonTo.equalsIgnoreCase("KROPPERT")) {
                    if (!skruTo.equalsIgnoreCase("null") || !skruTo.equalsIgnoreCase("")) {
                        hestHopp2 = new HestHopp(Posisjon.KROPPERT, new BigDecimal(skruTo), retningH);
                    } else {
                        hestHopp2 = new HestHopp(Posisjon.KROPPERT, retningH);
                    }
                } else if (posisjonTo.equalsIgnoreCase("PIKERT")) {
                    if (!skruTo.equalsIgnoreCase("null") || !skruTo.equalsIgnoreCase("")) {
                        hestHopp2 = new HestHopp(Posisjon.PIKERT, new BigDecimal(skruTo), retningH);
                    } else {
                        hestHopp2 = new HestHopp(Posisjon.PIKERT, retningH);
                    }
                } else if (posisjonTo.equalsIgnoreCase("Strak")) {
                    if (!skruTo.equalsIgnoreCase("null") || !skruTo.equalsIgnoreCase("")) {
                        hestHopp2 = new HestHopp(Posisjon.STRAK, new BigDecimal(skruTo), retningH);
                    } else {
                        hestHopp2 = new HestHopp(Posisjon.STRAK, retningH);
                    }
                }

                HestHopp hestHopp3 = null;
                if (posisjonTre.equalsIgnoreCase("KROPPERT")) {
                    if (!skruTre.equalsIgnoreCase("null") || !skruTre.equalsIgnoreCase("")) {
                        hestHopp3 = new HestHopp(Posisjon.KROPPERT, new BigDecimal(skruTre), retningH);
                    } else {
                        hestHopp3 = new HestHopp(Posisjon.KROPPERT, retningH);
                    }
                } else if (posisjonTre.equalsIgnoreCase("PIKERT")) {
                    if (!skruTre.equalsIgnoreCase("null") || !skruTre.equalsIgnoreCase("")) {
                        hestHopp3 = new HestHopp(Posisjon.PIKERT, new BigDecimal(skruTre), retningH);
                    } else {
                        hestHopp3 = new HestHopp(Posisjon.PIKERT, retningH);
                    }
                } else if (posisjonTre.equalsIgnoreCase("Strak")) {
                    if (!skruTre.equalsIgnoreCase("null") || !skruTre.equalsIgnoreCase("")) {
                        hestHopp3 = new HestHopp(Posisjon.STRAK, new BigDecimal(skruTre), retningH);
                    } else {
                        hestHopp3 = new HestHopp(Posisjon.STRAK, retningH);
                    }
                }

                //MedHoppredskapSerie medHoppredskapSerie = new MedHoppredskapSerie(context, hestHopp1, hestHopp2,hestHopp3);
                SerieManager sm = new SerieManager(context);
                sm.customHopp(name, verdi, retningH, hoppredskap,hestHopp1,hestHopp2,hestHopp3, null, Integer.parseInt(antallRotasjoner)+1);
                //generelleHopp = medHoppredskapSerie;
                generelleHopp = sm.getCustomHopp();

            }
            Log.v("vbcvbcxxx","hoppredskap: " + hoppredskap);
            Log.v("vbcvbcxxx","name: " + name);
            Log.v("vbcvbcxxx","antallRotasjoner: " + antallRotasjoner);
            Log.v("vbcvbcxxx","posisjonEn: " + posisjonEn);
            Log.v("vbcvbcxxx","posisjonTo: " + posisjonTo);
            Log.v("vbcvbcxxx","posisjonTre: " + posisjonTre);
            Log.v("vbcvbcxxx","verdi: " + verdi);
            Log.v("vbcvbcxxx","spesialHopp: " + spesialHopp);
            Log.v("vbcvbcxxx","skruEn: " + skruEn);
            Log.v("vbcvbcxxx","skruTo: " + skruTo);
            Log.v("vbcvbcxxx","skruTre: " + skruTre);
            Log.v("vbcvbcxxx","retning: " + retning);

        }

        return generelleHopp;
    }


}
