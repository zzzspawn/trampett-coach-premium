package com.norwegianonapps.trampettcoachpremium;

import android.content.Context;
import android.util.Log;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.Reader;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by zzzsp on 07-Nov-17.
 */

public class FileHandlerClass {
    Context context;
    public FileHandlerClass(Context context){
        this.context = context;
    }

    public void savePressedLanguage(String ruleset){
        String FILENAME = "language.rules";
        BufferedWriter writer = null;
        try{
            FileOutputStream fos = context.openFileOutput(FILENAME, Context.MODE_PRIVATE);
            writer = new BufferedWriter((new OutputStreamWriter(fos)));
            writer.write(ruleset);
            writer.flush();
            writer.close();
        }catch(Exception e){
            Log.v("okay","crash");
            e.printStackTrace();
        }
    }

    public boolean doesLanguageFileExist(){
        File file = context.getFileStreamPath("language.rules");
        return file.exists();
    }

    public String getLanguage(){
        String linje;
        if(doesLanguageFileExist()){
            try {
                InputStream inputStream = context.openFileInput("language.rules");
                Reader read = new InputStreamReader(inputStream);
                BufferedReader reader = new BufferedReader(read);
                linje = reader.readLine();
                reader.close();
            } catch (FileNotFoundException e) {
                Log.v("okay", "filenotfound");
                e.printStackTrace();
                linje = "English";
            } catch (IOException e) {
                Log.v("okay", "some crash");
                e.printStackTrace();
                linje = "English";
            }

        }else {
            savePressedLanguage("English");
            linje = "English";
        }

        return linje;

    }

    public void savePressedRuleset(String ruleset){
        String FILENAME = "ruleset.rules";
        String string;
        BufferedWriter writer = null;
        try{
            FileOutputStream fos = context.openFileOutput(FILENAME, Context.MODE_PRIVATE);
            writer = new BufferedWriter((new OutputStreamWriter(fos)));
            writer.write(ruleset);
            writer.flush();
            writer.close();
        }catch(Exception e){
            Log.v("okay","crash");
            e.printStackTrace();
        }
    }

    public boolean doesRulesetFileExist(){
        File file = context.getFileStreamPath("ruleset.rules");
        return file.exists();
    }

    public String getRuleset(){
        String linje;
        if(doesRulesetFileExist()){
            try {
                InputStream inputStream = context.openFileInput("ruleset.rules");
                Reader read = new InputStreamReader(inputStream);
                BufferedReader reader = new BufferedReader(read);
                linje = reader.readLine();
                reader.close();
            } catch (FileNotFoundException e) {
                Log.v("okay", "filenotfound");
                //e.printStackTrace();
                linje = "NGTF";
            } catch (IOException e) {
                Log.v("okay", "some crash");
                //e.printStackTrace();
                linje = "NGTF";
            }
        }else {
            savePressedRuleset("NGTF");
            linje = "NGTF";
        }
        return linje;
    }

    public void addCustomSeries(String series){
        String FILENAME = "series.series";
        if(doesCustomSeriesFileExist()){
            BufferedWriter writer = null;
            try{
                //FileOutputStream fos = context.openFileOutput(FILENAME, Context.MODE_APPEND);
                FileOutputStream fos = context.openFileOutput(FILENAME, Context.MODE_PRIVATE);
                writer = new BufferedWriter((new OutputStreamWriter(fos)));
                //writer.newLine();
                writer.write(series);
                writer.flush();
                writer.close();
            }catch(Exception e){
                Log.v("okay","crash");
                e.printStackTrace();
            }

        }else {
            BufferedWriter writer = null;
            try{
                FileOutputStream fos = context.openFileOutput(FILENAME, Context.MODE_PRIVATE);
                writer = new BufferedWriter((new OutputStreamWriter(fos)));
                writer.write(series);
                writer.flush();
                writer.close();
            }catch(Exception e){
                Log.v("okay","crash");
                e.printStackTrace();
            }


        }


    }

    public boolean doesCustomSeriesFileExist(){
        File file = context.getFileStreamPath("series.series");
        return file.exists();
    }

    public boolean doesSeriesExist(String serieInn){
        boolean finnes = false;
        if(doesCustomSeriesFileExist()){
            try {
                InputStream inputStream = context.openFileInput("series.series");
                Reader read = new InputStreamReader(inputStream);
                BufferedReader reader = new BufferedReader(read);
                String serieUt = "";
                while(serieUt != null){

                    if(serieInn.equalsIgnoreCase(serieUt)){
                        finnes = true;
                    }
                    if(!finnes) {
                        serieUt = reader.readLine();
                    }else {
                        serieUt = null;
                    }
                }

                reader.close();
            } catch (FileNotFoundException e) {
                Log.v("okay", "filenotfound");
                e.printStackTrace();

            } catch (IOException e) {
                Log.v("okay", "some crash");
                e.printStackTrace();

            }

            return finnes;

        }else {
            return false;
        }

    }

    public List<String> getAllSeries(){
        List<String> list = new ArrayList<>();
        if(doesCustomSeriesFileExist()) {
            try {
                InputStream inputStream = context.openFileInput("series.series");
                Reader read = new InputStreamReader(inputStream);
                BufferedReader reader = new BufferedReader(read);
                String serieUt = reader.readLine();
                while (serieUt != null) {
                    Log.v("gfdgfdsg","Ran");
                    if(serieUt != null) {
                        list.add(serieUt);
                    }
                    serieUt = reader.readLine();
                }

                reader.close();
            } catch (FileNotFoundException e) {
                Log.v("okay", "filenotfound");
                e.printStackTrace();

            } catch (IOException e) {
                Log.v("okay", "some crash");
                e.printStackTrace();

            }
            return list;
        }else {
            return null;
        }
    }


    public void saveNumberOfSeries(String number){
        String FILENAME = "number.series";
        BufferedWriter writer = null;
        try{
            FileOutputStream fos = context.openFileOutput(FILENAME, Context.MODE_PRIVATE);
            writer = new BufferedWriter((new OutputStreamWriter(fos)));
            writer.write(number);
            writer.flush();
            writer.close();
        }catch(Exception e){
            Log.v("okay","crash");
            e.printStackTrace();
        }
    }

    public boolean doesNumberOfSeriesFileExist(){
        File file = context.getFileStreamPath("number.series");
        return file.exists();
    }

    public String getNumberOfSeries(){
        String linje;
        if(doesNumberOfSeriesFileExist()){
            try {
                InputStream inputStream = context.openFileInput("number.series");
                Reader read = new InputStreamReader(inputStream);
                BufferedReader reader = new BufferedReader(read);
                linje = reader.readLine();
                reader.close();
            } catch (FileNotFoundException e) {
                Log.v("okay", "filenotfound");
                e.printStackTrace();
                linje = "English";
            } catch (IOException e) {
                Log.v("okay", "some crash");
                e.printStackTrace();
                linje = "English";
            }

        }else {
            savePressedLanguage("English");
            linje = "English";
        }

        return linje;

    }

    public void saveChangeOccurred(String number){
        String FILENAME = "change.occurred";
        BufferedWriter writer = null;
        try{
            FileOutputStream fos = context.openFileOutput(FILENAME, Context.MODE_PRIVATE);
            writer = new BufferedWriter((new OutputStreamWriter(fos)));
            writer.write(number);
            writer.flush();
            writer.close();
        }catch(Exception e){
            Log.v("okay","crash");
            e.printStackTrace();
        }
    }

    public boolean doesChangeOccurredFileExist(){
        File file = context.getFileStreamPath("change.occurred");
        return file.exists();
    }

    public String getChangeOccurred(){
        String linje;
        if(doesChangeOccurredFileExist()){
            try {
                InputStream inputStream = context.openFileInput("change.occurred");
                Reader read = new InputStreamReader(inputStream);
                BufferedReader reader = new BufferedReader(read);
                linje = reader.readLine();
                reader.close();
            } catch (FileNotFoundException e) {
                Log.v("okay", "filenotfound");
                e.printStackTrace();
                linje = "English";
            } catch (IOException e) {
                Log.v("okay", "some crash");
                e.printStackTrace();
                linje = "English";
            }

        }else {
            savePressedLanguage("English");
            linje = "English";
        }

        return linje;

    }

}
