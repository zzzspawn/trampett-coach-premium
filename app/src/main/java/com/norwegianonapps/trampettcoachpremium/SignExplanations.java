package com.norwegianonapps.trampettcoachpremium;

import android.graphics.Typeface;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class SignExplanations extends AppCompatActivity {
    FileHandlerClass fileHandlerClass;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_explanations);
        fileHandlerClass = new FileHandlerClass(this);
        settOppFonter();
        setLanguages();
    }

    public void settOppFonter(){
        Typeface typeface = null;
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.O) {
            typeface = getResources().getFont(R.font.turntegnregular);
        }else {
            typeface = Typeface.createFromAsset(getAssets(),  "fonts/turntegnregular.ttf");
        }
        TextView strekkhopp = findViewById(R.id.strekkhopp);
        TextView xHopp = findViewById(R.id.xHopp);
        TextView splitthopp = findViewById(R.id.splitthopp);
        TextView kroppertsalto = findViewById(R.id.kroppertsalto);
        TextView pikertsalto = findViewById(R.id.pikertsalto);
        TextView straksalto = findViewById(R.id.straksalto);
        TextView tsu = findViewById(R.id.tsukahara);
        TextView overslag = findViewById(R.id.overslag);
        TextView nittipaanittiav = findViewById(R.id.nittipanittiav);

        strekkhopp.setTypeface(typeface);
        xHopp.setTypeface(typeface);
        splitthopp.setTypeface(typeface);
        kroppertsalto.setTypeface(typeface);
        pikertsalto.setTypeface(typeface);
        straksalto.setTypeface(typeface);
        tsu.setTypeface(typeface);
        overslag.setTypeface(typeface);
        nittipaanittiav.setTypeface(typeface);
    }

    public void setLanguages(){
        TextView signExplanationDescriptionText = findViewById(R.id.signExplanationDescriptionText);
        TextView symbolText = findViewById(R.id.symbolText);
        TextView elementerText = findViewById(R.id.elementerText);
        TextView strekkHoppTekst = findViewById(R.id.strekkHoppTekst);
        TextView xHoppTekst = findViewById(R.id.xHoppTekst);
        TextView splitthoppTekst = findViewById(R.id.splitthoppTekst);
        TextView kroppertsaltoTekst = findViewById(R.id.kroppertsaltoTekst);
        TextView pikertsaltoTekst = findViewById(R.id.pikertsaltoTekst);
        TextView straksaltoTekst = findViewById(R.id.straksaltoTekst);
        TextView tsukaharaTekst = findViewById(R.id.tsukaharaTekst);
        TextView overslagTekst = findViewById(R.id.overslagTekst);
        TextView nittipanittiavTekst = findViewById(R.id.nittipanittiavTekst);
        TextView skruIGraderTekst = findViewById(R.id.skruIGraderTekst);
        Button signExplanationDescriptionButton = findViewById(R.id.signExplanationDescriptionButton);

        TextClassGetter tCG = new TextClassGetter(this);
        String[] array = tCG.getText("SignExplanations");
        signExplanationDescriptionText.setText(array[0]);
        symbolText.setText(array[1]);
        elementerText.setText(array[2]);
        strekkHoppTekst.setText(array[3]);
        xHoppTekst.setText(array[4]);
        splitthoppTekst.setText(array[5]);
        kroppertsaltoTekst.setText(array[6]);
        pikertsaltoTekst.setText(array[7]);
        straksaltoTekst.setText(array[8]);
        tsukaharaTekst.setText(array[9]);
        overslagTekst.setText(array[10]);
        nittipanittiavTekst.setText(array[11]);
        skruIGraderTekst.setText(array[12]);
        signExplanationDescriptionButton.setText(array[13]);
    }

    public void onTilbakePressed(View view){
        onBackPressed();
    }

}
