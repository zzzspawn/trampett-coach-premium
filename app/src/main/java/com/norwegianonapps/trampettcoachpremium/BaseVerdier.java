package com.norwegianonapps.trampettcoachpremium;

/**
 * Created by zzzsp on 03-Oct-17.
 */

import android.content.Context;
import android.widget.Button;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.InputStream;
import java.io.InputStreamReader;

public class BaseVerdier {



    private String salto;
    private String dobbelSalto;
    private String trippelSalto;
    private String overslag;
    private String overslagSalto;
    private String overslagDobbelSalto;
    private String overslag90Pa90Av;
    private String tsukahara;
    private String dobbelTsukahara;
    private String reglement;

    public String getSaltoVerdi() {
        return salto;
    }
    public String getDobbelSaltoVerdi() {
        return dobbelSalto;
    }
    public String getTrippelSaltoVerdi() {
        return trippelSalto;
    }
    public String getOverslagVerdi() {return overslag;}
    public String getOverslagSaltoVerdi() {return overslagSalto;}
    public String getOverslagDobbelSaltoVerdi() {return overslagDobbelSalto;}
    public String getOverslag90Pa90AvVerdi() {return overslag90Pa90Av;}
    public String getTsukaharaVerdi() {return tsukahara;}
    public String getDobbelTsukaharaVerdi() {return dobbelTsukahara;}
    Context context;


    public BaseVerdier(Context context){
        this.context = context;
        FileHandlerClass fileHandlerClass = new FileHandlerClass(context);
        reglement = fileHandlerClass.getRuleset();
    }

    public void importValues() {

        String filnavn;
        if(reglement.equalsIgnoreCase("ngtf")){
            filnavn = "baseverdier";
        }else if(reglement.equalsIgnoreCase("ueg")){
            filnavn = "baseverdierueg";
        }else if(reglement.equalsIgnoreCase("island")){
            filnavn = "baseverdierisland";
        }else {
            filnavn = "tilleggsverdier";
        }

        BufferedReader br;
        try {
            //br = new BufferedReader(new FileReader("baseverdier.csv"));
            InputStream inputStream = context.getResources().openRawResource(
                    context.getResources().getIdentifier(filnavn,
                            "raw", context.getPackageName()));

            InputStreamReader read = new InputStreamReader(inputStream);
            br = new BufferedReader(read);

            br.readLine();
            String line = br.readLine();
            salto = extractValue(line);
            line = br.readLine();
            dobbelSalto = extractValue(line);
            line = br.readLine();
            trippelSalto = extractValue(line);
            line = br.readLine();
            overslag = extractValue(line);
            line = br.readLine();
            overslagSalto = extractValue(line);
            line = br.readLine();
            overslagDobbelSalto = extractValue(line);
            line = br.readLine();
            overslag90Pa90Av = extractValue(line);
            line = br.readLine();
            tsukahara = extractValue(line);
            line = br.readLine();
            dobbelTsukahara = extractValue(line);
            br.close();
        }catch(Exception e) {
            e.printStackTrace();
        }
    }

    public String extractValue(String line) {
        String lineOut = "";
        int i = 0;
        int teller = 0;
        boolean first = false;
        while(i < line.length()) {
            if(line.charAt(i) == ',') {
                teller++;
            }
            if(teller == 2) {
                if(!first) {
                    i++;
                    first = true;
                }
                lineOut = lineOut+line.charAt(i);
            }
            i++;
        }

        //System.out.println(line);
        return lineOut;
    }

    public void printValues() {
        System.out.println("Verdi av Salto: " + salto);
        System.out.println("Verdi av Dobbel salto: " + dobbelSalto);
        System.out.println("Verdi av Trippel salto: " + trippelSalto);
        System.out.println("Verdi av Overslag: " + overslag);
        System.out.println("Verdi av Overslag salto: " + overslagSalto);
        System.out.println("Verdi av Overslag dobbel salto: " + overslagDobbelSalto);
        System.out.println("Verdi av Overslag 90 På, 90 Av: " + overslag90Pa90Av);
        System.out.println("Verdi av Tsukahara: " + tsukahara);
        System.out.println("Verdi av Dobbel Tsukahara: " + dobbelTsukahara);
    }





}
