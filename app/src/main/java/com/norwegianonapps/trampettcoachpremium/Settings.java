package com.norwegianonapps.trampettcoachpremium;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class Settings extends AppCompatActivity {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_settings);
        setLanguages();
    }
    @Override
    public void onResume()
    {  // After a pause OR at startup
        super.onResume();
        //Refresh your stuff here
        setLanguages();
    }

    public void onRulesetPressed(View view){
        Intent i=new Intent(this,RulesetSettings.class);
        //i.putExtra("valgt", "medhoppredskap");
        startActivity(i);
    }
    public void onLanguagePressed(View view){
        Intent i=new Intent(this,LanguageSettings.class);
        startActivity(i);
    }
    public void onTegnPressed(View view){
        Intent i=new Intent(this,SignExplanations.class);
        startActivity(i);
    }
    public void onCustomPressed(View view){
        Intent i=new Intent(this,CreateCustomSeries.class);
        startActivity(i);
    }

    public void onTestPressed(View view){
        Intent i=new Intent(this,TesterScreen.class);
        startActivity(i);
    }

    public void buttonBackPressed(View view){
        onBackPressed();
    }

    public void setLanguages(){
        TextView settingsTV = findViewById(R.id.settingsText);
        Button rulesetButton = findViewById(R.id.rulesetButton);
        Button languageButton = findViewById(R.id.languageButton);
        Button tegnForklaringButton = findViewById(R.id.tegnForklaringButton);
        Button customButton = findViewById(R.id.customTegnButton);
        Button backButtonSettings = findViewById(R.id.backButtonSettings);

        TextClassGetter tCG = new TextClassGetter(this);
        String[] array = tCG.getText("Settings");
        settingsTV.setText(array[0]);
        rulesetButton.setText(array[1]);
        languageButton.setText(array[2]);
        tegnForklaringButton.setText(array[3]);
        customButton.setText(array[4]);
        backButtonSettings.setText(array[5]);
    }


}
